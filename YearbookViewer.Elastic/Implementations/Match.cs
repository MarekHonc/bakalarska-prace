﻿using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.Elastic.Implementations
{
	/// <summary>
	/// Implementace pro zvýrazňování v textu pomocí elastic search.
	/// Bohužel elastic neumí vracet indexy shod.
	/// </summary>
	public class Match : IMatchData
	{
		/// <summary>
		/// Vrací nebo nastavuje index, na kterém se nachází shoda.
		/// </summary>
		public int Index
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje délku (veliksot) shody.
		/// </summary>
		public int Length 
		{ 
			get;
			set;
		}
	}
}
