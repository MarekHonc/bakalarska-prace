﻿using System.Linq;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Elastic.Extensions;

namespace YearbookViewer.Elastic.Implementations
{
	/// <summary>
	/// Provider pro data do search boxů pro hodnoty.
	/// </summary>
	public class ElasticSearchFilterProvider : ISearchFilterValueProvider, IFilePathRequired
	{
		/// <summary>
		/// Vrací filtry první úrovně tj. celý index.
		/// </summary>
		/// <returns></returns>
		public string[] GetTopLevelFilter()
		{
			var years = ElasticContext.Context
				.Search<ElasticTopLevelFilter>(s => s.Index(ElasticContext.TopLevelFilters).All());

			var result = years.Hits.Select(h => h.Source.FilterText).ToArray();
			return result;
		}

		/// <summary>
		/// Druhá úroveň filtrů na základě zvoleného vrchního.
		/// Tj. všechny z indexu, které jsou pro daný rok.
		/// </summary>
		public string[] GetSecondaryFilter(string topLevelFilter)
		{
			var contents = ElasticContext.Context
				.Search<ElasticSecondaryLevel>(s => s
					.Index(ElasticContext.SecondaryLevelFilters)
					.Source(so => so
						.Includes(i => i
							.Fields(
								ff => ff.ContentText
							)
						)
					)
					.All()
					.Query(q => q
						.MatchPhrase(c => c.Field(p => p.TopLevelFilter.FilterText).Query(topLevelFilter))
					)
				);

			var result = contents.Hits.Select(h => h.Source.ContentText).ToArray();
			return result;
		}

		/// <summary>
		/// Vrací všechen content podle předaných filtrů.
		/// </summary>
		public string[] YearBookNames(string topLevelFilter, string secondaryFilter)
		{
			var pages = ElasticContext.Context
				.Search<ElasticContent>(s => s
					.Index(ElasticContext.YearbookPages)
					.Source(so => so
						.Includes(i => i
							.Fields(
								ff => ff.PageName
							)
						)
					)
					.All()
					 .Query(q =>
						// Buď vyhledávám podle roku a nebo podle obsahu.
						secondaryFilter.IsNullOrEmpty()
					 	? q.MatchPhrase(c => c.Field(p => p.TopLevelFilter.FilterText).Query(topLevelFilter))
					 	: q.MatchPhrase(c => c.Field(p => p.SecondaryLevel.ContentText).Query(secondaryFilter))
					 )
					// .Sort(so => so.Ascending(b => b.PageName))
				);

			var result = pages.Hits.Select(h => h.Source.PageName).ToArray();
			return result;
		}

		/// <inheritdoc cref="ISearchFilterValueProvider.Instance"/>.
		public ISearchFilterValueProvider Instance => this;

		/// <inheritdoc cref="SetPaths"/>.
		public void SetPaths(params string[] paths)
		{
			ElasticContext.Detect(paths[1]);
		}
	}
}
