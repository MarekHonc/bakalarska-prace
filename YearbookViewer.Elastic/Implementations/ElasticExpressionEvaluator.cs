﻿using Nest;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.Elastic.Implementations
{
	/// <summary>
	/// Vyhodnocovač pro vyhledávání v textu používající elastic search.
	/// </summary>
	public class ElasticExpressionEvaluator : IExpressionEvaluator
	{
		/// <summary>
		/// Vrací jestli se v textu vyskytuje shoda s vyhledávaným textem.
		/// </summary>
		/// <param name="text">Text, který bude kontrolován.</param>
		/// <param name="expr">Výraz podle který se v textu hledá.</param>
		/// <param name="isExpression">Nepoužívá se!</param>
		public bool IsMatch(string text, string expr, bool isExpression = false)
		{
			return expr.ToLower().Contains(text?.ToLower());
		}

		/// <summary>
		/// Vrací všechny shody textu s textem.
		/// </summary>
		/// <param name="text">Text, který bude kontrolován.</param>
		/// <param name="expr">Výraz podle který se v textu hledá.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		/// <returns>Koleckce všech matchů, které se v textu nachází.</returns>
		public ICollection<IMatchData> Matches(string text, string expr, bool isExpression = false)
		{
			if (isExpression)
				throw new Exception("Expresions are not supported");

			expr = Regex.Escape(expr);
			var results = new List<IMatchData>();

			foreach (System.Text.RegularExpressions.Match match in Regex.Matches(text, expr, RegexOptions.IgnoreCase))
			{
				results.Add(new Match() { Index = match.Index, Length = match.Length });
			}

			return results;
		}

		/// <summary>
		/// Rozdělí string podle daného textu.
		/// </summary>
		/// <param name="text">Text, který bude výrazem rozdělen.</param>
		/// <param name="expr">Výraz podle kterého dělí daný text, pokud výraz není platný vyhodí výjimku.</param>
		/// <param name="isExpression">Nepoužívá se!</param>
		/// <returns>Pole stringů, které zniklo na základě rozdělení.</returns>
		public string[] Split(string text, string expr, bool isExpression = false)
		{
			// Rozdělím podle speciální tagu pro označení -> Škoda že WPF není jako html...
			return text.Split(new[] { ElasticContext.HighlightEnclosure }, StringSplitOptions.RemoveEmptyEntries);
		}

		/// <inheritdoc cref="IExpressionEvaluator.Instance"/>.
		public IExpressionEvaluator Instance => this;

		#region NotUsed

		public bool IsValid(string expr)
		{
			return true;
		}

		#endregion
	}
}
