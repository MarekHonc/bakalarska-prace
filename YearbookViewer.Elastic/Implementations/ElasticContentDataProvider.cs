﻿using Nest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.Readers;

namespace YearbookViewer.Elastic.Implementations
{
	/// <summary>
	/// Provider pro data ročenek.
	/// </summary>
	public class ElasticContentDataProvider : IContentDataProvider, IFilePathRequired
	{
		/// <inheritdoc cref="GetCount"/>.
		public IReadOnlyCollection<IContentData> GetCount(IFilterData filters, int count, int? offset = null)
		{
			var searchQuery = new SearchDescriptor<ElasticContent>();

			searchQuery.Index(ElasticContext.YearbookPages);

			// Mám hledání napříč vším, nebo nemám vybrán rok ani obsah.
			if(filters.UseGlobalSearch || 
				(filters.TopLevelFilter.IsNullOrEmpty() && filters.SecondaryFilter.IsNullOrEmpty()))
			{
				searchQuery.Query(q => q
					.MatchPhrase(qs => qs
						.Field(f => f.Text)
						.Query(filters.SearchTerm)
					)
				);
			}
			else
			{
				searchQuery.Query(q =>
					q.Bool(b => b
						.Must(mu =>
							filters.SecondaryFilter.IsNullOrEmpty()
								? mu.MatchPhrase(c => c.Field(p => p.TopLevelFilter.FilterText).Query(filters.TopLevelFilter))
								: mu.MatchPhrase(c => c.Field(p => p.SecondaryLevel.ContentText).Query(filters.SecondaryFilter))
						).Filter(fi => fi
							.MatchPhrase(qs => qs
								.Query(filters.SearchTerm)
								.Field(f => f.Text)
							)
						)
					)
				);
			}

			searchQuery.Skip(offset);
			searchQuery.Size(count);
			// searchQuery.Sort(so => so.Ascending(f => f.PageName));

			searchQuery.Highlight(h => h
				.PreTags(ElasticContext.HighlightEnclosure)
				.PostTags(ElasticContext.HighlightEnclosure)
				.NumberOfFragments(0)
				.Fields(fs => fs
					.Field(f => f.Text)
				)
			);

			var pages = ElasticContext.Context
				.Search<ElasticContent>(searchQuery);

			var results = new List<IContentData>();

			foreach(var hit in pages.Hits)
			{
				hit.Source.ImageTextForHighlight = string.Join(Environment.NewLine, hit.Highlight["Text"]);
				results.Add(hit.Source);
			}

			return results;
		}

		/// <inheritdoc cref="GetFirst"/>.
		public IContentData GetFirst(IFilterData filters)
		{
			var pages = ElasticContext.Context
				.Search<ElasticContent>(ElasticContext.GetFirstYearBook(filters));

			return pages.Hits.FirstOrDefault()?.Source;
		}

		/// <inheritdoc cref="Save"/>.
		public bool Save(IFilterData filters, string newText)
		{
			var file = (ElasticContent)GetFirst(filters);

			// Updatuji elastic
			ElasticContext.Context
				.Update<ElasticContent>(file.Id, u => u
					.Index(ElasticContext.YearbookPages)
					.Doc(new ElasticContent() { Text = newText })
				);

			// Získám třídu pro zápis.
			var reader = CustomTextReader.GetReader(Path.GetExtension(file.PagePath));

			// Uložím a vracím, zda-li se povedlo.
			return reader.Write(file.PagePath, newText, file.ImageText);
		}

		/// <inheritdoc cref="IContentDataProvider.Instance"/>.
		public IContentDataProvider Instance => this;

		/// <inheritdoc cref="SetPaths"/>.
		public void SetPaths(params string[] paths)
		{
			ElasticContext.Detect(paths[1]);
		}
	}
}
