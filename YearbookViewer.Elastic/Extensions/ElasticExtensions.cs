﻿using Nest;

namespace YearbookViewer.Elastic.Extensions
{
	/// <summary>
	/// Třída s rozšiřujícími metodami pro elastic search.
	/// </summary>
	public static class ElasticExtensions
	{
		/// <summary>
		/// Vrátí všechny výskyty.
		/// </summary>
		public static SearchDescriptor<T> All<T>(this SearchDescriptor<T> query) where T: class
		{
			// Bohužel nenašel jsem jiný způsob jak zajistit vrácení všech.
			return query.Size(999);
		}
	}
}
