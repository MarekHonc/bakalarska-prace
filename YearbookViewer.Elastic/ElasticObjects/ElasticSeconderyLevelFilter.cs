﻿namespace YearbookViewer.Elastic
{
	/// <summary>
	/// Třída pro uložení všech obsahů ročenek.
	/// </summary>
	public class ElasticSecondaryLevel
	{
		/// <summary>
		/// Vrací nebo nastavuje rok, ke kterému se obsah váže.
		/// </summary>
		public ElasticTopLevelFilter TopLevelFilter
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje obsah daného roku.
		/// </summary>
		public string ContentText
		{
			get;
			set;
		}
	}
}
