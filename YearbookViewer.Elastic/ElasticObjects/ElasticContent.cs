﻿using Nest;
using System;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.Elastic
{
	/// <summary>
	/// Třída pro uložení do elastic search.
	/// </summary>
	public class ElasticContent : IContentData
	{
		[Keyword]
		public Guid Id
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje ročník ročenky.
		/// </summary>
		public ElasticTopLevelFilter TopLevelFilter
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje obsah, do kterého patří.
		/// </summary>
		public ElasticSecondaryLevel SecondaryLevel
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje název stránky.
		/// </summary>
		[Keyword]
		public string PageName
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje cestu k souboru.
		/// </summary>
		public string PathToFile
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje text v ročence.
		/// </summary>
		public string Text
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastvuje cestu k obrázku.
		/// </summary>
		public string PathToImage
		{
			get;
			set;
		}

		#region Implementace IContentData

		[Ignore]
		public string ImageUrl => this.PathToImage;

		[Ignore]
		public string ImageText => this.Text;

		[Ignore]
		public string PagePath => this.PathToFile;

		[Ignore]
		public string ImageTextForHighlight
		{
			get;
			set;
		}

		[Ignore]
		string IContentData.Year => this.TopLevelFilter.FilterText;

		#endregion
	}
}
