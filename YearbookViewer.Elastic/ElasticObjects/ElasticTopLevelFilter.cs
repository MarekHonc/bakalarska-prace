﻿namespace YearbookViewer.Elastic
{
	/// <summary>
	/// Třída pro uložení roků.
	/// </summary>
	public class ElasticTopLevelFilter
	{
		/// <summary>
		/// Vrací nebo nastavuje rok ročenky.
		/// </summary>
		public string FilterText
		{
			get;
			set;
		}
	}
}
