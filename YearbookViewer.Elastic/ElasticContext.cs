﻿using Nest;
using System;
using System.Threading.Tasks;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.Elastic
{
	/// <summary>
	/// Singleton context pro přístup do elastic search.
	/// </summary>
	public class ElasticContext
	{
		#region Přístup k elastic serveru

		private static string elasticServerAddress = "http://localhost:9200/";
		private static ElasticClient context;

		#endregion

		/// <summary>
		/// Vytvoří nový context pro práci s elastic search.
		/// </summary>
		private static void CreateContext()
		{
			var settings = new ConnectionSettings(new Uri(elasticServerAddress))
				.EnableDebugMode()
				.DefaultFieldNameInferrer(s => s) //disable CAMEL CASE
				.DefaultMappingFor<ElasticContent>(yb => yb.IdProperty(id => id.Id))
				.PrettyJson();

			context = new ElasticClient(settings);
		}

		/// <summary>
		/// Vrací singleton context pro přístup k elastic search.
		/// </summary>
		internal static ElasticClient Context
		{
			get
			{
				if (context == null)
					CreateContext();

				return context;
			}
		}

		/// <summary>
		/// Vrací aktuální adresu elastic search serveru.
		/// </summary>
		public static string ElasticServerAddress => elasticServerAddress;

		/// <summary>
		/// Vrací, zda-li je připojení na Elastic Search server platné.
		/// </summary>
		/// <param name="address">Adresa, na které se elastic nachází.</param>
		/// <returns><code>true</code> pokud se podařilo připojit, jinak <code>false</code>.</returns>
		public static bool IsConnectionValid(string address)
		{
			try
			{
				var settings = new ConnectionSettings(new Uri(address));

				var client = new ElasticClient(settings);
				var response = client.Ping();

				return response.IsValid;
			}
			catch
			{
				return false;
			}
		}

		#region Create

		/// <summary>
		/// Vymaže všechny položky z elastiku.
		/// </summary>
		public static async Task RemoveAllExisting()
		{
			await Context.Indices.DeleteAsync(SecondaryLevelFilters);
			await Context.Indices.DeleteAsync(TopLevelFilters);
			await Context.Indices.DeleteAsync(YearbookPages);
		}

		/// <summary>
		/// Přidá a vrátí filtr vrchní úrovně do elastic search.
		/// </summary>
		public static async Task<ElasticTopLevelFilter> CreateTopLevel(string topLevel)
		{
			// Vytvořím rok.
			var topLevelObj = new ElasticTopLevelFilter()
			{
				FilterText = topLevel
			};

			// Přidám do elastiku.
			await Context.IndexManyAsync(new [] { topLevelObj }, TopLevelFilters);

			return topLevelObj;
		}

		/// <summary>
		/// Přidá a vrátí filtr druhé úrovně do elastic search.
		/// </summary>
		public static async Task<ElasticSecondaryLevel> CreateSecondaryLevel(ElasticTopLevelFilter topLevel, string secondaryLevel)
		{
			// Vytvořím filtr druhé úrovně.
			var secondaryLevelObj = new ElasticSecondaryLevel()
			{
				ContentText = secondaryLevel,
				TopLevelFilter = topLevel
			};

			// Přidám do elastiku.
			await Context.IndexManyAsync(new[] { secondaryLevelObj }, SecondaryLevelFilters);

			return secondaryLevelObj;
		}

		/// <summary>
		/// Přidá a vrátí novou položku textu s obrázek do elastiku.
		/// </summary>
		public static async Task<ElasticContent> CreateContent(ElasticTopLevelFilter topLevel,
			ElasticSecondaryLevel secondaryLevel, string pageName, string pathToFile, string text, string pathToImage)
		{
			// Vytvořím položku.
			var content = new ElasticContent()
			{
				Id = Guid.NewGuid(),
				TopLevelFilter = topLevel,
				SecondaryLevel = secondaryLevel,
				PageName = pageName,
				PathToFile = pathToFile,
				Text = text,
				PathToImage = pathToImage
			};

			// Přidám do elastiku.
			await Context.IndexManyAsync(new[] { content }, YearbookPages);

			return content;
		}

		#endregion

		#region AutoDetect

		/// <summary>
		/// Nastaví připojí na elastic search.
		/// </summary>
		public static void Detect(string serverAddress)
		{
			// Nastavím novou adresu serveru a kontext pře vytvořím.
			elasticServerAddress = serverAddress;
			CreateContext();
		}

		#endregion

		#region Queries

		/// <summary>
		/// Vrací query pro vyhledání první ročenky.
		/// </summary>
		public static SearchDescriptor<ElasticContent> GetFirstYearBook(IFilterData filters)
		{
			var searchQuery = new SearchDescriptor<ElasticContent>();

			searchQuery.Index(YearbookPages);
			searchQuery.Size(1);
			searchQuery.Query(q =>
				filters.PageName.IsNotNullOrEmpty()
					? q.MatchPhrase(c => c.Field(p => p.PageName).Query(filters.PageName))
					: filters.SecondaryFilter.IsNullOrEmpty()
						? q.MatchPhrase(c => c.Field(p => p.TopLevelFilter.FilterText).Query(filters.TopLevelFilter))
						: q.MatchPhrase(c => c.Field(p => p.SecondaryLevel.ContentText).Query(filters.SecondaryFilter))

			);
			// searchQuery.Sort(s => s.Ascending(f => f.PageName));

			return searchQuery;
		}

		#endregion

		/// <summary>
		/// Vrací index, pod kterým jsou uloženy všechny roky.
		/// </summary>
		public const string TopLevelFilters = "top-level";

		/// <summary>
		/// Vrací index, pod kterým jsou uloženy všechny stránky ročenek.
		/// </summary>
		public const string YearbookPages = "content-pages";

		/// <summary>
		/// Vrací index, pod kterým jsou uloženy všechny obsahy ročenek.>
		/// </summary>
		public const string SecondaryLevelFilters = "secondary-level";

		/// <summary>
		/// Vrací prexif hightlightů.
		/// </summary>
		public const string HighlightEnclosure = "<hightlight>";
	}
}
