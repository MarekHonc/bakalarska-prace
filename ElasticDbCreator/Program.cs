﻿using Nest;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YearbookViewer.Common;
using YearbookViewer.DL;
using YearbookViewer.Elastic;

namespace ElasticDbCreator
{
	class Program
	{
		// TODO: Stejně se nepoužívá? možná smazat? - projekt sloužil jako technologické demo pro mě

		static void Main(string[] args)
		{
			ElasticClient client = null; // ElasticContext.Context;

			client.Indices.Create(ElasticContext.YearIndex);
			client.Indices.Create(ElasticContext.YearbookPages, i => i.Map<ElasticYearBook>(mm => mm.AutoMap()));

			var years = FileBrowser.GetAllDirectoryNamesBeginintWith("D:\\Rocenky_JJHS", "r").Select(y => new ElasticYear() { Year = y });

			// Roky
			var elasticYears = years;
			client.IndexMany(elasticYears, ElasticContext.YearIndex);

			// Obsahy
			foreach (var year in years)
			{
				client.Indices.Create(ElasticContext.YearbookContentsForYear);

				var contents = FileBrowser.GetContents(Path.Combine("D:\\Rocenky_JJHS\\obsah\\Obsah_Rocenky_JJHS_Final"), year.Year);

				var toAdd = new List<ElasticContent>();

				foreach (var content in contents)
				{
					var pages = new Tuple<int, int>(0, int.MaxValue);

					try
					{
						pages = FileBrowser.PageNumbersFromContent(content, contents);
					}
					catch (Exception)
					{

					}

					toAdd.Add(new ElasticContent()
					{
						Year = year,
						ContentText = content,
						PageStart = pages.Item1,
						PageEnd = pages.Item2
					});
				}

				var toAdd2 = new List<ElasticYearBook>();

				foreach (var path in FileBrowser.GetAllTextFiles(Path.Combine("D:\\Rocenky_JJHS", year.Year), new [] { "txt" }))
				{
					var fileRegex = new Regex(@"s\d+");
					var page = 0;
					
					try
					{
						page = int.Parse(fileRegex.Match(path).Value.Replace("s", string.Empty));
					}
					catch(Exception)
					{

					}

					toAdd2.Add(new ElasticYearBook()
					{
						Id = Guid.NewGuid(),
						Content = toAdd.FirstOrDefault(x => x.PageStart <= page && x.PageEnd > page),
						Year = year,
						PageName = Path.GetFileName(path),
						PathToFile = path,
						PathToImage = File.Exists(path.Replace(".txt", ".jpg")) ? path.Replace(".txt", ".jpg") : null,
						Text = string.Join(Environment.NewLine, File.ReadLines(path))
					});
				}

				client.IndexMany(toAdd2, ElasticContext.YearbookPages);

				client.IndexMany(toAdd, ElasticContext.YearbookContentsForYear);
			}
		}
	}
}
