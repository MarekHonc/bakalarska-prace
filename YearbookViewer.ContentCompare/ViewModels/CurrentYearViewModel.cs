﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Documents;
using YearbookViewer.Common.Extensions;
using YearbookViewer.ContentCompare.ViewModels.Base;
using YearbookViewer.Readers.ImportGrouping;

namespace YearbookViewer.ContentCompare.ViewModels
{
	/// <summary>
	/// ViewModel pro zobrazení obsahů konkrétního roku.
	/// </summary>
	public class CurrentYearViewModel : ViewModelBase
	{
		#region private properties

		private int year;
		private readonly string contentFilePath;
		private readonly string imagePath;

		private readonly string xlsxFilePath;
		private readonly string xlsxYearColumn;
		private readonly string xlsxPageColumn;
		private readonly string xlsxNameColumn;

		#endregion

		public CurrentYearViewModel(int year, string imagePath, string contentFilePath,
			string xlsxFilePath, string xlsxYearColumn, string xlsxPageColumn, string xlsxNameColumn)
		{
			this.Year = year;

			this.contentFilePath = contentFilePath;
			this.xlsxFilePath = xlsxFilePath;
			this.imagePath = imagePath;

			this.xlsxYearColumn = xlsxYearColumn;
			this.xlsxPageColumn = xlsxPageColumn;
			this.xlsxNameColumn = xlsxNameColumn;
		}

		/// <summary>
		/// Vrací nebo nastavuje rok, který se zobrazuje.
		/// </summary>
		public int Year
		{
			get { return this.year; }
			set
			{
				this.year = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací cestu k obrázku.
		/// </summary>
		public string ImagePath
		{
			get { return this.imagePath; }
		}

		#region obsahy samotné

		private ObservableCollection<ContentViewModel> xlsxContents;
		private ObservableCollection<ContentViewModel> fileContents;
		private ObservableCollection<ContentViewModelGroup> groupedContents;

		/// <summary>
		/// Vrací nebo nastavuje všechny obsahy, které k sobě sedí - pokud je hodnota dostupná, nebo nesedí - jedna z hodnot je null.
		/// </summary>
		public ObservableCollection<ContentViewModelGroup> GroupedContents
		{
			get { return this.groupedContents; }
			set
			{
				this.groupedContents = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Nastaví do modelu všechny obsahy pro daný rok.
		/// </summary>
		public void SetFileContents()
		{
			// 1) Ze souboru.
			var rows = File.ReadAllLines(this.contentFilePath)
				.Where(x => !string.IsNullOrWhiteSpace(x) && Regex.Match(x.Substring(0, 5), @"\d+").Success)
				.Select(x => x.Replace("#", string.Empty).Replace("\t", string.Empty).Trim())
				.Select(x => new ContentViewModel()
				{
					StartPage = int.Parse(Regex.Matches(x, @"\d+")[0].Value),
					Name = x.Replace(Regex.Matches(x, @"\d+")[0].Value, string.Empty).Trim()
				}).ToObservableCollection();

			this.fileContents = rows;

			// 2) Z xlsx
			var contentsFromXlsx = ImportGroupingProvider.GetGroups(this.xlsxFilePath,
				this.xlsxYearColumn, this.xlsxPageColumn, this.xlsxNameColumn);

			foreach (var content in contentsFromXlsx)
			{
				// Pokud rok nesedí - pokračuji.
				if (!content.Key.Contains(this.Year.ToString()))
					continue;

				// Vytvořím kolekci.
				var collection = content.Value.Select(x => new ContentViewModel()
				{
					StartPage = x.StartPage,
					Name = x.Name
				}).ToObservableCollection();

				// Nastavím do modelu.
				this.xlsxContents = collection;

				// Už nemusím nic dělat.
				break;
			}

			// Naházím do kolekce, kterou pak renderuji.
			var result = new List<ContentViewModelGroup>();

			foreach (var file in this.xlsxContents)
			{
				result.Add(new ContentViewModelGroup() { Item1 = file });
			}

			foreach (var file in this.fileContents)
			{
				var existing = result.FirstOrDefault(r => r.Item1.StartPage == file.StartPage);

				if (existing == null)
				{
					result.Add(new ContentViewModelGroup() {Item2 = file });
				}
				else
				{
					existing.Item2 = file;
				}
			}

			this.GroupedContents = result.ToObservableCollection();
		}

		#endregion
	}

	public class ContentViewModelGroup
	{
		public ContentViewModel Item1
		{
			get;
			set;
		}

		public ContentViewModel Item2
		{
			get;
			set;
		}
	}
}
