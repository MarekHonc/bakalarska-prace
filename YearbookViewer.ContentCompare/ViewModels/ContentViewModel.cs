﻿using YearbookViewer.ContentCompare.ViewModels.Base;

namespace YearbookViewer.ContentCompare.ViewModels
{
	/// <summary>
	/// Model, reprezentující obsah.
	/// </summary>
	public class ContentViewModel : ViewModelBase
	{
		#region private variables

		private int startPage;
		private string name;

		#endregion

		/// <summary>
		/// Vrací nebo nastavuje první stránku obsahu.
		/// </summary>
		public int StartPage
		{
			get
			{
				return this.startPage;
			}
			set
			{
				this.startPage = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje název obsahu.
		/// </summary>
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Override Equals aby .NET bral v potaz hodnoty objektu.
		/// </summary>
		/// <remarks>Je zde aby se správně choval combobox, pri změně vybrané položky z kódu.</remarks>
		public override bool Equals(object obj)
		{
			if (obj is ContentViewModel model)
			{
				return model.StartPage == this.StartPage && model.Name.Equals(this.Name);
			}

			return base.Equals(obj);
		}

		/// <summary>
		/// Override GetHashCode aby .NET bral bral v potaz hodnoty objektu.
		/// </summary>
		/// <remarks>Je zde aby se správně choval combobox, pri změně vybrané položky z kódu.</remarks>
		public override int GetHashCode()
		{
			return this.StartPage + this.Name.GetHashCode();
		}
	}
}
