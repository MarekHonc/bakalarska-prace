﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace YearbookViewer.ContentCompare.ViewModels.Base
{
	/// <summary>
	/// Základní view model.
	/// </summary>
	public class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Vyvolá oznámení o změně (této) property.
		/// </summary>
		/// <param name="propertyName">Název property, která se změnila. Nemusí se předávat, pokud je voláno ze setteru property.</param>
		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			var handler = this.PropertyChanged;
			handler?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
