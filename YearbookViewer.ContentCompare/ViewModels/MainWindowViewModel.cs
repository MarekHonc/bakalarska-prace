﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YearbookViewer.Common;
using YearbookViewer.ContentCompare.Views;
using YearbookViewer.ContentCompare.ViewModels.Base;

namespace YearbookViewer.ContentCompare.ViewModels
{
	/// <summary>
	/// Hlavní View model aplikace.
	/// </summary>
	public class MainWindowViewModel : ViewModelBase
	{
		/// <summary>
		/// Vrací nebo nastavuje model s nastavením aplikace.
		/// </summary>
		public static SettingsViewModel Settings
		{
			get;
			set;
		}

		#region private variables

		private CurrentYearViewModel selectedYear;

		#endregion

		public MainWindowViewModel()
		{
			SettingsWindow.OnSettingChanged += (a, b) =>
			{
				RaisePropertyChanged(nameof(Years));
			};
		}

		/// <summary>
		/// Vrací nebo nastavuje seznam všech roků.
		/// </summary>
		public ObservableCollection<CurrentYearViewModel> Years
		{
			get
			{
				// Musím mít nastavení.
				if (Settings == null)
					return new ObservableCollection<CurrentYearViewModel>();

				// Výsledek.
				var result = new ObservableCollection<CurrentYearViewModel>();

				// Zjistím všechny roky.
				var collection = FileBrowser.GetAllChildrenDirectories(Settings.YearbooksPath);

				// Procházím.
				foreach (var year in collection)
				{
					// Regex pro detekci čísla.
					var yearMatch = Regex.Match(year.Name, @"\d+");

					// Pokud neobsahuje "r" nedělám nic.
					if (!year.Name.Contains("r") || !yearMatch.Success)
						continue;

					// Detetuji všechny potřebné prvky.
					var yearNumber = int.Parse(yearMatch.Value);
					var contentFile = Directory.EnumerateFiles(Settings.ContentPath).First(f => f.Contains(year.Name));
					var pathToImage = Directory.EnumerateFiles(year.FullName).First(f => f.Contains("inhalt"));

					// A přidám do výsledku.
					result.Add(new CurrentYearViewModel(yearNumber, pathToImage,
						contentFile, Settings.XlsxPath, "C", "D", "A"));
				}

				return result;
			}
		}

		/// <summary>
		/// Aktuálně zvolený rok.
		/// </summary>
		public CurrentYearViewModel SelectedYear
		{
			get
			{
				return this.selectedYear;
			}
			set
			{
				this.selectedYear = value;
				value.SetFileContents();

				RaisePropertyChanged();
			}
		}
	}
}
