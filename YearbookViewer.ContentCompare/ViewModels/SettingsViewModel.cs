﻿using YearbookViewer.Common.Extensions;
using YearbookViewer.ContentCompare.ViewModels.Base;

namespace YearbookViewer.ContentCompare.ViewModels
{
	/// <summary>
	/// Model s počátečním nastavením aplikace.
	/// </summary>
	public class SettingsViewModel : ViewModelBase
	{
		#region private variables

		private string yearbooksPath;
		private string contentPath;
		private string xlsxPath;

		#endregion

		public SettingsViewModel()
		{
			this.YearbooksPath = @"C:\Users\marek\Desktop\Rocenky_JJHS";
			this.ContentPath = @"C:\Users\marek\Desktop\obsah\Obsah_Rocenky_JJHS_Final";
			this.XlsxPath = @"C:\Users\marek\Desktop\obsah\Obsah_Inhalt_1891-1941_orig.xlsx";
		}

		/// <summary>
		/// Vrací nebo nastavuje cestu k ročenkám.
		/// </summary>
		public string YearbooksPath
		{
			get
			{
				return this.yearbooksPath;
			}
			set
			{
				this.yearbooksPath = value;

				RaisePropertyChanged();
				RaisePropertyChanged(nameof(this.EverythingSet));
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje cestu k obsahům.
		/// </summary>
		public string ContentPath
		{
			get
			{
				return this.contentPath;
			}
			set
			{
				this.contentPath = value;

				RaisePropertyChanged();
				RaisePropertyChanged(nameof(this.EverythingSet));
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje cestu k xslx souboru s obsahy.
		/// </summary>
		public string XlsxPath
		{
			get
			{
				return this.xlsxPath;
			}
			set
			{
				this.xlsxPath = value;

				RaisePropertyChanged();
				RaisePropertyChanged(nameof(this.EverythingSet));
			}
		}

		/// <summary>
		/// Vrací, zda-li jsou všechny položky vyplněné.
		/// </summary>
		public bool EverythingSet
		{
			get
			{
				return this.YearbooksPath.IsNotNullOrEmpty() && this.ContentPath.IsNotNullOrEmpty() && this.XlsxPath.IsNotNullOrEmpty();
			}
		}
	}
}
