﻿using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Globalization;
using System.Windows.Data;
using System;

namespace YearbookViewer.ContentCompare.Code
{
	/// <summary>
	/// Konvertor, pro zobrazení obrázku na základě jeho cesty ve filesystemu.
	/// </summary>
	public class PathToImageConverter : IValueConverter
	{
		/// <summary>
		/// Provede konvertování cesty (např. D://Image.jpg) přímo na obrázek, který muho zobrazit.
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (targetType == typeof(ImageSource) && value is string)
			{
				var str = value.ToString();
				var uri = new Uri(str, UriKind.RelativeOrAbsolute);

				return new BitmapImage(uri);
			}

			return value;
		}

		/// <summary>
		/// Nepotřebuji.
		/// </summary>
		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
