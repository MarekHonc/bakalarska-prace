﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace YearbookViewer.ContentCompare.Code
{
	/// <summary>
	/// Vlastní prvek, je to border, ve kterém lze zoomovat na jeho obsah.
	/// Border musí mít child element, pokud ho nemá vnikne výjimka.
	/// </summary>
	/// <remarks>
	/// Kód čerpán a upraven z:
	/// https://stackoverflow.com/a/6782715/14060365
	/// </remarks>
	public class ZoomBorder : Border
	{
		/// <summary>
		/// Konstanta označující krok pro přiblížení.
		/// </summary>
		private const double step = 0.2d;

		/// <summary>
		/// Konstatnta označující maximální oddálení.
		/// </summary>
		private const double minZoom = 0.4d;

		/// <summary>
		/// Konstanta označující maximální přiblížení.
		/// </summary>
		private const double maxZoom = 8;

		#region Private variables

		private UIElement child = null;
		private Point origin;
		private Point start;

		#endregion

		/// <summary>
		/// Vrací transformaci, pro daný <see cref="element"/> element.
		/// </summary>
		private TranslateTransform GetTranslateTransform(UIElement element)
		{
			var group = (TransformGroup)element.RenderTransform;
			var firstChild = group.Children.First(tr => tr is TranslateTransform);

			return (TranslateTransform)firstChild;
		}

		/// <summary>
		/// Vrací škálovaní, pro daný <see cref="element"/> element.
		/// </summary>
		private ScaleTransform GetScaleTransform(UIElement element)
		{
			var group = (TransformGroup)element.RenderTransform;
			var firstChild = group.Children.First(tr => tr is ScaleTransform);

			return (ScaleTransform)firstChild;
		}

		/// <summary>
		/// Vrací nebo nastavuje child element prvku.
		/// </summary>
		public override UIElement Child
		{
			get
			{
				return base.Child;
			}
			set
			{
				// Inicializuji prvek pouze pokud není již nastaven.
				if (value != this.Child)
					this.Initialize(value);

				base.Child = value;
			}
		}

		/// <summary>
		/// Inicializuje prvek pro zoom.
		/// Přidá všechny eventy, přidá si veškěeré "handlery" pro zvětšování atd...
		/// </summary>
		public void Initialize(UIElement element)
		{
			// Null hodnota je nežádoucí.
			if (element == null)
				throw new Exception("Zoom Border - argument element cannot be null!");

			// Nastavím.
			this.child = element;

			// Vytvořím skupinu pro transformaci.
			TransformGroup group = new TransformGroup();

			// Vytvořím škálování a transformaci.
			var scale = new ScaleTransform();
			var transform = new TranslateTransform();

			// Přidám do skupiny a do child elementu, abych později mohl hodnoty získávat.
			group.Children.Add(scale);
			group.Children.Add(transform);
			child.RenderTransform = group;
			// A počátek měnení je levý horní roh.
			child.RenderTransformOrigin = new Point(0.0, 0.0);

			// Provážu s eventy pro manipulaci.
			this.MouseWheel += MouseWheelMove;
			this.MouseLeftButtonDown += LeftMouseDown;
			this.MouseLeftButtonUp += LeftMouseUp;
			this.MouseMove += MouseHoldingAndMove;
			this.PreviewMouseRightButtonDown += new MouseButtonEventHandler(RightMouseClick);

			// Chci aby se mi držel ve svých hranicích.
			this.ClipToBounds = true;

		}

		/// <summary>
		/// Resetuje veškerý zoom, přiblížení i změnu pozice.
		/// </summary>
		public void Reset()
		{
			// Reseruji zoom.
			var scale = GetScaleTransform(child);
			scale.ScaleX = 1.0;
			scale.ScaleY = 1.0;

			// Resetuji změnu pozice.
			var transform = GetTranslateTransform(child);
			// Opět na počátek tj. levý horní roh.
			transform.X = 0.0;
			transform.Y = 0.0;
		}

		/// <summary>
		/// Přiblíží content uvnitř tohoto elementu.
		/// </summary>
		/// <param name="x">X souřadnice bodu, ke kterému chci přiblížit.</param>
		/// <param name="y">Y souřadnice bodu, ke kterému chci přiblížit.</param>
		/// <param name="zoom">Příznak, jestli se má přiblížit, nebo oddálit.</param>
		public void Zoom(double x, double y, bool zoom = true)
		{
			var scale = GetScaleTransform(child);
			var transform = GetTranslateTransform(child);
			
			// Zjistím krok -> chci přiblíži je pozitivní, jinak negatovní.
			double move = zoom ? step : (-1) * step;

			// Pokud jsem již mimo hranice -> neposouvám dál.
			// Pokud oddaluji, nejdu dál než je minim.
			if (!zoom && (scale.ScaleX <= minZoom || scale.ScaleY <= minZoom))
				return;

			// Pokud přibližuji, nepustím dál než je maximum
			if (zoom && (scale.ScaleX >= maxZoom || scale.ScaleY >= maxZoom))
				return;

			// Zjistím absolutní pozici pro přiblížení.
			var absoluteX = x * scale.ScaleX + transform.X;
			var absoluteY = y * scale.ScaleY + transform.Y;

			// Přidám do škálování.
			scale.ScaleX += move;
			scale.ScaleY += move;

			// Přidám do transformace.
			transform.X = absoluteX - x * scale.ScaleX;
			transform.Y = absoluteY - y * scale.ScaleY;
		}

		#region Events

		/// <summary>
		/// Event na pohyb kolečka myši.
		/// Přiblíží/oddálí okolo daného bodu.
		/// </summary>
		private void MouseWheelMove(object sender, MouseWheelEventArgs e)
		{
			// Přibližuji vůči aktuální pozici kurzoru.
			var relative = e.GetPosition(child);
			Zoom(relative.X, relative.Y, e.Delta > 0);
		}

		/// <summary>
		/// Event na zmáčknutí levého tlačítka myši.
		/// Začíná sledovat pohyb myši.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void LeftMouseDown(object sender, MouseButtonEventArgs e)
		{
			var transform = GetTranslateTransform(child);

			// Zečátkem sledování myši je její aktuální poloha -> efekt že obrázek se pohybuje tímto bodem.
			start = e.GetPosition(this);

			// Počátek transfomace je její aktuální poloha.
			origin = new Point(transform.X, transform.Y);

			this.Cursor = Cursors.Hand;

			// Zapnu sledování.
			child.CaptureMouse();
		}

		/// <summary>
		/// Event na uvolnení levého tlačítka myši.
		/// Zastaví sledování pohybu myši.
		/// </summary>
		private void LeftMouseUp(object sender, MouseButtonEventArgs e)
		{
			// Zastavím sledování.
			child.ReleaseMouseCapture();
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// Event na kliknutí pravým tlačítkem.
		/// Resetuje zoom.
		/// </summary>
		private void RightMouseClick(object sender, MouseButtonEventArgs e)
		{
			this.Reset();
		}

		/// <summary>
		/// Event na sledování pobyhu myši.
		/// Hýbe rovnou i s child elementem.
		/// </summary>
		private void MouseHoldingAndMove(object sender, MouseEventArgs e)
		{
			// Pouze pokud mám zapnuté sledování.
			if (child.IsMouseCaptured)
			{
				// Aktuální transfomace zůstová zachována a posouvám od ní.
				var tranform = GetTranslateTransform(child);

				// Vytvořím si vektor, reprezentující pohyb myši.
				// Mám pak vektor rekrezentující relativní pohyb myši.
				var vector = start - e.GetPosition(this);

				// A x musím posunout o x pozici vektoru, to samé y.
				tranform.X = origin.X - vector.X;
				tranform.Y = origin.Y - vector.Y;
			}
		}

		#endregion
	}
}
