﻿using System;
using System.Windows;
using System.ComponentModel;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;
using YearbookViewer.ContentCompare.ViewModels;

namespace YearbookViewer.ContentCompare.Views
{
	/// <summary>
	/// Interaction logic for SettingsWindow.xaml
	/// </summary>
	public partial class SettingsWindow : Window
	{
		private bool killApp = true;

		public SettingsWindow()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Akce volaná před zavřením okna.
		/// </summary>
		protected override void OnClosing(CancelEventArgs e)
		{
			if (this.killApp)
			{
				// Zavřu celou aplikaci.
				Application.Current.Shutdown();
			}
			else
			{
				base.OnClosing(e);
			}
		}

		/// <summary>
		/// Načte xlsx soubor.
		/// </summary>
		private void LoadXlsx(object sender, MouseButtonEventArgs e)
		{
			var dialog = new OpenFileDialog();

			dialog.Filter = string.Format("Excel 2007 - 365 (*{0})|*{0}", ".xlsx");

			if (dialog.ShowDialog() == true)
				((TextBox)sender).Text = dialog.FileName;
		}

		/// <summary>
		/// Event, jenž upozorňuje na změnu nastavení.
		/// </summary>
		public static event EventHandler OnSettingChanged;

		/// <summary>
		/// Nastavení je hotovo - toto okno zavírám a otevírám hlavní.
		/// </summary>
		private void Done(object sender, RoutedEventArgs e)
		{
			// Doplnění závislostí.
			MainWindowViewModel.Settings = (SettingsViewModel)this.DataContext;
			OnSettingChanged?.Invoke(this, EventArgs.Empty);

			// Zavření okna.
			this.killApp = false;
			this.Close();
		}
	}
}
