﻿using System;
using System.IO;
using System.Linq;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common;

namespace YearbookViewer.DL.Implementations
{
	/// <summary>
	/// Třída poskytující hodnoty do filtrů pomocí procházení file systému.
	/// </summary>
	public class SearchFilterFromFilesProvider : ISearchFilterValueProvider, IFilePathRequired
	{
		private string basePath;

		/// <summary>
		/// Vrací všechny roky které mám dostupné.
		/// </summary>
		public string[] GetTopLevelFilter()
		{
			if (this.basePath.IsNullOrEmpty())
				return Array.Empty<string>();

			// Vracím všechny podřazené adresáře.
			return FileBrowser.GetAllChildrenDirectoriesNames(basePath);
		}

		/// <summary>
		/// Vrací obsahy ročenek pro daný ročník.
		/// </summary>
		/// <param name="topLevelFilter">Název složky, ve které se nachází sekundární filter.</param>
		public string[] GetSecondaryFilter(string topLevelFilter)
		{
			if (topLevelFilter.IsNullOrEmpty())
				return Array.Empty<string>();

			// A vrátím všechny potomky ve složce.
			return FileBrowser.GetAllChildrenDirectoriesNames(Path.Combine(basePath, topLevelFilter));
		}

		/// <summary>
		/// Vrací názvy všech stránek, podle předaného top level filtru a sekundárního filtru.
		/// </summary>
		/// <param name="topLevelFilter">Název složky, ve které se nachází sekundární filter.</param>
		/// <param name="secondaryFilter">Název sekundární složky, ve které se nachází již obrázky k zobrazení.</param>
		public string[] YearBookNames(string topLevelFilter, string secondaryFilter)
		{
			if (topLevelFilter.IsNullOrEmpty())
				return Array.Empty<string>();

			// Výsledné názvy stránek
			string[] result;

			var compatibleExtensions = Constants.CompatibleTextExtensions;

			// Výsledkem jsou všechny textové soubory, pokud mám vybrán pouze hlavní filter.
			if (secondaryFilter.IsNullOrEmpty())
			{
				// Vezmu obsahy všech složek.
				result = FileBrowser.GetAllChildrenDirectories(Path.Combine(basePath, topLevelFilter))
					.SelectMany(d => FileBrowser.GetAllTextFilesNames(d.FullName, compatibleExtensions))
					.ToArray();
			}
			// Jinak bere v potaz i sekundární filter.
			else
			{
				var path = Path.Combine(basePath, topLevelFilter, secondaryFilter);
				result = FileBrowser.GetAllTextFilesNames(path, compatibleExtensions);
			}

			return result;
		}

		/// <summary>
		/// Nastavení pracovních cest.
		/// </summary>
		/// <param name="paths">Pole pracovních est, první je k souborům, druhá k obsahům.</param>
		public void SetPaths(params string[] paths)
		{
			this.basePath = paths[0];
		}

		/// <inheritdoc cref="ISearchFilterValueProvider.Instance"/>.
		public ISearchFilterValueProvider Instance => this;
	}
}
