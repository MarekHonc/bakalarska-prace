﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.DL.Implementations.DataImplemetations;

namespace YearbookViewer.DL.Implementations
{
	/// <summary>
	/// Vyhodnocovač výrazů, používající regex.
	/// </summary>
	public class RegexExpressionEvaluator : IExpressionEvaluator, IExpressionAcceptable
	{
		/// <summary>
		/// Vrací jestli je daný regex výraz platný.
		/// </summary>
		/// <param name="expr">Výraz, jehož platnost se kontroluje.</param>
		public bool IsValid(string expr)
		{
			if (string.IsNullOrWhiteSpace(expr))
				return false;

			try
			{
				Regex.Match("", expr);
			}
			catch (ArgumentException)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Rozdělí string podle daného výrezu.
		/// </summary>
		/// <param name="text">Text, který bude výrazem rozdělen.</param>
		/// <param name="expr">Výraz podle kterého dělí daný text, pokud výraz není platný vyhodí výjimku.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		/// <returns>Pole stringů, které zniklo na základě rozdělení.</returns>
		public string[] Split(string text, string expr, bool isExpression = false)
		{
			expr = PreProcessExpression(expr, isExpression);

			var regex = new Regex($@"({expr})", RegexOptions.IgnoreCase);

			return regex.Split(text);
		}

		/// <summary>
		/// Vrací jestli se v textu vyskytuje shoda s vyhledávaným výrazem.
		/// </summary>
		/// <param name="text">Text, který bude kontrolován.</param>
		/// <param name="expr">Výraz podle který se v textu hledá.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		public bool IsMatch(string text, string expr, bool isExpression = false)
		{
			expr = PreProcessExpression(expr, isExpression);

			return Regex.IsMatch(text ?? string.Empty, expr, RegexOptions.IgnoreCase);
		}

		/// <summary>
		/// Vrací všechny shody výrazu s textem.
		/// </summary>
		/// <param name="text">Text, který bude kontrolován.</param>
		/// <param name="expr">Výraz podle který se v textu hledá.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		/// <returns>Koleckce všech matchů, které se v textu nachází.</returns>
		public ICollection<IMatchData> Matches(string text, string expr, bool isExpression = false)
		{
			expr = PreProcessExpression(expr, isExpression);
			var results = new List<IMatchData>();

			foreach(Match match in Regex.Matches(text, expr, RegexOptions.IgnoreCase))
			{
				results.Add(new RegexMatch() { Index = match.Index, Length = match.Length });
			}

			return results;
		}

		/// <summary>
		/// Předzpracuje výraz, kontroluje jeho platnost, pokud není platný vythodí výjimku.
		/// Vyhodnocuje i jestli se má escapovat či nikoliv.
		/// </summary>
		private string PreProcessExpression(string expr, bool isExpression)
		{
			if (!this.IsValid(expr))
				throw new ArgumentException($"Given regex expression {expr} is not valid!!");

			if (!isExpression)
				expr = Regex.Escape(expr);

			return expr;
		}

		/// <inheritdoc cref="IExpressionEvaluator.Instance"/>.
		public IExpressionEvaluator Instance => this;
	}
}
