﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using YearbookViewer.Common;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.DL.Implementations.DataImplemetations;
using YearbookViewer.Readers;

namespace YearbookViewer.DL.Implementations
{
	/// <summary>
	/// Třída poskytující data pomocí procházení souborů ve file systému.
	/// </summary>
	public class YearBookContentDataFromFilesProvider : IContentDataProvider, IFilePathRequired
	{
		private string basePath;
		private readonly string[] compatibleExtensions = Constants.CompatibleTextExtensions;

		/// <summary>
		/// Vrací daná počet ročenek.
		/// </summary>
		/// <param name="filters">Filtry, pro které vrací daný počet ročenek.</param>
		/// <param name="count">Počet, kolik jich chci.</param>
		/// <param name="offset">Počet přeskočených ročenek.</param>
		/// <returns>Kolekci nalezených ročenek.</returns>
		public IReadOnlyCollection<IContentData> GetCount(IFilterData filters, int count, int? offset = null)
		{
			// Vytvořím si regex.
			var expression =  filters.IsExpression ? filters.SearchTerm : Regex.Escape(filters.SearchTerm);
			var finalRegex = new Regex(expression, RegexOptions.IgnoreCase);

			IEnumerable<ContentDataFromFiles> collection;

			// Pokud chci hledat globálně a nebo nemám vybraný ani rok.
			if (filters.UseGlobalSearch || filters.TopLevelFilter.IsNullOrEmpty())
			{
				// Stáhnu si všechny složky.
				var folders = FileBrowser.GetAllChildrenDirectories(this.basePath);

				// A začnu "procházet" vše.
				collection = folders
					.SelectMany(f => FileBrowser.GetAllChildrenDirectories(f.FullName))
					.SelectMany(f => FileBrowser.GetAllTextFiles(f.FullName, compatibleExtensions))
					.Select(f => new ContentDataFromFiles(Path.GetFileName(f), f, Path.GetFileName(Path.GetDirectoryName(Path.GetDirectoryName(f)))));
			}
			else
			{
				// Pokud mám vybraný sekundární filtr -> složím si cestu rovnou z něj.
				if (filters.SecondaryFilter.IsNotNullOrEmpty())
				{
					var path = Path.Combine(this.basePath, filters.TopLevelFilter, filters.SecondaryFilter);

					// A vytáhnu přesně podle složky.
					collection = FileBrowser.GetAllTextFiles(path, compatibleExtensions)
						.Select(f => new ContentDataFromFiles(Path.GetFileName(f), f, filters.TopLevelFilter));
				}
				// Jinak beru podle hlavního filtru.
				else
				{
					// Tj. vytáhnu si složky.
					var folders = FileBrowser.GetAllChildrenDirectories(Path.Combine(this.basePath, filters.TopLevelFilter));

					// A ze všech pod složek
					collection = folders.SelectMany(f => FileBrowser.GetAllTextFiles(f.FullName, compatibleExtensions))
						.Select(f => new ContentDataFromFiles(Path.GetFileName(f), f, filters.TopLevelFilter));
				}
			}

			// Nakonec stáhnu výsledky ze souborů, na kterých se pohybuji.
			var result = collection
				.Where(c => finalRegex.IsMatch(c.ImageText))
				.Skip(offset ?? 0)
				.Take(count)
				.ToList();

			return result;
		}

		/// <summary>
		/// Vrátí první ročenku, která vyhovuje filtrům.
		/// </summary>
		public IContentData GetFirst(IFilterData filters)
		{
			// Nemám hlavní filtr -> vracím nic.
			if (filters.TopLevelFilter.IsNullOrEmpty())
				return null;

			// Cesta k souboru.
			string filePath = null;

			// Mám název stránky.
			if (filters.PageName.IsNotNullOrEmpty())
			{
				// Pokud mám i sekundární filtr -> nemusím vyhledávat.
				if (filters.SecondaryFilter.IsNotNullOrEmpty())
				{
					filePath = Path.Combine(this.basePath, filters.TopLevelFilter, filters.SecondaryFilter, filters.PageName);
				}
				else
				{
					// Jinak musím holt projít vše...
					foreach (var directory in FileBrowser.GetAllChildrenDirectories(Path.Combine(this.basePath, filters.TopLevelFilter)))
					{
						// Vytáhnu všechny soubory.
						var files = FileBrowser.GetAllTextFilesNames(directory.FullName, compatibleExtensions);

						// A pokud jsem nenalezl shodu - pokračuji.
						if (!files.Contains(filters.PageName))
							continue;

						// Nalezl jsem shodu - je zbytečné pokračovat - loop ruším.
						filePath = Path.Combine(directory.FullName, filters.PageName);
						break;
					}
				}
			}
			// Pokud mám sekundární filtr - získám podle něj.
			else if(filters.SecondaryFilter.IsNotNullOrEmpty())
			{
				filePath = FirstFileBySecondaryFilter(filters.TopLevelFilter, filters.SecondaryFilter);
			}
			// Jinak získám první soubor z hlavního filtru.
			else
			{
				// Získám první sekundární složku.
				var firstFolder = FileBrowser.GetAllChildrenDirectoriesNames(Path.Combine(this.basePath, filters.TopLevelFilter))[0];
				filePath = FirstFileBySecondaryFilter(filters.TopLevelFilter, firstFolder);
			}

			// Vrátím.
			return new ContentDataFromFiles(Path.GetFileName(filePath), filePath, filters.TopLevelFilter);
		}

		/// <summary>
		/// Uložím do souboru nový text.
		/// </summary>
		public bool Save(IFilterData filters, string newText)
		{
			var file = (ContentDataFromFiles)GetFirst(filters);

			// Získám třídu pro zápis.
			var reader = CustomTextReader.GetReader(Path.GetExtension(file.PagePath));

			// Uložím a vracím, zda-li se povedlo.
			return reader.Write(file.PagePath, newText, file.ImageText);
		}

		/// <summary>
		/// Nastavení pracovních cest.
		/// </summary>
		/// <param name="paths"></param>
		public void SetPaths(params string[] paths)
		{
			this.basePath = paths[0];
		}

		/// <inheritdoc cref="IContentDataProvider.Instance"/>.
		public IContentDataProvider Instance => this;

		#region private helpers

		/// <summary>
		/// Vrací první stránku, podle kombinace hlavního a sekundárního filtru.
		/// </summary>
		private string FirstFileBySecondaryFilter(string topLevelFilter, string secondaryFilter)
		{
			// Poskládám složku, ze které získám data.
			var folderPath = Path.Combine(this.basePath, topLevelFilter, secondaryFilter);

			// Získám první stránku.
			var firstFile = FileBrowser.GetAllTextFilesNames(folderPath, compatibleExtensions)[0];

			// A nakonec získám cestu k stránce
			return Path.Combine(folderPath, firstFile);
		}

		#endregion
	}
}
