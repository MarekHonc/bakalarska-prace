﻿using System.IO;
using System;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.Readers;
using YearbookViewer.Readers.Base;

namespace YearbookViewer.DL.Implementations.DataImplemetations
{
	/// <summary>
	/// Jestli ve finále budu používat vyhledávání v souborech, bude to chtít zhězčit kód, todle je zatím pouze pro dolazení GUI.
	/// </summary>
	public class ContentDataFromFiles : IContentData
	{
		#region Private variables

		private ITextFileReader reader;
		private readonly string pageName;
		private readonly string pagePath;
		private readonly string imagePath;
		private readonly string year;

		#endregion

		/// <summary>
		/// Vytvoří nový objekt, pro zobrazení souboru spjatého obrázkem.
		/// </summary>
		internal ContentDataFromFiles(string pageName, string pagePath, string year)
		{
			this.pageName = pageName;
			this.pagePath = pagePath;
			this.year = year;

			// Zjistím koncovku souboru.
			var extension = Path.GetExtension(pagePath);

			// Vytáhnu třídu, která soubor umí číst.
			this.reader = CustomTextReader.GetReader(extension);

			// Ještě zjistím, kde mám obrázek k souboru.
			var pathToImage = CustomTextReader.GetImageForText(pagePath);
			this.imagePath = pathToImage;

			// Pokud obrázek opravdu existuje.
			if (this.imagePath.IsNullOrEmpty())
				throw new Exception($"No image data found for file '{pagePath}'");
		}

		/// <summary>
		/// Vrací název stránky.
		/// </summary>
		public string PageName => this.pageName;

		/// <summary>
		/// Vrací cestu, na které se nachází obrázek.
		/// </summary>
		public string ImageUrl => this.imagePath;

		/// <summary>
		/// Vrací text, které se na obrázku nachází.
		/// </summary>
		public string ImageText => string.Join(Environment.NewLine, this.reader.Read(this.pagePath));

		/// <summary>
		/// Vrací rok, ze kterého je text.
		/// </summary>
		public string Year => this.year;

		/// <summary>
		/// Vrací text, který se dá použít pro zvýrazňování.
		/// </summary>
		public string ImageTextForHighlight => this.ImageText;

		/// <summary>
		/// Vrací cestu přímo k souboru.
		/// </summary>
		public string PagePath => this.pagePath;
	}
}
