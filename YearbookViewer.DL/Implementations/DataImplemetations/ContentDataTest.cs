﻿using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.DL.Implementations.DataImplemetations
{
	internal class ContentDataTest : IContentData
	{
		private int number;

		public ContentDataTest(int number)
		{
			this.number = number;
		}

		public string PageName => "Jméno 1";
		public string PagePath { get; }

		public string ImageUrl => @"D:\Rocenky_JJHS\r1891\r1891_s0" + number + ".jpg";

		public string ImageText => @"«Emile« die Stelle: ,,Vo»vage1- Er piec1 a’est- v0yag-er c0mme Thal-Es.
Pl;-icon, Pythiig·01·e.«·1) auf das Titelblatt.Auch Werthers Leiden wird
einmal angeführt.Für die Thier- und Pflanzeuwe«lt des Riesengebirges,
sowie für dessen volkswirtschaftliche Zustände hat er ein aufmerksames Auge.
Seine Reise begann von Prag ans a1u 2l. August 1796 und
endete am 18. September desselben Jahres.Er schildert sie in 27
Briefeu.Im ersten, von Prag ans datiert, beschreibt er den panischen
Schrecken der Einwohner, welcher sie vor den angeblich anrückenden
Franzosen (es war im ersten Coalitionskriege) ergriff, sowie deren kopf-
lose Flucht. »Der Schreckeu,« bemerkt er, ,, hat sich auf alle Alterselassen
und Geschlechter erstreckt, 1md auch die Schönen Prags seien im Begriffe
die Stadt zu verlassen, damit, fügt er boshaft hinzu, der Feind ihnen
nicht ranbe, was sie ihren Geliebten längst gegeben.« Der zweite Brief
ist von Kosn1auos datiert.Er schildert darin, wie die Straße mit einer
Menge von Fracht-Wagen, beladen mit Kisten und Koffern der Flüch-
tenden, bedeckt waren, wie er aber selbst als Flüchtling angesehen wurde.
Den Weg bis Kosmanos über Brandeis, Altbnnzlau 11nd Benatek findet
er ohne jedes Interesse, wegen des furchtbaren Staubes beschwerlich;
		die böhmischen Dörfer mit ihren schmntzigen, verfallenen Häuschen
		seien ebenfalls keine Angenweide.Er erwähnt der Cotton-Fabrik von
Kosmanos, die er jedoch nicht besichtigte.Der dritte Brief nun ist be-
reits von Reicheuberg am 27. August datiert.Mit Befriedigung schreibt
er, dass die Seeuerie sich vortheilhaft in dem Maße ändere, wie man
dem Gebirge sich nähere, wie die Gegend schon von Kosmanos an schöuer
werde, wie das Riesen- und Jsergebirge, sowie der hohe ,, Geschken« in
der Ferne sichtbar seien. Er klagt jedoch über die abscheuliche Straße,
welche er mit dem Bett eines reißenden Waldstro1nes vergleicht, soviel
Steine lagen darauf, nnd so lebensgefährlich war darauf die Fahrt.
Trotzdem war sie die Hauptstraße, da sie nach Reichenberg führte; man
kann sich daher vorstellen, wie die damaligen Neben- und Landstraßen
beschaffen gewesen fein mögen.Seiner Aufmerksamkeit entgieng die in
dieser Gegend begiunende Holzverschwendnng nicht, die sich besonders an
den Garten-, Feld- nnd Hofz·cinnen vorfand. Von Mi’1nchengrätz nahm
sie gegen Liebeuan immer zu.Latten, welche in einander gelegt waren,
wechselteu mit Brettern und diese schließlich mit ganzen« Baumstämmen«
ab, obwohl man damals bereits über Holzmangel Klage führte. Er
schlägt daher mit Recht lebende Zäuue als weniger kostspielig und schöner
vor.Jemehr er sich den deutschen Ortschaften näherte, desto auffallender
trat ihm die Verschöuernng der Dörfer entgegen, selbst.auch jener, welche
von »Stockböhmen«, wie er die Tschechen nennt, bewohnt waren: --Hölzerne,
gut und nett gebante Häuser treten an die Stelle der-elenden, mit Stroh
gedeckten Lehn1hütten, die O-bstgärteu werden zahlreicher, die Kinder
waren nicht mehr halb oder ganz nackt nnd voll Schmutz, sondern rein-
lich nnd wohlgekleidet.Die kleinen runden Hütchen der Erwachsenen
verloren sich, die lichtblaue Farbe der Kleider wurde immer mehr von der
dunkelblauen verdrängt, die weißen ledernen Hosen verwandelten sich in
schwarze, bloude Haare und große blaue Augen wurden häufiger, schwarze
«) »Wenn man zu Fuß reist, reist man wie Thale-s, Plato, Phtl)agnras.«";

		public string Year => throw new System.NotImplementedException();

		public string ImageTextForHighlight => throw new System.NotImplementedException();
	}
}
