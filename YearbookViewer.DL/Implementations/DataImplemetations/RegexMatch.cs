﻿using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.DL.Implementations.DataImplemetations
{
	/// <summary>
	/// Třada reprezentující shodu s regexem.
	/// </summary>
	public class RegexMatch : IMatchData
	{
		/// <summary>
		/// Vrací nebo nastavuje index, na kterém se nachází shoda.
		/// </summary>
		public int Index
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje délku (veliksot) shody.
		/// </summary>
		public int Length
		{
			get;
			set;
		}
	}
}
