﻿namespace YearbookViewer.Readers.TesseractOCR
{
	/// <summary>
	/// Seznam všech podporovaných jazyků OCR.
	/// </summary>
	public enum OCRLanguage
	{
		/// <summary>
		/// Čeština
		/// </summary>
		ces,

		/// <summary>
		/// Švabach
		/// </summary>
		deu_frak
	}
}
