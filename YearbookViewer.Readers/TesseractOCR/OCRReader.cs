﻿using System;
using System.Diagnostics;
using System.IO;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.Readers.TesseractOCR
{
	/// <summary>
	/// Třída, která se stará o čtení dat z obrázku.
	/// </summary>
	public class OCRReader
	{
		private static string baseScriptPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TesseractOCR", "Scripts");
		private static string currentName = "current";

		/// <summary>
		/// Vrací cestu k souboru, ve kterém jsou uloženy souřadnice všech klíčových slov.
		/// </summary>
		public static void ReadImage(string imagePath, OCRLanguage language, Action<string> successCallback)
		{
			var resultPath = Path.Combine(baseScriptPath, currentName);
			var path = Path.Combine(baseScriptPath, "readimage.bat");
			
			ExecuteCommand(
				path,
				string.Join(" ", imagePath.Enquote(), resultPath.Enquote(), language), 
				waitForEnd: false,
				() => successCallback(resultPath + ".tsv")
			);
		}

		/// <summary>
		/// Vrací, zda-li se může použít OCR čtečka.
		/// </summary>
		public static bool CanBeUsed()
		{
			var path = Path.Combine(baseScriptPath, "version.bat");
			var exitCode = ExecuteCommand(path);

			return exitCode == 0;
		}

		/// <summary>
		/// Vykoná *.bat soubor a vrátí kód, se kterém skončil.
		/// </summary>
		private static int ExecuteCommand(string command, string arguments = null, bool waitForEnd = true, Action callback = null)
		{
			if (arguments != null)
			{
				command += $" {arguments}";
			}

			var processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
			processInfo.CreateNoWindow = true;
			processInfo.UseShellExecute = false;
			processInfo.RedirectStandardError = true;
			processInfo.RedirectStandardOutput = true;

			var process = Process.Start(processInfo);
			process.BeginOutputReadLine();

			if (waitForEnd)
			{
				process.BeginErrorReadLine();
				process.WaitForExit();

				return process.ExitCode;
			}
			else
			{
				process.OutputDataReceived += (sender, args) =>
				{
					callback();
				};

				return -1;
			}
		}
	}
}
