﻿using System;
using System.Collections.Generic;
using System.IO;
using YearbookViewer.Common;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Readers.Base;
using YearbookViewer.Common.Enums;
using YearbookViewer.Readers.Implementations;

namespace YearbookViewer.Readers
{
	/// <summary>
	/// Knihovní třída, starající se o poskytnutí správné čtečky souborů, pro konkrétní soubor.
	/// </summary>
	public static class CustomTextReader
	{
		/// <summary>
		/// Slovník, se všemi čtečkami souborů (aby se nemuseli pořád dokola vytvářet).
		/// </summary>
		private static readonly Dictionary<TextFileType, ITextFileReader> readers =
			new Dictionary<TextFileType, ITextFileReader>()
			{
				{ TextFileType.Txt, new TxtFileReader() },
				{ TextFileType.Pdf, new PdfFileReader() },
				{ TextFileType.Docx, new DocxFileReader() }
			};

		/// <summary>
		/// Vrací kolekci všech podporovaných typů souborů.
		/// </summary>
		private static readonly ImageFileType[] imageTypes = Constants.SupportedImageTypes;

		/// <summary>
		/// Vrací konkrétní čtečku souboru, které lze použít pro daný teb souboru.
		/// Pokud pro danou koncovku neexistuje žádná čtečka, vrací <code>null</code>.
		/// </summary>
		/// <param name="fileExtension">Koncovka souboru, se kterým chci pracovat.</param>
		/// <returns>Rozhraní, které pracuje s konkrétním souborem.</returns>
		public static ITextFileReader GetReader(string fileExtension)
		{
			// Nejdříve odstraním všechny tečky.
			fileExtension = fileExtension.Replace(".", string.Empty);

			// Až poté parsuji enum.
			if (Enum.TryParse<TextFileType>(fileExtension, true, out var type))
			{
				return GetReader(type);
			}

			throw new Exception($"File type '{fileExtension}' is not supported");
		}

		/// <summary>
		/// Vrací konkrétní čtečku souboru, které lze použít pro daný teb souboru.
		/// </summary>
		/// <param name="fileType">Typ souboru, se kterým chci pracovat.</param>
		/// <returns>Rozhraní, které pracuje s konkrétním souborem.</returns>
		public static ITextFileReader GetReader(TextFileType fileType)
		{
			return readers[fileType];
		}

		/// <summary>
		/// Vrací cestu, na které se nachází obrázek k požadovanému textu.
		/// </summary>
		/// <param name="pathToTextFile">Cesta, na které se nachází textový popis.</param>
		/// <returns>Cestu, na které byl dohledám obrázek, null pokud dohledán nebyl.</returns>
		public static string GetImageForText(string pathToTextFile)
		{
			// Zjistím koncovku.
			var extension = Path.GetExtension(pathToTextFile);

			// A hledám soubor se stejným název, který je obrázkem.
			foreach (var type in imageTypes)
			{
				var path = pathToTextFile.Replace(extension, $".{type.ToLower()}");

				// Obrázek jsem dohledal -> vracím.
				if (File.Exists(path))
					return path;
			}

			return null;
		}
	}
}
