﻿using System.Collections.Generic;
using System.Linq;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace YearbookViewer.Readers.ImportGrouping
{
	/// <summary>
	/// Třída získání všech požadovaných skupin dat ze souboru.
	/// </summary>
	public class ImportGroupingProvider
	{
		/// <summary>
		/// Vrací slovník se všemi požadovanými skupinami, klíčem je složka a hodnotou list všech rozdělení podle čísla stránek.
		/// </summary>
		public static Dictionary<string, List<GroupingModel>> GetGroups(string fileName,
			string xlsxYearColumn = "C", string xlsxPageColumn = "D", string xlsxNameColumn = "A")
		{
			var result = new Dictionary<string, List<GroupingModel>>();

			// Otevřu soubor pro čtení.
			using (var doc = SpreadsheetDocument.Open(fileName, false))
			{
				// První list.
				var workbookPart = doc.WorkbookPart;
				var worksheetPart = workbookPart.WorksheetParts.First();
				var sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

				// Tabulka s textovými hodnotami.
				var stringTable = workbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();

				// Nemám tabulku stringů - nemusím nic dělat, stejně nic nezískám.
				if (stringTable == null)
					return result;

				// Všechny řádky.
				foreach (var row in sheetData.Elements<Row>())
				{
					// Hodnoty pro přidání do výsledku.
					var key = string.Empty;
					var page = -1;
					var nameIndex = -1;

					// Projedu všechny buňky.
					foreach (var cell in row.Elements<Cell>())
					{
						if (cell.CellReference.Value.Contains(xlsxNameColumn))
						{
							nameIndex = int.Parse(cell.InnerText);
						}
						else if (cell.CellReference.Value.Contains(xlsxPageColumn))
						{
							page = int.Parse(cell.InnerText);
						}
						else if (cell.CellReference.Value.Contains(xlsxYearColumn))
						{
							key = cell.InnerText;
						}
					}

					// Nedohledal jsem stránku.
					if (page < 0)
					{
						continue;
					}

					// Vytvořím model.
					var model = new GroupingModel(page, stringTable.SharedStringTable.ElementAt(nameIndex).InnerText.Trim());

					// Přidám do výsledku.

					// Pokud ještě ve slovníku není - přidám.
					if (!result.TryGetValue(key, out var list))
					{
						result[key] = new List<GroupingModel>();
					}

					// Přidám do výsledné kolekce.
					result[key].Add(model);
				}
			}

			// Seřadím záznamy a doplním konce.
			foreach (var key in result.Keys.ToList())
			{
				var value = result[key].OrderBy(o => o.StartPage).ToArray();

				// Naplním i konečné stránky.
				for (var i = 0; i < value.Length - 1; i++)
				{
					value[i].EndPage = value[i + 1].StartPage - 1;
				}

				// Vrátím do výsledku.
				result[key] = value.ToList();
			}

			return result;
		}
	}
}
