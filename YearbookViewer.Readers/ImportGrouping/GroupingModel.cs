﻿namespace YearbookViewer.Readers.ImportGrouping
{
	/// <summary>
	/// Model obsahující informace o seskupení dat.
	/// </summary>
	public class GroupingModel
	{
		public GroupingModel(int startPage, string name)
		{
			this.StartPage = startPage;
			this.Name = name;
		}

		/// <summary>
		/// Vrací počáteční stránku na které začíná obsah.
		/// </summary>
		public int StartPage
		{
			get;
		}

		/// <summary>
		/// Vrací nebo nastavuje stránku, na které končí obsah. Pokud je null, obsah nekončí.
		/// </summary>
		public int? EndPage
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací název obsahu.
		/// </summary>
		public string Name
		{
			get;
		}

		public override string ToString()
		{
			return $"[{this.StartPage.ToString().PadLeft(3, '0')}]" + this.Name;
		}
	}
}
