﻿using System.IO;
using YearbookViewer.Common.Enums;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Readers.Base;

namespace YearbookViewer.Readers.Implementations
{
	/// <summary>
	/// Třída, umožňující čtení a zápis do souboru typu *.txt (<see cref="TextFileType.Txt"/>).
	/// </summary>
	internal class TxtFileReader : ITextFileReader
	{
		/// <inheritdoc cref="ITextFileReader.Read"/>.
		public string[] Read(string path)
		{
			return File.ReadAllLines(path);
		}

		/// <inheritdoc cref="ITextFileReader.Write"/>.
		public bool Write(string path, string newContent, string oldContent = null)
		{
			// Pokud mám starý text, vytvářím zálohu.
			if (oldContent.IsNotNullOrEmpty())
			{
				try
				{
					File.WriteAllText(path.Replace($".{TextFileType.Txt.ToLower()}", ".bak"), oldContent);
				}
				catch
				{
					return false;
				}
			}

			// Při zápisu do souboru se může cokoliv pokazit.
			try
			{
				File.WriteAllText(path, newContent);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}
