﻿using System.IO;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Layout;
using iText.Layout.Element;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Readers.Base;

namespace YearbookViewer.Readers.Implementations
{
	/// <summary>
	/// Třída, umožňující čtení a zápis do souboru typu *.pdf (<see cref="TextFileType.Pdf"/>).
	/// </summary>
	internal class PdfFileReader : ITextFileReader
	{
		/// <inheritdoc cref="ITextFileReader.Read"/>.
		public string[] Read(string path)
		{
			string[] result;

			// Otevřu dokument pro čtení.
			using (var document = new PdfDocument(new PdfReader(path)))
			{
				// Zjistím si počet stránek.
				var pages = document.GetNumberOfPages();

				// Vytvořím si kolekci, zde 1 stránka = 1 položka v poli.
				result = new string[pages];

				// A pro každou stránku získám text.
				for (var i = 1; i <= pages; ++i)
				{
					var page = document.GetPage(i);
					result[i - 1] = PdfTextExtractor.GetTextFromPage(page);
				}
			}

			return result;
		}

		/// <inheritdoc cref="ITextFileReader.Write"/>.
		public bool Write(string path, string newContent, string oldContent = null)
		{
			// Nejdřív záloha.
			try
			{
				// Pokud mám vytvořit zálohu -> jenom soubor přejmenuji.
				if (oldContent.IsNotNullOrEmpty())
				{
					var bakFile = path.Replace(".pdf", ".bak");

					if (File.Exists(bakFile))
						File.Delete(bakFile);

					File.Copy(path, bakFile);
				}
			}
			catch
			{
				return false;
			}

			try
			{
				// Otevřu pdf pro zápis.
				using (var writer = new Document(new PdfDocument(new PdfWriter(path))))
				{
					// Nejdřív všechno smáznu.
					for (var i = 1; i <= writer.GetPdfDocument().GetNumberOfPages(); ++i)
					{
						writer.GetPdfDocument().RemovePage(i);
					}

					// A pak přidám.
					var paragraph = new Paragraph(newContent);
					writer.Add(paragraph);
				}

				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}