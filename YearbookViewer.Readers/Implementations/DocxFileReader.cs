﻿using System;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Readers.Base;

namespace YearbookViewer.Readers.Implementations
{
	/// <summary>
	/// Třída, umožňující čtení a zápis do souboru typu *.docx (<see cref="TextFileType.Docx"/>).
	/// </summary>
	internal class DocxFileReader : ITextFileReader
	{
		/// <inheritdoc cref="ITextFileReader.Read"/>.
		public string[] Read(string path)
		{
			string[] result;

			// Otevřu word pro čtení.
			using (var word = WordprocessingDocument.Open(path, isEditable: false))
			{
				// Vytáhnu všechny odstavce.
				var paragraphs = word.MainDocumentPart.Document.Body.Descendants<Paragraph>();

				// Stáhnu všechny texty - pracuji s enumerable - toto se vyhodnocuje pouze jednou.
				result = paragraphs.Select(p => p.InnerText).ToArray();
			}

			return result;
		}

		/// <inheritdoc cref="ITextFileReader.Write"/>.
		public bool Write(string path, string newContent, string oldContent = null)
		{
			// Nejdřív záloha.
			try
			{
				// Pokud mám vytvořit zálohu -> jenom soubor přejmenuji.
				if (oldContent.IsNotNullOrEmpty())
				{
					var bakFile = path.Replace(".docx", ".bak");

					if (File.Exists(bakFile))
						File.Delete(bakFile);

					File.Copy(path, bakFile);
				}
			}
			catch
			{
				return false;
			}

			// Pak až edituji word.
			try
			{
				var fileExists = File.Exists(path);

				// Nakonec vytvářím word soubor.
				using (var word =  fileExists 
					? WordprocessingDocument.Open(path, isEditable: true)
					: WordprocessingDocument.Create(path, WordprocessingDocumentType.Document)
				)
				{
					// Pokud nemám nic (např. vytvářím soubor), tak si vše vytvořím.
					if (word.MainDocumentPart == null)
					{
						var mainPart = word.AddMainDocumentPart();
						mainPart.Document = new Document();
						mainPart.Document.Body = new Body();
					}

					// Odeberu všechny odstavce.
					word.MainDocumentPart.Document.Body.RemoveAllChildren<Paragraph>();

					// Získám si tělo dokumentu.
					var body = word.MainDocumentPart.Document.Body;

					// Řádek dám jako odstavec?
					foreach (var content in newContent.Split(Environment.NewLine.ToCharArray()))
					{
						// A zapíšu - vše jako jeden odstavec.
						var paragraph = body.AppendChild(new Paragraph());
						var run = paragraph.AppendChild(new Run());
						run.AppendChild(new Text(content));
					}

					word.MainDocumentPart.Document.Save();
				}

				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}