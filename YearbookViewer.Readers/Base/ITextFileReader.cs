﻿namespace YearbookViewer.Readers.Base
{
	/// <summary>
	/// Rozhraní, pro práci s textovým souborem.
	/// </summary>
	public interface ITextFileReader
	{
		/// <summary>
		/// Přečte soubor na požadované cestě.
		/// </summary>
		/// <param name="path">Cesta k souboru, který se má přečíst.</param>
		/// <returns>Obsah souboru.</returns>
		string[] Read(string path);

		/// <summary>
		/// Uloží do souboru požadovaný text.
		/// Pokud přepisuje soubor, vytvoří i jeho zálohu.
		/// </summary>
		/// <param name="path">Cesta, kam se nový text uloží.</param>
		/// <param name="newContent">Text, který se uloží do souboru.</param>
		/// <param name="oldContent">Původní text, pokud je předán, vytvoří se při úpravě textu rovnou i záloha.</param>
		/// <returns>Vrací, zda-li uložení do souboru proběhlo úspěšně.</returns>
		bool Write(string path, string newContent, string oldContent = null);
	}
}
