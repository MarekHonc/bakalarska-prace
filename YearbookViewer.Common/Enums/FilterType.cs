﻿namespace YearbookViewer.Common.Enums
{
	/// <summary>
	/// Výčet všech možností, jak lze používat filtry.
	/// </summary>
	public enum FilterType
	{
		/// <summary>
		/// Standardní filter.
		/// </summary>
		Filter,

		/// <summary>
		/// Možnost seskupení.
		/// </summary>
		Group
	}
}
