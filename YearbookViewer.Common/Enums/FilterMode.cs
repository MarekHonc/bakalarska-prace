﻿using System;

namespace YearbookViewer.Common.Enums
{
	/// <summary>
	/// Mód nastavení filtrů aplikace.
	/// </summary>
	[Flags]
	public enum FilterMode
	{
		/// <summary>
		/// Filtr s názvy stránek.
		/// </summary>
		PageNameFilter = 1,

		/// <summary>
		/// Filtr s názvy stránek a jeden dodatečný.
		/// </summary>
		SecondaryFilter = 2,

		/// <summary>
		/// Kompletní sada filtrů.
		/// </summary>
		TopLevelFilter = 4,
	}
}
