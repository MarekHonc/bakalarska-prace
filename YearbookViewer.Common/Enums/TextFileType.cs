﻿namespace YearbookViewer.Common.Enums
{
	/// <summary>
	/// Výčet všech podporovaných typů textových souborů, ze kterých umožňuje tato knihovna číst text.
	/// </summary>
	public enum TextFileType
	{
		/// <summary>
		/// Standardní textový soubor.
		/// </summary>
		Txt,

		/// <summary>
		/// Soubor pdf.
		/// </summary>
		Pdf,

		/// <summary>
		/// Umím číst i docx.
		/// </summary>
		Docx
	}
}
