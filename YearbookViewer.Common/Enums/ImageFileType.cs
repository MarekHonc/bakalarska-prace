﻿namespace YearbookViewer.Common.Enums
{
	/// <summary>
	/// Výčet všech podporovaných typů obrázků.
	/// </summary>
	public enum ImageFileType
	{
		Jpg,

		Jpeg,

		Png
	}
}
