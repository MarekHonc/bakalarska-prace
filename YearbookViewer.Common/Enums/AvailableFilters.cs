﻿using YearbookViewer.Common.Attributes;

namespace YearbookViewer.Common.Enums
{
	/// <summary>
	/// Vrací všechny dostupné možnosti filtrování.
	/// </summary>
	public enum AvailableFilters
	{
		/// <summary>
		/// Položky začínající na xxx.
		/// </summary>
		[FilterType(FilterType.Filter)]
		BeginsWith,

		/// <summary>
		/// Položky končící na xxx.
		/// </summary>
		[FilterType(FilterType.Filter)]
		EndsWith,

		/// <summary>
		/// Položky obsahující xxx.
		/// </summary>
		[FilterType(FilterType.Filter)]
		Contains,

		/// <summary>
		/// Seskupení položek podle obsahu.
		/// </summary>
		[FilterType(FilterType.Group)]
		Contents
	}
}
