﻿namespace YearbookViewer.Common.Interfaces
{
	/// <summary>
	/// Interface, ktyrý vynucuje zadání pracovní cesty.
	/// </summary>
	public interface IFilePathRequired
	{
		/// <summary>
		/// Nastaví cesty k souborům, odkud se mají data tahat.
		/// </summary>
		/// <param name="paths">
		/// Jednotlivé cesty, na indexu 0 je cesta k složce s ročemkami,
		/// na indexu 1 cesta k souboru s obsahy.
		/// Na dalších indexech mohou být libovolné cesty, kdyby byly pro danou implementaci třeba.
		/// </param>
		void SetPaths(params string[] paths);
	}
}
