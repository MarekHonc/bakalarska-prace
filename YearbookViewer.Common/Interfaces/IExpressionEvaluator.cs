﻿using System.Collections.Generic;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.Common.Interfaces
{
	/// <summary>
	/// Interface, který poskytuje možnost zpracovávat výrazy pro vyhlůedávání v textu.
	/// </summary>
	/// <remarks>
	/// Může být např. vyhodnocovač pomocí regexu,
	/// nebo Elastic search. https://www.elastic.co/guide/en/elasticsearch/reference/current/regexp-syntax.html
	/// </remarks>
	public interface IExpressionEvaluator
	{
		/// <summary>
		/// Vrací jestli je daný výraz platný.
		/// </summary>
		/// <param name="expr">Výraz, jehož platnost se kontroluje.</param>
		bool IsValid(string expr);

		/// <summary>
		/// Rozdělí string podle daného výrezu.
		/// </summary>
		/// <param name="text">Text, který bude výrazem rozdělen.</param>
		/// <param name="expr">Výraz podle kterého dělí daný text, pokud výraz není platný vyhodí výjimku.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		/// <returns>Pole stringů, které zniklo na základě rozdělení.</returns>
		string[] Split(string text, string expr, bool isExpression = false);

		/// <summary>
		/// Vrací jestli se v textu vyskytuje shoda s vyhledávaným výrazem.
		/// </summary>
		/// <param name="text">Text, který bude kontrolován.</param>
		/// <param name="expr">Výraz podle který se v textu hledá.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		bool IsMatch(string text, string expr, bool isExpression = false);

		/// <summary>
		/// Vrací všechny shody výrazu s textem.
		/// </summary>
		/// <param name="text">Text, který bude kontrolován.</param>
		/// <param name="expr">Výraz podle který se v textu hledá.</param>
		/// <param name="isExpression">Příznak, jestli má předaný termín interpretovat jako plain text nebo výraz.</param>
		/// <returns>Koleckce všech matchů, které se v textu nachází.</returns>
		ICollection<IMatchData> Matches(string text, string expr, bool isExpression = false);

		/// <summary>
		/// Vrací aktuální instanci této implementace.
		/// </summary>
		IExpressionEvaluator Instance { get; }
	}
}
