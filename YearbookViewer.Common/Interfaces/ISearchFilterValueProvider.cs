﻿namespace YearbookViewer.Common.Interfaces
{
	/// <summary>
	/// Interface, který dodává data pro filtrování.
	/// </summary>
	public interface ISearchFilterValueProvider
	{
		/// <summary>
		/// Vrací jednotlivé roky, ve kterých se ročenky nachází.
		/// </summary>
		string[] GetTopLevelFilter();

		/// <summary>
		/// Vrací seznam 
		/// </summary>
		/// <returns></returns>
		string[] GetSecondaryFilter(string topLevelFilter);

		/// <summary>
		/// Vrací názvy ročenek.
		/// </summary>
		string[] YearBookNames(string topLevelFilter, string secondaryFilter);

		/// <summary>
		/// Vrací aktuální instanci této implementace.
		/// </summary>
		ISearchFilterValueProvider Instance { get; }
	}
}
