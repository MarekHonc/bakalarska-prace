﻿namespace YearbookViewer.Common.Interfaces.DataInterfaces
{
	/// <summary>
	/// Interface pro filtrování dat, které stahuji.
	/// </summary>
	public interface IFilterData
	{
		/// <summary>
		/// Vrací rok ročenky, podle kterého se filtruje.
		/// </summary>
		string TopLevelFilter { get; }

		/// <summary>
		/// Vrací aktuálně zvolený obsah, podle kterého má vybírat.
		/// </summary>
		string SecondaryFilter { get; }

		/// <summary>
		/// Vrací název ročenky, podle kterého filtruje.
		/// </summary>
		string PageName { get; }

		/// <summary>
		/// Vrací případný text, který chci vyhledávat, bere v poraz i ostatní filtry.
		/// </summary>
		string SearchTerm { get; }

		/// <summary>
		/// Vrací jestli vyhledávaný string <see cref="SearchTerm"/> má brát zpracován jako výraz.
		/// </summary>
		/// <remarks>
		/// Jak se pak výraz interpretuje je již závislé na konkrétní implemetaci <see cref="IExpressionEvaluator"/>.
		/// </remarks>
		bool IsExpression { get; }

		/// <summary>
		/// Vrací, jestli se má použít globánílní vyhledávání.
		/// </summary>
		bool UseGlobalSearch { get; }
	}
}
