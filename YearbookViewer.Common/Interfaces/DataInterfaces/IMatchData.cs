﻿namespace YearbookViewer.Common.Interfaces.DataInterfaces
{
	/// <summary>
	/// Rozhraní, které zobrazuje výsledku hledání v textu.
	/// </summary>
	public interface IMatchData
	{
		/// <summary>
		/// Vrací index, na kterém se nachází shoda.
		/// </summary>
		int Index { get; }

		/// <summary>
		/// Vrací délku (veliksot) shody.
		/// </summary>
		int Length { get; }
	}
}
