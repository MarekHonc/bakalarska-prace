﻿namespace YearbookViewer.Common.Interfaces.DataInterfaces
{
	/// <summary>
	/// Interface, sloužící pro zobrazení hlavního contentu.
	/// Jedná se o jakési oddělení, jednotlivých vrstev aplikace.
	/// </summary>
	public interface IContentData
	{
		/// <summary>
		/// Vrací název ročenky.
		/// </summary>
		string PageName { get; }

		/// <summary>
		/// Vrací cestu k textovému souboru.
		/// </summary>
		string PagePath { get; }

		/// <summary>
		/// Vrací rok, ze kterého ročenka pochází.
		/// </summary>
		string Year { get; }

		/// <summary>
		/// Vrací cestu k obrázku.
		/// </summary>
		string ImageUrl { get; }

		/// <summary>
		/// Vrací text, který je na obrázku.
		/// </summary>
		string ImageText { get; }

		/// <summary>
		/// Vrací text, který je obrázku a vhodný pro použití k zvýrazněnení.
		/// </summary>
		string ImageTextForHighlight { get; }
	}
}
