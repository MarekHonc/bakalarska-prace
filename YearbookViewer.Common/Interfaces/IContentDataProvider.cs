﻿using System.Collections.Generic;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.Common.Interfaces
{
	/// <summary>
	/// Interface pro získávání dat ročenky k zobrazení.
	/// </summary>
	public interface IContentDataProvider
	{
		/// <summary>
		/// Vrací první ročenku, která vyhovuje filtrům.
		/// Pokud filtrům nic nevyhovuje, může vrátit null.
		/// </summary>
		IContentData GetFirst(IFilterData filters);

		/// <summary>
		/// Vrací kolekci výsledků vyhledávání omezených maximálním počtem (aby při vyhleávání "a" to nevrátilo milion výsledů a dotaz netrval hodinu).
		/// </summary>
		/// <param name="filters">Objekt s filtry, podle kterých se vyhledává.</param>
		/// <param name="count">Počet, kolik chci navrátit nálezů.</param>
		/// <param name="offset">Počet, kolik nálezů chci přeskočit..</param>
		IReadOnlyCollection<IContentData> GetCount(IFilterData filters, int count, int? offset = null);

		/// <summary>
		/// Uloží nový text ročenky.
		/// </summary>
		/// <param name="filters">Filtry, které ročenku vybrali.</param>
		/// <param name="newText">Nový text.</param>
		/// <returns><code>true</code> pokud se změny podařilo uložit, jinak <code>false</code>.</returns>
		bool Save(IFilterData filters, string newText);

		/// <summary>
		/// Vrací aktuální instanci této implementace.
		/// </summary>
		IContentDataProvider Instance { get; }
	}
}
