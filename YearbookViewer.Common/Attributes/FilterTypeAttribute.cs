﻿using System;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.Common.Attributes
{
	/// <summary>
	/// Atribut, označující jakým způsobem lze používat filtry.
	/// </summary>
	public class FilterTypeAttribute : Attribute
	{
		/// <summary>
		/// Seznam použití, jak lze konkrétní filter použít.
		/// </summary>
		public FilterType[] AvailableTypes { get; }

		/// <summary>
		/// Vytvoří atribut s návazností na použití filtrů.
		/// </summary>
		public FilterTypeAttribute(params FilterType[] types)
		{
			this.AvailableTypes = types;
		}
	}
}
