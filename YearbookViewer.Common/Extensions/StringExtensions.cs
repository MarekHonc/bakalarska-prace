﻿using System.Linq;

namespace YearbookViewer.Common.Extensions
{
	/// <summary>
	/// Třída s extension metodami pro string.
	/// </summary>
	public static class StringExtensions
	{
		/// <summary>
		/// Vrací, jestli string není null nebo prázný.
		/// </summary>
		/// <remarks>Jenom zabalení volání, přijde mi to čitelnějsí.</remarks>
		public static bool IsNotNullOrEmpty(this string s)
		{
			return !string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// Vrací, jestli string je null nebo prázný.
		/// </summary>
		/// <remarks>Jenom zabalení volání, přijde mi to čitelnějsí.</remarks>
		public static bool IsNullOrEmpty(this string s)
		{
			return string.IsNullOrEmpty(s);
		}

		/// <summary>
		/// Stejně jako normální .Substring, jen mu nevadí, když je vstup ve formátu, který nelze oříznout danými hranicemi.
		/// </summary>
		/// <remarks>Nakonec asi není třeba, le když už jsem to napsal.</remarks>
		public static string SafeSubstring(this string input, int startIndex, int? length = null)
		{
			if (input.IsNullOrEmpty())
				return input;

			// Tady jsem v kelu, nemůžu nic substringnout, vracím prázdno.
			if (startIndex >= input.Length)
			{
				return string.Empty;
			}

			if (length.HasValue)
			{
				var requiredLength = startIndex + length.Value;
				if (input.Length < requiredLength)
				{
					// Průšvih, nemůžu oříznout tak jak bych chtěl - ještě zkusím mrknout na částečný substring.
					if (input.Length > startIndex)
					{
						return input.Substring(startIndex);
					}
					else
					{
						return string.Empty;
					}
				}
			}

			// Pokud neznám konec.
			if (length == null)
			{
				return input.Substring(startIndex);
			}

			// Tady je vše ok, vracím klasický substring.
			return input.Substring(startIndex, length.Value);
		}

		/// <summary>
		/// Pokud je tento text prázdný <see cref="IsNullOrEmpty"/>, tak vrací null, aby se dal použít operátor "??".
		/// </summary>
		public static string NullForEmpty(this string text)
		{
			return text.IsNullOrEmpty() ? null : text;
		}

		/// <summary>
		/// Nastaví první písmenko daného stringu na velká písmeno.
		/// </summary>
		public static string FirstCharToUpper(this string input)
		{
			if (input.IsNullOrEmpty())
				return input;

			return input.First().ToString().ToUpper() + input.Substring(1);
		}

		/// <summary>
		/// Vrací předaný string v uvozovkách.
		/// </summary>
		public static string Enquote(this string s)
		{
			return $"\"{s}\"";
		}
	}
}
