﻿using System;
using System.Linq;
using System.Reflection;
using YearbookViewer.Common.Attributes;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.Common.Extensions
{
	/// <summary>
	/// Třída, obsahující rozšiřující metody pro enumy.
	/// </summary>
	public static class EnumExtensions
	{
		#region Enum extensions

		/// <summary>
		/// Vrací položky výčtu jako string, které je psán pouze malými písmenky.
		/// </summary>
		public static string ToLower(this Enum @enum)
		{
			return @enum.ToString().ToLower();
		}

		#endregion

		#region AvailableFiltersExtensions

		/// <summary>
		/// Vrací <see cref="AvailableFilters"/> jako typ.
		/// </summary>
		private static readonly Type filterType = typeof(AvailableFilters);

		/// <summary>
		/// Vrací typy filtrů pro které daný enum reprezentuje.
		/// </summary>
		public static FilterType[] GetFilterTypes(this AvailableFilters filter)
		{
			// Vytáhnu member info.
			var info = filterType.GetMember(Enum.GetName(filterType, filter));

			// Potřebuji mít přesně 1 member info.
			if (info.Length != 1)
				throw new ArgumentException($"{nameof(AvailableFilters)} of {filter} should have exactly 1 member info");

			// Vytáhnu první atribut.
			var attribute = info[0].GetCustomAttributes<FilterTypeAttribute>().FirstOrDefault();

			// Vrátím pole dostupných filtrů.
			return attribute.AvailableTypes;
		}

		#endregion
	}
}
