﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace YearbookViewer.Common.Extensions
{
	/// <summary>
	/// Extension metody pro práci s kolekcemi.
	/// </summary>
	public static class CollectionExtensions
	{
		/// <summary>
		/// Vrací Observable kolekci, vytvořenou z jakéhokoliv IEnumerable.
		/// </summary>
		/// <typeparam name="T">Typ položek v kolekci.</typeparam>
		/// <param name="source">Kolekce, kterou konvertuje.</param>
		public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> source)
		{
			return new ObservableCollection<T>(source);
		}
	}
}
