﻿using System;
using System.Linq;
using YearbookViewer.Common.Enums;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.Common
{
	/// <summary>
	/// Statická třída, obsahující všechny konstanty.
	/// </summary>
	public static class Constants
	{
#if DEBUG
		/// <summary>
		/// Vrací složka, kam se ukládají všechny soubory, se kterými aplikace pracuje.
		/// </summary>
		public static string FileStoreLocation = "C:\\";
#else
		/// <summary>
		/// Vrací složka, kam se ukládají všechny soubory, se kterými aplikace pracuje.
		/// </summary>
		public static string FileStoreLocation = AppDomain.CurrentDomain.BaseDirectory;
#endif

		/// <summary>
		/// Vrací název výchozí složky kam se ukládají data.
		/// </summary>
		public const string DataFolderName = "Data";

		/// <summary>
		/// Vrací výchozí název složky pro nevrchnější filtr (není volitelný uživatelem).
		/// </summary>
		public const string DefaultTopLevelFilter = "TopLevelFilter";

		/// <summary>
		/// Vrací výchozí název složky pro sekundární filtr (není volitelný uživatelem).
		/// </summary>
		public const string SecondaryLevelFilter = "SecondaryLevelFilter";

		/// <summary>
		/// Vrací všechny konfigurace filtrů, podporované aplikací.
		/// </summary>
		public static readonly FilterMode[] ValidFilterModes = new FilterMode[]
		{
			FilterMode.TopLevelFilter | FilterMode.SecondaryFilter | FilterMode.PageNameFilter,
			FilterMode.SecondaryFilter | FilterMode.PageNameFilter,
			FilterMode.PageNameFilter
		};

		/// <summary>
		/// Vrací všechny typy podporovaných textových souborů.
		/// </summary>
		public static string[] CompatibleTextExtensions = ((TextFileType[])Enum.GetValues(typeof(TextFileType))).Select(e => e.ToLower()).ToArray();

		/// <summary>
		/// Vrací všechny typy podporovaných textových souborů.
		/// </summary>
		public static string[] CompatibleImageExtensions = ((ImageFileType[])Enum.GetValues(typeof(ImageFileType))).Select(e => e.ToLower()).ToArray();

		/// <summary>
		/// Vrací všechny typy podporovaných obrázků.
		/// </summary>
		public static ImageFileType[] SupportedImageTypes = (ImageFileType[])Enum.GetValues(typeof(ImageFileType));
	}
}
