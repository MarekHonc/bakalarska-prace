﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.Common
{
	/// <summary>
	/// Pomocná třída, obsahující metody pro práci s ročenkami ve file systému.
	/// Pro správné vyhledávání a funkci jsou nutné přesně dvě úrovně vnoření.
	/// Tj.
	/// Složka - SubSložka - image1
	///                    - image2
	///        - SubSložka - image3
	///                    - image4
	/// </summary>
	public static class FileBrowser
	{
		/// <summary>
		/// Seznam zakázaných znaků v názvu složek.
		/// </summary>
		private static char[] _invalidFolderCharacters = new[] {'<', '>', ':', '"', '/', '\\', '|', '?', '*'};

		/// <summary>
		/// Klíčové slovíčko, které označují text, kterým chybí obrázek.
		/// </summary>
		private const string _missingImageKeyWord = "MISS";
		 
		/// <summary>
		/// Vrací všechny přímo vnořené adresáře.
		/// </summary>
		/// <param name="path">Cesta, na které se adresáře nachází.</param>
		/// <param name="searchPattern">Vyhledávací výraz, který názvy musí splnit (pokud je vyplněn).</param>
		/// <returns>Názvy všech podsložek.</returns>
		public static DirectoryInfo[] GetAllChildrenDirectories(string path, string searchPattern = null)
		{
			var directory = new DirectoryInfo(path);

			// Pokud složka neexistuje -> výjimka.
			if (!directory.Exists)
				throw new FileNotFoundException($"Requested folder '{path}' does not exist!");

			// Pokud mám vyhledávací výraz.
			if (searchPattern.IsNotNullOrEmpty())
				return directory.GetDirectories(searchPattern);

			// Jinak vracím vše.
			return directory.GetDirectories();
		}

		/// <summary>
		/// Vrací všechny názvy přímo vnořených adresářů.
		/// </summary>
		/// <param name="path">Cesta, na které se adresáře nachází.</param>
		/// <param name="searchPattern">Vyhledávací výraz, který názvy musí splnit (pokud je vyplněn).</param>
		/// <returns>Názvy všech podsložek.</returns>
		public static string[] GetAllChildrenDirectoriesNames(string path, string searchPattern = null)
		{
			return GetAllChildrenDirectories(path, searchPattern)
				.Select(d => d.Name)
				.ToArray();
		}

		/// <summary>
		/// Vrací všechny textové soubory podle <param name="extensions"></param>, které se v dané složce vyskytují.
		/// </summary>
		/// <param name="path">Cesta, na které se složka nachází.</param>
		/// <param name="extensions">Koncovky všech souborů, které chci vrátit.</param>
		/// <exception cref="FileNotFoundException">Pokud požadovaná složka neexistuje.</exception>
		/// <returns>Všechny textové soubory.</returns>
		public static string[] GetAllTextFiles(string path, string[] extensions)
		{
			// Vytvořím regex pro detekci platných typů souborů.
			var regex = $".*\\.({string.Join("|", extensions)})";

			// A vyhledávám.
			var result = Directory.EnumerateFiles(path)
				.Where(f => Regex.IsMatch(f, regex))
				.ToArray();

			return result;
		}

		/// <summary>
		/// Vrací názvy všech textových soubory podle <param name="extensions"></param>, které se v dané složce vyskytují.
		/// </summary>
		/// <param name="path">Cesta, na které se složka nachází.</param>
		/// <param name="extensions">Koncovky všech souborů, které chci vrátit.</param>
		/// <exception cref="FileNotFoundException">Pokud požadovaná složka neexistuje.</exception>
		/// <returns>Všechny textové soubory.</returns>
		public static string[] GetAllTextFilesNames(string path, string[] extensions)
		{
			return GetAllTextFiles(path, extensions)
				.Select(Path.GetFileName)
				.ToArray();
		}

		/// <summary>
		/// Vrací maximální počet vnořených adresářů, které se nacházejí v předaném adresáři.
		/// </summary>
		/// <param name="path">Cesta, na které se nachází složka, kterou zkoumám.</param>
		/// <returns>Nejvyšší úroveň vnoření.</returns>
		public static int GetNumberOfNestedDirectories(string path)
		{
			return GetNumberOfNestedDirectories(path, 0);
		}

		/// <summary>
		/// Vrací maximální počet vnořených adresářů, které se nacházejí v předaném adresáři.
		/// </summary>
		/// <param name="path">Cesta, na které se nachází složka, kterou zkoumám.</param>
		/// <param name="startingIndex">Počáteční index vnoření.</param>
		/// <returns>Nejvyšší úroveň vnoření.</returns>
		public static int GetNumberOfNestedDirectories(string path, int startingIndex)
		{
			var directoryInfo = new DirectoryInfo(path);
			var tempIndex = startingIndex;

			// Procházím všechny vnořené složky - nevím která bude mít největší vnoření.
			foreach (var directory in directoryInfo.EnumerateDirectories())
			{
				// Provedu rekurzivně operaci i pro vnořené složky.
				var nested = GetNumberOfNestedDirectories(directory.FullName, startingIndex + 1);

				// A pokud mám větší vnoření.
				if (nested > tempIndex)
				{
					// Hodnotu aktualizuji.
					tempIndex = nested;
				}
			}

			// Vracím výsledné vnořené.
			return tempIndex;
		}

		/// <summary>
		/// Vrací počet souborů ve složce.
		/// </summary>
		/// ¨<param name="path">Cesta na které se nachází složka, ve které se počítají všechny soubory.</param>
		public static int GetNumberOfFiles(string path)
		{
			return Directory.GetFiles(path, "*", SearchOption.AllDirectories).Length;
		}

		/// <summary>
		/// Překopíruje asynchronně všechny soubory z jedné složky do druhé.
		/// </summary>
		/// <param name="progress">Akce, která se volá po zpracování souboru.</param>
		/// <param name="fromDirectory">Složka, ze které se kopírují soubory.</param>
		/// <param name="toDirectory">Složka, do které se kopírují soubory</param>
		/// <param name="compatibleExtensions">Typy souborů, které se kopírují (např. .txt, .png, ...) jakékoliv jiné typy jsou následně ignorovány.</param>
		/// <param name="recurse">Příznak nastavující jestli se má funkce volat rekurzivně i na podsložky ALE všechny soubory nakopírovat do jedné složky.</param>
		/// <param name="alreadyProcessed">Počet souborů, které byly již zpracovány.</param>
		/// <param name="onFileCopied">Akce, která se provede po přepopírování souboru.</param>
		public static async Task<int> CopyFiles(Action<int> progress, string fromDirectory, string toDirectory,
			string[] compatibleExtensions = null, bool recurse = true, int alreadyProcessed = 0, Func<string, Task> onFileCopied = null)
		{
			// Zpracované soubory.
			var processed = alreadyProcessed;

			// Zkopíruji všechny soubory.
			foreach (var file in Directory.EnumerateFiles(fromDirectory))
			{
				// Pokud soubor obsahuje klíčové slovíčko, které označuje že data chybí - přeskočím.
				if(file.Contains(_missingImageKeyWord))
					continue;

				// Pokud mám předány i koncovky, které mám používat.
				if (compatibleExtensions != null)
				{
					// Tak kopíruji pouze ty koncovky.
					var extension = Path.GetExtension(file).Replace(".", string.Empty);
					if (!compatibleExtensions.Contains(extension))
						continue;
				}

				// Překopíruji soubor.
				var result = await CopyFile(file, toDirectory);

				// Provedu akci po překopírování souboru.
				if (onFileCopied != null)
				{
					await onFileCopied(result);
				}

				// Dám info o progressu.
				processed++;
				progress(processed);
			}

			// Pokud mám rekurzivně všechny podsložky sjednotit do jedné.
			if (recurse)
			{
				// Ještě projedu všechny složky a přidám je do stejné složky.
				foreach (var directory in Directory.EnumerateDirectories(fromDirectory))
				{
					processed += await CopyFiles(progress, directory, toDirectory, compatibleExtensions, recurse, processed, onFileCopied);
				}
			}

			// A vrátím kolik souborů jsem zpracoval.
			return processed;
		}

		/// <summary>
		/// Asynchronně překopíruje daný soubor do cílové složky.
		/// </summary>
		/// <param name="file">Soubor, který se kopíruje.</param>
		/// <param name="toDirectory">Složka, do které se kopíruje soubor.</param>
		public static async Task<string> CopyFile(string file, string toDirectory)
		{
			// Pokud soubor obsahuje klíčové slovíčko, které označuje že data chybí - přeskočím.
			if (file.Contains(_missingImageKeyWord))
				return string.Empty;

			// Název souboru.
			var fileName = Path.GetFileName(file);

			// Cesta k novému souboru (kam kopíruji).
			var destination = Path.Combine(toDirectory, fileName);

			// A zkopíruji (asynchronně).
			using (var source = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
			{
				using (var dest = File.Create(destination))
				{
					await source.CopyToAsync(dest);
				}
			}

			return destination;
		}

		/// <summary>
		/// Vymaže všechny neplatné znaky tak, aby z předaného stringu šel vždy vytvořit název složky.
		/// </summary>
		public static string EnsureValidDirectoryName(this string folderName)
		{
			var arr = folderName.Split(_invalidFolderCharacters, StringSplitOptions.RemoveEmptyEntries);
			return string.Join(string.Empty, arr).Trim();
		}
	}
}
