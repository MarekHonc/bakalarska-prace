﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace YearbookViewer.App.Code.Interfaces
{
	/// <summary>
	/// Rozhraní pro asynchronní comandy.
	/// </summary>
	public interface IAsyncCommand : ICommand
	{
		/// <summary>
		/// Spustí task asynchronně - neblokuje hlavní vlákno aplikace.
		/// </summary>
		Task ExecuteAsync();
	}
}
