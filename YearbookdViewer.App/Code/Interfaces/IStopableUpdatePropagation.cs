﻿namespace YearbookViewer.App.Code.Interfaces
{
	/// <summary>
	/// Interface, umožňující pozastavení komunikace mezi modely.
	/// </summary>
	public interface IStopableUpdatePropagation
	{
		/// <summary>
		/// Vrací, jestli se má pozastavit update dalších kompoment, závislých na daném modelu.
		/// </summary>
		bool StopUpdatePropagation { get; }
	}
}
