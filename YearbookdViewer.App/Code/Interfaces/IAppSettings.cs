﻿using YearbookViewer.App.Code.Factories;
using YearbookViewer.Common.Enums;
using YearbookViewer.Readers.TesseractOCR;

namespace YearbookViewer.App.Code.Interfaces
{
	/// <summary>
	/// Všechny možná nastavení aplikace.
	/// </summary>
	public interface IAppSettings
	{
		/// <summary>
		/// Vrací culture aplikace.
		/// </summary>
		string Culture
		{
			get;
		}

		/// <summary>
		/// Vrací šířku, na kterou se má zobrazovat obrázek.
		/// </summary>
		int ImageWidth
		{
			get;
		}

		/// <summary>
		/// Vrací šířku, přes kterou se zobrazuje text.
		/// </summary>
		int TextWidth
		{
			get;
		}

		/// <summary>
		/// Vrací, jestli se má aplikace dotazovat pro uložení.
		/// </summary>
		bool ConfirmSave
		{
			get;
		}

		/// <summary>
		/// Vrací aktuální zdroj dat.
		/// </summary>
		DataSourceType CurrentDateSource
		{
			get;
		}

		/// <summary>
		/// Vrací adresu serveru, pro získání dat.
		/// </summary>
		string ServerAddress
		{
			get;
		}

		/// <summary>
		/// Vrací aktuální mód aplikace.
		/// </summary>
		FilterMode ApplicationMode
		{
			get;
		}

		/// <summary>
		/// Vrací aktuální jazyk, ve kterém je nastavena OCR.
		/// </summary>
		OCRLanguage OCRLanguage
		{
			get;
		}

		/// <summary>
		/// Vrací, zda-li se při nálezu výskytu má zvýraznit celé slovo, nebo pouze jeho část.
		/// </summary>
		bool OCRHighlightWholeWord
		{
			get;
		}
	}
}
