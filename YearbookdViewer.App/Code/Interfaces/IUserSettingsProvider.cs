﻿using YearbookViewer.App.Code.Factories;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.App.Code.Interfaces
{
	/// <summary>
	/// Interface obstarávající práci s nastavením aplikace uživatelem.
	/// </summary>
	public interface IUserSettingsProvider
	{
		/// <summary>
		/// Načte uživatelem definovaná nastavení.
		/// </summary>
		IAppSettings Load();

		/// <summary>
		/// Uloží uživatel definovaná nastavení.
		/// </summary>
		void Save(IAppSettings settings);


		/// <summary>
		/// Uloží uživatelem definovaný layout.
		/// </summary>
		void SaveLayout(IAppSettings settingsViewModel);

		/// <summary>
		/// Nastaví nový mód aplikace.
		/// </summary>
		/// <param name="applicationMode">Nový mód aplikace.</param>
		/// <param name="dataSource">Zdroj dat aplikace.</param>
		/// <param name="serverAddress">Adresa serveru, na které se vyhledávač nachází.</param>
		void SaveAfterImport(FilterMode applicationMode, DataSourceType dataSource, string serverAddress);
	}
}
