﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Třída pro uložení položky do XML.
	/// </summary>
	/// <typeparam name="T">Typ, kterého požka je.</typeparam>
	public class XmlField<T>
	{
		protected readonly XElement xElement;
		protected readonly Func<T> defaultValueFactory;

		/// <summary>
		/// Vytvoří novou položku pro uložení do XML.
		/// </summary>
		/// <param name="xElement">Element XMl</param>
		/// <param name="defaultValue">Výchozí hodnota.</param>
		public XmlField(XElement xElement, T defaultValue)
			: this(xElement, () => defaultValue)
		{
		}

		public XmlField(XElement xElement, Func<T> defaultFactory)
		{
			this.xElement = xElement;
			this.defaultValueFactory = defaultFactory ?? (() => default(T));
		}

		/// <summary>
		/// Vrací nebo nastavuje hodnotu Xml nodu.
		/// Je už přímo typovaná tzn. když uložím int tak pak se mi hodnota načte i jako int.
		/// </summary>
		public virtual T Value
		{
			get
			{
				return GetValueCore(() =>
				{
					var value = GetInnerXml(this.xElement);

					// když je prázdná, tak vracím výchozí hodnotu
					if (string.IsNullOrEmpty(value))
					{
						return this.defaultValueFactory();
					}

					return FromXml<T>(value);
				});
			}
			set
			{
				SetValueCore(() =>
				{
					var xmlString = ToXmlString(value);
					XElement xmlValue = null;
					if (!string.IsNullOrEmpty(xmlString))
					{
						xmlValue = XElement.Parse(xmlString);
					}

					this.xElement.RemoveNodes();
					if (xmlValue != null)
					{
						this.xElement.Add(xmlValue);
					}
				});
			}
		}

		/// <summary>
		/// Metoda, která slouží k deserializování hodnoty uložené v XML do výsledného datového typu.
		/// </summary>
		/// <param name="deserialize">
		/// Funkce, která podle <see cref="T"/> získá data ve správném formátu.
		/// Pokud data nejsou dustupná použije výchozí hodnotu.
		/// </param>
		protected T GetValueCore(Func<T> deserialize)
		{
			var result = deserialize();
			return result;
		}

		/// <summary>
		/// Metoda, která slouží k upravení hodnoty v předaném XML Elementu podle předané <typeparamref name="T"/> vstupní hodnoty.
		/// </summary>
		/// <param name="updateElement">
		/// Funkce, která příchozí podle hodnoty udělá úpravy na <see cref="T"/>.
		/// </param>
		protected void SetValueCore(Action updateElement)
		{
			updateElement();
		}

		#region Helpers

		/// <summary>
		/// Vrací vnitřní XML tohoto elementu.
		/// (Něco jako <see cref="XElement.Value"/>, ale funguje i při vnořených nodech).
		/// </summary>
		private static string GetInnerXml(XElement element)
		{
			var reader = element.CreateReader();
			reader.MoveToContent();

			return reader.ReadInnerXml();
		}

		/// <summary>
		/// Deserializuje objekt z XML do daného typu objektu.
		/// </summary>
		/// <typeparam name="P">Typ očekávaného objektu.</typeparam>
		/// <param name="xml">Xml jako string.</param>
		/// <param name="def">Výchozí hodnota, pokud je příchozí string prázdný.</param>
		private static P FromXml<P>(string xml, P def = default(P))
		{
			return (P)FromXml(xml, typeof(P), def);
		}

		/// <summary>
		/// Deserializuje objekt z XML do daného typu objektu.
		/// </summary>
		/// <param name="type">Typ očekávaného objektu.</param>
		/// <param name="xml">Xml jako string.</param>
		/// <param name="def">Výchozí hodnota, pokud je příchozí string prázdný.</param>
		public static object FromXml(string xml, Type type, object def = null)
		{
			if (string.IsNullOrEmpty(xml))
				return def;

			using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(xml)))
			{
				return new XmlSerializer(type).Deserialize(stream);
			}
		}

		/// <summary>
		/// Zeserializuje objekt do XML.
		/// <typeparamref name="P">Typovaný paramentr.</typeparamref>
		/// <param name="input">Paramentr, který bud následně uložen.</param>
		/// </summary>
		private static string ToXmlString<P>(P input)
		{
			if (input == null)
				return null;

			var sb = new StringBuilder();
			using (var writer = XmlWriter.Create(sb, new XmlWriterSettings() { OmitXmlDeclaration = true }))
			{
				var nsSerializer = new XmlSerializerNamespaces();
				nsSerializer.Add("", "");
				var type = input.GetType();
				new XmlSerializer(type, "").Serialize(writer, input, nsSerializer);
				return sb.ToString();
			}
		}

		#endregion
	}
}
