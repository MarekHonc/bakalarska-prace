﻿using System.IO;
using YearbookViewer.App.Code.BaseClasses;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.Common;
using YearbookViewer.Common.Enums;
using YearbookViewer.Readers.TesseractOCR;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Třída pro práci s nastavením jako s XML.
	/// </summary>
	public class XmlSettings : BaseXmlSettings, IAppSettings
	{
		private readonly string settingsPath;

		public XmlSettings(string settingsPath)
		{
			this.settingsPath = settingsPath;
		}

		/// <summary>
		/// Vrací nebo nastavuje XML dokuemnt.
		/// </summary>
		protected override string SettingsXmlValue
		{
			get => File.ReadAllText(this.settingsPath);
			set => File.WriteAllText(this.settingsPath, value);
		}

		/// <summary>
		/// Vrací nebo natavuje (<code>.Value =</code>), kulturu v jaké je aktuálně aplikace lokalizovaná.
		/// </summary>
		public XmlField<string> Culture
		{
			get
			{
				return new XmlField<string>(this["Culture"], defaultValue: "cs");
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje (<code>.Value =</code>), jestli se má aplikace vyžadovat potvrzení při uložení přepisu.
		/// </summary>
		public XmlField<bool> ConfirmSave
		{
			get
			{
				return new XmlField<bool>(this["ConfirmSave"], defaultValue: true);
			}
		}

		/// <summary>
		/// Vrací nebo natavuje (<code>.Value =</code>), procentuální šířku, přes kterou se zobrazuje obrázek.
		/// </summary>
		public XmlField<int> ImageWidth
		{
			get
			{
				return new XmlField<int>(this["ImageWidth"], defaultValue: 60);
			}
		}

		/// <summary>
		/// Vrací nebo natavuje (<code>.Value =</code>), procentuální šířku, přes kterou se zobrazuje text.
		/// </summary>
		public XmlField<int> TextWidth
		{
			get
			{
				return new XmlField<int>(this["TextWidth"], defaultValue: 40);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje (<code>.Value =</code>), aktuální zdroj dat.
		/// </summary>
		public XmlField<DataSourceType> CurrentDataSource
		{
			get
			{
				return new XmlField<DataSourceType>(this["CurrentDataSource"], defaultValue: DataSourceType.FileBrowser);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje (<code>.Value =</code>) adresu serveru, kde se nachází vyhledávací engine.
		/// </summary>
		public XmlField<string> ServerAddress
		{
			get
			{
				return new XmlField<string>(this["ServerAddress"], defaultValue: null);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje (<code>.Value =</code>) mód filtrů aplikace.
		/// </summary>
		public XmlField<FilterMode> ApplicationMode
		{
			get
			{
				return new XmlField<FilterMode>(this["ApplicationMode"], defaultValue: Constants.ValidFilterModes[0]);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje (<code>.Value =</code> jazyk pro OCR.
		/// </summary>
		public XmlField<OCRLanguage> OCRLanguage
		{
			get
			{
				return new XmlField<OCRLanguage>(this["OCRLanguage"], defaultValue: Readers.TesseractOCR.OCRLanguage.ces);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje (<code>.Value =</code> zda-li se při nálezu pomocí OCR má zvýraznit celé slovo.
		/// </summary>
		public XmlField<bool> OCRHighlightWholeWord
		{
			get
			{
				return new XmlField<bool>(this["OCRHighlightWholeWord"], defaultValue: true);
			}
		}

		#region Implementace IAppSettings

		string IAppSettings.Culture => Culture.Value;

		int IAppSettings.ImageWidth => ImageWidth.Value;

		int IAppSettings.TextWidth => TextWidth.Value;

		bool IAppSettings.ConfirmSave => ConfirmSave.Value;

		DataSourceType IAppSettings.CurrentDateSource => CurrentDataSource.Value;

		string IAppSettings.ServerAddress => ServerAddress.Value;

		FilterMode IAppSettings.ApplicationMode => ApplicationMode.Value;

		OCRLanguage IAppSettings.OCRLanguage => OCRLanguage.Value;

		bool IAppSettings.OCRHighlightWholeWord => OCRHighlightWholeWord.Value;

		#endregion
	}
}