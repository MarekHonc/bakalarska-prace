﻿using System;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Služba, starající se o práci s uživatelským nastavením uloženým v XML.
	/// </summary>
	public class XmlUserSettingsProvider : IUserSettingsProvider
	{
		private XmlSettings settings;

		/// <summary>
		/// Načte nastavení z XML.
		/// </summary>
		/// <returns>Vrací načtené nastavení.</returns>
		public IAppSettings Load()
		{
			// Pokud nastavení nemám, vytvořím.
			if (this.settings == null)
				this.settings = new XmlSettings(@"AppSettings.xml");

			return this.settings;
		}

		/// <summary>
		/// Uloží nsatavení do XML, pokud nastavení nejsou načteny, vyhodí výjimku.
		/// </summary>
		/// <param name="newSettings">Nové nastavení.</param>
		public void Save(IAppSettings newSettings)
		{
			// Nemám nastavení -> tak vyhodím výjimku.
			if (this.settings == null)
				throw new Exception("XmlUserSettingsProvider - Cannot save settings before they were loaded");

			// Aktualizuji hodnoty.
			this.settings.Culture.Value = newSettings.Culture;
			this.settings.OCRLanguage.Value = newSettings.OCRLanguage;
			this.settings.ConfirmSave.Value = newSettings.ConfirmSave;
		}

		/// <summary>
		/// Uloží nastavení o layoutu do XML.
		/// </summary>
		/// <param name="newSettings">Nové nastavení.</param>
		public void SaveLayout(IAppSettings newSettings)
		{
			// Nemám nastavení -> tak vyhodím výjimku.
			if (this.settings == null)
				throw new Exception("XmlUserSettingsProvider - Cannot save settings before they were loaded");

			this.settings.ImageWidth.Value = newSettings.ImageWidth;
			this.settings.TextWidth.Value = newSettings.TextWidth;
		}

		/// <summary>
		/// Nastaví nový mód aplikace.
		/// </summary>
		/// <param name="applicationMode">Nový mód aplikace.</param>
		/// /// <param name="dataSource">Zdroj dat aplikace.</param>
		/// <param name="serverAddress">Adresa serveru, na které se vyhledávač nachází.</param>
		public void SaveAfterImport(FilterMode applicationMode, DataSourceType dataSource, string serverAddress)
		{
			// Nemám nastavení -> tak vyhodím výjimku.
			if (this.settings == null)
				throw new Exception("XmlUserSettingsProvider - Cannot save settings before they were loaded");

			this.settings.ApplicationMode.Value = applicationMode;
			this.settings.CurrentDataSource.Value = dataSource;

			if (dataSource == DataSourceType.ElasticSearch)
			{
				this.settings.ServerAddress.Value = serverAddress;
			}
		}
	}
}
