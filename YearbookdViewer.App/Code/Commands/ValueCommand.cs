﻿using System;
using System.Windows.Input;

namespace YearbookViewer.App.Code.Commands
{
	/// <summary>
	/// Základní parametrický command, příjmající základní datové typy.
	/// </summary>
	/// <remarks>Nevím jestli bude potřeba, ale implementuji, když už to dělám :)</remarks>
	public class ValueCommand<T> : ICommand
	{
		private readonly Action<T> execute;

		/// <summary>
		/// Vytvoří nový parametrický Command.
		/// </summary>
		/// <param name="execute">Akce, která se má pro daný command provést.</param>
		public ValueCommand(Action<T> execute) /*  Func<T, bool> enabled = null */
		{
			this.execute = execute;
		}

		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		/// <summary>
		/// Vrací, jeslti je možné command spustit.
		/// </summary>
		/// <param name="parameter">Parametr, který Command má.</param>
		/// <returns>Vrací true (zatím nemám potřebu commandy blokovat).</returns>
		public bool CanExecute(object parameter)
		{
			return true;
		}

		/// <summary>
		/// Provede daný command.
		/// </summary>
		/// <param name="parameter">Parametr, který Command má.</param>
		public void Execute(object parameter)
		{
			// Rovnou přetypuji, přetypování by mělo být ok, pokud padne je to chyba programátora ne uživatele.
			this.execute.Invoke((T)parameter);
		}
	}
}
