﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using YearbookViewer.App.Code.Interfaces;

namespace YearbookViewer.App.Code.Commands
{
	/// <summary>
	/// Bázový asynchronní comand.
	/// </summary>
	public class AsyncCommand : IAsyncCommand
	{
		private Func<Task> command;

		/// <summary>
		/// Vytvoří nový asynchronní comand.
		/// </summary>
		/// <param name="command">Delegát akce, která se provede.</param>
		public AsyncCommand(Func<Task> command)
		{
			this.command = command;
		}

		/// <summary>
		/// Vrací, zda-li se může command spustit, vrací vždy true.
		/// </summary>
		public bool CanExecute(object parameter)
		{
			return true;
		}

		/// <summary>
		/// Spustí command.
		/// </summary>
		public async void Execute(object parameter)
		{
			await this.ExecuteAsync();
		}

		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		/// <summary>
		/// Provede daný command.
		/// </summary>
		public async Task ExecuteAsync()
		{
			await this.command();
		}
	}
}
