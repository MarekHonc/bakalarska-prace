﻿using System;
using System.Windows.Input;

namespace YearbookViewer.App.Code.Commands
{
	/// <summary>
	/// Základní bezparametrický command.
	/// </summary>
	/// <remarks>Nevím jestli bude potřeba, ale implementuji, když už to dělám :)</remarks>
	public class Command : ICommand
	{
		private readonly Action execute;

		/// <summary>
		/// Vytvoří nový Command.
		/// </summary>
		/// <param name="execute">Akce, která se má pro daný command provést.</param>
		public Command(Action execute) /* Func<bool> enabled = null) */
		{
			this.execute = execute;
		}

		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		/// <summary>
		/// Vrací, jeslti je možné command spustit.
		/// </summary>
		/// <returns>Vrací true (zatím nemám potřebu commandy blokovat).</returns>
		public bool CanExecute(object parameter)
		{
			return true;
		}

		/// <summary>
		/// Provede daný command.
		/// </summary>
		public void Execute(object parameter)
		{
			// Spustím command.
			execute.Invoke();
		}
	}
}
