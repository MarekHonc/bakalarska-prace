﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Knihovní třída, starající se o funkce pro zvýrazňování textu v GUI vrstvě..
	/// </summary>
	public static class HighLighter
	{
		private static IExpressionEvaluator evaluator;

		/// <summary>
		/// Metoda, pro nakonfigurování zvýrazňovače.
		/// </summary>
		/// <param name="ev">Interface, který použiji pro zvýraznění.</param>
		public static void Configure(IExpressionEvaluator ev)
		{
			evaluator = ev;
		}

		/// <summary>
		/// Prefix vyhledávání pomocí výrazů.
		/// </summary>
		public const string EXPRESSION_PREFIX = "expr-";

		/// <summary>
		/// Funkce pro zvýrarnění textu v daném RichTextBoxu.
		/// </summary>
		/// <param name="textBox">RichTextBox, kde chci zvýrazňovat.</param>
		/// <param name="search">Vyhledávaný termín, pokud začíná na <see cref="EXPRESSION_PREFIX"/> bude výraz interpretován jako výraz.</param>
		/// <param name="filters">Aktuální filtery.</param>
		public static void HighlightSelection(RichTextBox textBox, string search, IFilterData filters)
		{
			// Nejdřív dám do pryč veškeré staré zvýraznění.
			var content = new TextRange(textBox.Document.ContentStart, textBox.Document.ContentEnd);
			content.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Black));
			content.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Normal);

			if(search.IsNullOrEmpty())
				return;

			var isExpression = IsExpression(search, out search);

			TextPointer pointer = textBox.Document.ContentStart;
			while (pointer != null)
			{
				if (pointer.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
				{
					string textRun = pointer.GetTextInRun(LogicalDirection.Forward);
					var matches = evaluator.Matches(textRun, search, isExpression);
					foreach (var match in matches)
					{
						int startIndex = match.Index;
						int length = match.Length;
						TextPointer start = pointer.GetPositionAtOffset(startIndex);
						TextPointer end = start.GetPositionAtOffset(length);

						var textRange = new TextRange(start, end);
						
						if(textRange.Text != search)
							continue;

						textRange.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Orange));
						textRange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
					}
				}

				pointer = pointer.GetNextContextPosition(LogicalDirection.Forward);
			}
		}

		/// <summary>
		/// Vezme vyhledávaný výryz a zjistí, jestli ho má interpretovat jako výraz nebo plain text.
		/// </summary>
		/// <param name="search">Text, podle kterého se vyhledává, většinou input uživatele.</param>
		/// <param name="expression">Výraz, který znikne, je např. odebrán prefix.</param>
		/// <returns>True, pokud se má text interpretovat jako výraz, jinak false.</returns>
		public static bool IsExpression(string search, out string expression)
		{
			if (search.StartsWith(EXPRESSION_PREFIX))
			{
				expression = search.Replace(EXPRESSION_PREFIX, string.Empty);
				return true;
			}

			expression = search;
			return false;
		}
	}
}
