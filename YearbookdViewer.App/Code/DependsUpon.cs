﻿using System;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Atribute, používaný pro označení dané property, že je závislá na x. dalších.
	/// </summary>
	public class DependsUponAttribute : Attribute
	{
		private readonly string[] properties;

		public DependsUponAttribute(params string[] properties)
		{
			this.properties = properties;
		}

		/// <summary>
		/// Vrací všechny proměnné, na kterých je daná vlastnost závislá.
		/// </summary>
		public string[] Properties
		{
			get => this.properties;
		}
	}
}
