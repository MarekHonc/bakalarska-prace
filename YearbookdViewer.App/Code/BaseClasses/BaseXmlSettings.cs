﻿using System.Xml.Linq;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.Code.BaseClasses
{
	/// <summary>
	/// Výchozí abstraktní třída pro práci s nastavením (pomocí XML vlastnosti).
	/// </summary>
	public abstract class BaseXmlSettings
	{
		/// <summary>
		/// Prázdné nastavení v textové podobě (výchozí hodnota <see cref="SettingsXmlValue"/>).
		/// </summary>
		protected const string EMPTY_APP_SETTINGS_XML = "<AppSettings></AppSettings>";

		/// <summary>
		/// Vrací nebo nastavuje string, který se používá pro čtení a ukládání nastavení.
		/// </summary>
		protected abstract string SettingsXmlValue
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací základní XML objekt, který automaticky updatuje Content a ModifiedAt pokud je změněn.
		/// </summary>
		protected XElement GetXmlRoot()
		{
			var xml = XElement.Parse(this.SettingsXmlValue.NullForEmpty() ?? EMPTY_APP_SETTINGS_XML);
			xml.Changed += (sender, args) =>
			{
				this.SettingsXmlValue = xml.ToString(SaveOptions.None);
			};
			return xml;
		}

		protected XElement this[string name]
		{
			get
			{
				var root = GetXmlRoot();
				var element = root.Element(name);
				if (element == null)
				{
					element = new XElement(name);
					root.Add(element);
				}
				return element;
			}
		}
	}
}