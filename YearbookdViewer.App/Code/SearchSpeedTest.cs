﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Třída, která umožňuje testovat rychlost full-textového vyhledávání.
	/// </summary>
	public class SearchSpeedTest
	{
		private static string[] keyWords = new[]
		{
			"e", "a", "der", "die", "aus", "dem erfreulichen",
			"so will ich mir", "Was will", "Einrichtungsgegenstände",
			"This will return nothing"
		};

		private readonly IContentDataProvider dataProvider;

		public SearchSpeedTest(IContentDataProvider dataProvider)
		{
			this.dataProvider = dataProvider;
		}

		/// <summary>
		/// Vrací všechny výsledky rychlostních testů.
		/// </summary>
		public List<(double, string)> GetSpeedResults()
		{
			var result = new List<(double, string)>();
			var stopWatch = new Stopwatch();

			foreach (var keyWord in keyWords)
			{
				var times = new List<long>();

				for (var i = 0; i < 10; i++)
				{
					stopWatch.Start();

					DoTest(keyWord);
					times.Add(stopWatch.ElapsedMilliseconds);

					stopWatch.Stop();
					stopWatch.Reset();
				}

				result.Add((times.Average(), keyWord));
			}

			return result;
		}

		#region helpers

		/// <summary>
		/// Provede vyhledání.
		/// </summary>
		private void DoTest(string keyWord)
		{
			var filters = new FakeFilters(keyWord);
			this.dataProvider.GetCount(filters, 10);
		}

		/// <summary>
		/// Fakový provider dat pro vyhledávání - používá pouze to, co je třeba.
		/// </summary>
		private class FakeFilters : IFilterData
		{
			public FakeFilters(string search)
			{
				this.SearchTerm = search;
			}

			/// <summary>
			/// Vrací vyhledávaný termín.
			/// </summary>
			public string SearchTerm
			{
				get;
			}

			/// <summary>
			/// Vždy prohledávám vše.
			/// </summary>
			public bool UseGlobalSearch => true;

			#region NotUsed

			public bool IsExpression
			{
				get;
			}

			public string TopLevelFilter
			{
				get;
			}

			public string SecondaryFilter
			{
				get;
			}

			public string PageName
			{
				get;
			}

			#endregion

		}

		#endregion
		
	}
}
