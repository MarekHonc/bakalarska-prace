﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;

namespace YearbookViewer.App.Code.CustomElements
{
	/// <summary>
	/// Vlastní chování zvýrozňování v textu, jedná se o hezké MVVM řešení, původně jsem chtěl implementovat i pro RichTextBox, ale to je o dost složitější.
	/// Kód čerpán a upraven z:
	/// https://stackoverflow.com/a/60474831/14060365
	/// </summary>
	public static class HighlightTermBehavior
	{
		private static IExpressionEvaluator evaluator;

		/// <summary>
		/// Metoda, pro nakonfigurování zvýrazňovače.
		/// </summary>
		/// <param name="ev">Interface, který použiji pro zvýraznění.</param>
		public static void Configure(IExpressionEvaluator ev)
		{
			evaluator = ev;
		}

		/// <summary>
		/// Konstanta, označující počet znaků, který bude zobrazen před a za vyhledávaným slovem.
		/// </summary>
		private const int NumberOfShownChars = 50;

		/// <summary>
		/// Konstanta, oznažující počet výskytů, které mohu maximálně hledat, poté by to bylo již zbytečné.
		/// </summary>
		private const int HihglightCount = 10;

		/// <summary>
		/// Konstanta, definující jaké znaky se mají zobrazit pokud text pokračuje. 
		/// </summary>
		private const string TextContinues = "...";

		/// <summary>
		/// Nastavení dependency pro WPF - vlastnost na kterou mohu přistupovat přímo z XAMLu.
		/// Jedná se o text prvku.
		/// </summary>
		public static readonly DependencyProperty TextProperty =
			DependencyProperty.RegisterAttached(
				"Text",
				typeof(string),
				typeof(HighlightTermBehavior),
				new FrameworkPropertyMetadata(string.Empty, OnTextChanged)
			);

		/// <summary>
		/// Nastavení dependency pro WPF - vlastnost na kterou mohu přistupovat přímo z XAMLu.
		/// Jedná se o text, který bude zvýrazněn.
		/// </summary>
		public static readonly DependencyProperty TermToBeHighlightedProperty =
			DependencyProperty.RegisterAttached(
				"TermToBeHighlighted",
				typeof(string),
				typeof(HighlightTermBehavior),
				new FrameworkPropertyMetadata(string.Empty, OnTextChanged)
			);

		/// <summary>
		/// Vrací text, který je aktuálně zobrazen.
		/// </summary>
		/// <param name="frameworkElement">WPF element z frameworku.</param>
		public static string GetText(FrameworkElement frameworkElement) => (string)frameworkElement.GetValue(TextProperty);

		/// <summary>
		/// Nastavuje text, který je aktuálně zobrazen.
		/// </summary>
		/// <param name="frameworkElement">WPF element z frameworku.</param>
		/// <param name="value">Text, který bude nastaven pro zobrazení.</param>
		public static void SetText(FrameworkElement frameworkElement, string value) => frameworkElement.SetValue(TextProperty, value);

		/// <summary>
		/// Vrací text, který má být zvýrazněn.
		/// </summary>
		/// <param name="frameworkElement">WPF element z frameworku.</param>
		public static string GetTermToBeHighlighted(FrameworkElement frameworkElement) => (string)frameworkElement.GetValue(TermToBeHighlightedProperty);

		/// <summary>
		/// Nastavuje text, který má být zvýrezněn.
		/// </summary>
		/// <param name="frameworkElement">WPF element z frameworku.</param>
		/// <param name="value"></param>
		public static void SetTermToBeHighlighted(FrameworkElement frameworkElement, string value) => frameworkElement.SetValue(TermToBeHighlightedProperty, value);

		/// <summary>
		/// Handler, reagující na změnu textu, aby fungoval Binding.
		/// </summary>
		/// <param name="dependency">Objekt na kterém je změna vyvolána.</param>
		/// <param name="args">Parametry vyvolená změny.</param>
		private static void OnTextChanged(DependencyObject dependency, DependencyPropertyChangedEventArgs args)
		{
			if (dependency is TextBlock textBlock)
				SetTextBlockTextAndHighlightTerm(textBlock, GetText(textBlock), GetTermToBeHighlighted(textBlock));
		}

		#region PrivateMethods

		/// <summary>
		/// Hlavní metoda pro zvýrazňování v textu.
		/// </summary>
		/// <param name="textBlock">Textblock, ve kterém zvýranuji.</param>
		/// <param name="text">Text, který je v textblocku obsažen.</param>
		/// <param name="termToBeHighlighted">Výraz pro zvýraznění.</param>
		private static void SetTextBlockTextAndHighlightTerm(TextBlock textBlock, string text, string termToBeHighlighted)
		{
			textBlock.Text = string.Empty;

			// Pokud nemám text, ani nic k zvýraznění -> nemá cenu nic dělat.
			if (text.IsNullOrEmpty() || termToBeHighlighted.IsNullOrEmpty())
				return;

			// Rozdělím si text.
			var textParts = SplitTextIntoTermAndNotTermParts(text, termToBeHighlighted);

			// Prvních x výskytů stačí.
			var count = textParts.Count > HihglightCount ? HihglightCount : textParts.Count;

			// A Zvýrazním.
			for (int i = 0; i < count; i++)
			{
				AddPartToTextBlockAndHighlightIfNecessary(
					textBlock,
					termToBeHighlighted,
					textParts[i],
					i > 0 ? textParts[i - 1] : null,
					i < textParts.Count - 1 ? textParts[i + 1] : null
				);
			}
		}

		/// <summary>
		/// Přidá text do textblocku, pokud má být zvýrazněn, rovnou ho zvýrazní.
		/// </summary>
		/// <param name="textBlock">Textblock, kam text přidává.</param>
		/// <param name="termToBeHighlighted">Výraz, který má být zvýrazněn.</param>
		/// <param name="termToBeHighlighted">Aktuální část textu..</param>
		/// <param name="prevPart">Předchozí část textu.</param>
		/// <param name="nextPart">Následující část textu.</param>
		private static void AddPartToTextBlockAndHighlightIfNecessary(TextBlock textBlock, string termToBeHighlighted, string textPart, string prevPart = null, string nextPart = null)
		{
			var isExpression = HighLighter.IsExpression(termToBeHighlighted, out termToBeHighlighted);

			if (evaluator.IsMatch(textPart, termToBeHighlighted, isExpression))
			{
				var previous = evaluator.IsMatch(prevPart, termToBeHighlighted, isExpression) ? null : prevPart;
				var next = evaluator.IsMatch(nextPart, termToBeHighlighted, isExpression) ? null : nextPart;

				AddHighlightedPartToTextBlock(textBlock, textPart, previous, next);
			}
		}

		/// <summary>
		/// Přidá zvýrazněný výraz do textu.
		/// </summary>
		/// <param name="textBlock">Textblock, kam text přidává.</param>
		/// <param name="part">Část textu, která má být zvýrazněna.</param>
		/// <param name="prevPart">Předchozí část textu.</param>
		/// <param name="nextPart">Následující část textu.</param>
		private static void AddHighlightedPartToTextBlock(TextBlock textBlock, string part, string prevPart, string nextPart)
		{
			// Přidám text před nález.
			if (prevPart.IsNotNullOrEmpty())
			{
				if (prevPart.Length > NumberOfShownChars)
					prevPart = TextContinues + prevPart.Substring(prevPart.Length - NumberOfShownChars, NumberOfShownChars);

				textBlock.Inlines.Add(new Run() { Text = prevPart });
			}

			// Přidám nález.
			textBlock.Inlines.Add(new Run() { Text = part, FontWeight = FontWeights.Bold, Foreground = new SolidColorBrush(Colors.Orange) });

			// Přidám text za nález, pokud existuje.
			if (nextPart.IsNotNullOrEmpty())
			{
				if (nextPart.Length > NumberOfShownChars)
					nextPart = nextPart.Substring(0, NumberOfShownChars) + TextContinues;

				textBlock.Inlines.Add(new Run() { Text = nextPart });
			}
		}

		/// <summary>
		/// Rozdělí text na jednotlivé bloky, vždy je block před textem pak vyhledáváaný text a pak další blok.
		/// </summary>
		/// <param name="text">Text, ve kterém vyhledávám.</param>
		/// <param name="term">Vyhledávaný termín.</param>
		/// <returns>List, kde je rozdělený vstupní text.</returns>
		public static List<string> SplitTextIntoTermAndNotTermParts(string text, string term)
		{
			if (text.IsNullOrEmpty())
				return new List<string>() { string.Empty };

			var isExpression = HighLighter.IsExpression(term, out term);

			return evaluator.Split(text, term, isExpression)
						.Where(p => p != string.Empty)
						.ToList();
		}

		#endregion
	}
}
