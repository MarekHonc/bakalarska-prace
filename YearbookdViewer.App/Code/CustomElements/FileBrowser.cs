﻿using System.Windows.Controls;
using System.Windows.Input;

namespace YearbookViewer.App.Code.CustomElements
{
	/// <summary>
	/// Vlastní element umožňující vybírat vybírat přímo složku ve file systému.
	/// </summary>
	/// <remarks>
	/// Neupravuje žádné chování bindingů, ani žádný nepřidává.
	/// </remarks>
	public class FileBrowser : TextBox
	{
		/// <summary>
		/// Na kliknutí myší, otevírám dialog.
		/// </summary>
		protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
		{
			var fileDialog = new System.Windows.Forms.FolderBrowserDialog();
			var ok = fileDialog.ShowDialog();

			if (ok == System.Windows.Forms.DialogResult.OK)
			{
				this.Text = fileDialog.SelectedPath;
			}
		}
	}
}
