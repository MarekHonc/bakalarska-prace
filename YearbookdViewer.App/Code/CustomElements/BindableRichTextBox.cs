﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;

namespace YearbookViewer.App.Code.CustomElements
{
	/// <summary>
	/// Vlastní prvek pro bindovatelný rich text box.
	/// </summary>
	/// <remarks>
	/// Kód čerpán a upraven z:
	/// https://www.codeproject.com/Articles/137209/Binding-and-styling-text-to-a-RichTextBox-in-WPF
	/// </remarks>
	public class BindableRichTextBox : RichTextBox
	{
		/// <summary>
		/// Nastavení dependecy property pro WPF -> vlastnost, ke který mohu přistupovat z XAMLu.
		/// </summary>
		public static readonly DependencyProperty DocumentProperty =
			DependencyProperty.Register(
				nameof(Document),
				typeof(FlowDocument),
				typeof(BindableRichTextBox),
				new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnDocumentChanged))
			);

		/// <summary>
		/// Vrací nebo nastavuje již bindovatelný dokument.
		/// </summary>
		/// <remarks>Používám new kostnrukci, abych přepsal již existující Document property.</remarks>
		public new FlowDocument Document
		{
			get
			{
				return (FlowDocument)this.GetValue(DocumentProperty);
			}

			set
			{
				this.SetValue(DocumentProperty, value);
			}
		}

		/// <summary>
		/// Jěště handler pro změnu dokumentu, nevolá se pokud se změní pouze obsah dokumentu.
		/// </summary>
		public static void OnDocumentChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args)
		{
			RichTextBox rtb = (RichTextBox)obj;
			rtb.Document = (FlowDocument)args.NewValue;
		}

		/// <summary>
		/// Na ztrátu focusu měním i text.
		/// Binding volám manuálně, pro konverzi ze stringu na DocumentFlow to ve výchozím stavu reaguje na změnu celého dokumentu, ale já chci při změně textu.
		/// </summary>
		protected override void OnLostFocus(RoutedEventArgs e)
		{
			var binding = this.GetBindingExpression(DocumentProperty);
			binding.UpdateSource();
		}
	}
}
