﻿using System.Collections.Generic;
using YearbookViewer.App.Code.Factories.EmptyImplementation;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.DL.Implementations;
using YearbookViewer.Elastic.Implementations;

namespace YearbookViewer.App.Code.Factories
{
	/// <summary>
	/// Tovární třída, poskytující přístup k vyhledávacím filtrům.
	/// </summary>
	public class SearchFiltersProviderFactory : FactoryBase<ISearchFilterValueProvider>, ISearchFilterValueProvider
	{
		#region Singleton

		/// <summary>
		/// Provideři jsou vždy singleton v rámci aplikace.
		/// </summary>

		private readonly EmptySearchFilterProvider emptySearchFilterProvider = new EmptySearchFilterProvider();
		private readonly SearchFilterFromFilesProvider searchFilterFromFilesProvider = new SearchFilterFromFilesProvider();
		private readonly ElasticSearchFilterProvider elasticSearchFilterProvider = new ElasticSearchFilterProvider();

		#endregion

		/// <summary>
		/// Vytvoří novou instanci tovární třídy, obsahující všechny providery vyhledávacích filtrů.
		/// </summary>
		/// <param name="settingsProvider">Nastavení aplikace.</param>
		public SearchFiltersProviderFactory(IUserSettingsProvider settingsProvider) : base(settingsProvider) { }

		/// <summary>
		/// Vrací slovník, ve kterém se nachází všechny instance providerů vyhledávacích filtrů.
		/// </summary>
		protected override Dictionary<DataSourceType, ISearchFilterValueProvider> InstancesDictionary =>
			new Dictionary<DataSourceType, ISearchFilterValueProvider>()
			{
				{ DataSourceType.DefaultEmpty, emptySearchFilterProvider },
				{ DataSourceType.FileBrowser, searchFilterFromFilesProvider },
				{ DataSourceType.ElasticSearch, elasticSearchFilterProvider }
			};

		#region Implementace

		/// <inheritdoc cref="ISearchFilterValueProvider.GetTopLevelFilter"/>.
		public string[] GetTopLevelFilter()
		{
			return this.CurrentInstance.GetTopLevelFilter();
		}

		/// <inheritdoc cref="ISearchFilterValueProvider.GetSecondaryFilter"/>.
		public string[] GetSecondaryFilter(string topLevelFilter)
		{
			return this.CurrentInstance.GetSecondaryFilter(topLevelFilter);
		}

		/// <inheritdoc cref="ISearchFilterValueProvider.YearBookNames"/>.
		public string[] YearBookNames(string topLevelFilter, string secondaryFilter)
		{
			return this.CurrentInstance.YearBookNames(topLevelFilter, secondaryFilter);
		}

		/// <summary>
		/// Vrací aktuální instanci, se kterou se pracuje.
		/// </summary>
		public ISearchFilterValueProvider Instance => this.CurrentInstance;

		#endregion

	}
}
