﻿using System.Collections.Generic;
using YearbookViewer.App.Code.Factories.EmptyImplementation;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.DL.Implementations;
using YearbookViewer.Elastic.Implementations;

namespace YearbookViewer.App.Code.Factories
{
	/// <summary>
	/// Tovární třída, poskytující přístup k vyhodnocení výrazu.
	/// </summary>
	public class ExpressionEvaluatorsFactory : FactoryBase<IExpressionEvaluator>, IExpressionEvaluator
	{
		#region Singleton

		/// <summary>
		/// Provideři jsou vždy singleton v rámci aplikace.
		/// </summary>

		private readonly EmptyExpressionEvaluator emptyExpressionEvaluator = new EmptyExpressionEvaluator();
		private readonly RegexExpressionEvaluator regexExpressionEvaluator = new RegexExpressionEvaluator();
		private readonly ElasticExpressionEvaluator elasticExpressionEvaluator = new ElasticExpressionEvaluator();

		#endregion

		/// <summary>
		/// Vytvoří novou instanci tovární třídy, obsahující všechny instance pro vyhodnocení výrazů.
		/// </summary>
		/// <param name="settingsProvider">Nastavení aplikace.</param>
		public ExpressionEvaluatorsFactory(IUserSettingsProvider settingsProvider) : base(settingsProvider) { }

		/// <summary>
		/// Vrací slovník, ve kterém se nachází všechny instance pro vyhodnocení výrazů.
		/// </summary>
		protected override Dictionary<DataSourceType, IExpressionEvaluator> InstancesDictionary =>
			new Dictionary<DataSourceType, IExpressionEvaluator>()
			{
				{ DataSourceType.DefaultEmpty, emptyExpressionEvaluator },
				{ DataSourceType.FileBrowser, regexExpressionEvaluator },
				{ DataSourceType.ElasticSearch, elasticExpressionEvaluator }
			};

		#region Implementace

		/// <inheritdoc cref="IExpressionEvaluator.IsValid"/>
		public bool IsValid(string expr)
		{
			return this.CurrentInstance.IsValid(expr);
		}

		/// <inheritdoc cref="IExpressionEvaluator.Split"/>
		public string[] Split(string text, string expr, bool isExpression = false)
		{
			return this.CurrentInstance.Split(text, expr, isExpression);
		}

		/// <inheritdoc cref="IExpressionEvaluator.IsMatch"/>
		public bool IsMatch(string text, string expr, bool isExpression = false)
		{
			return this.CurrentInstance.IsMatch(text, expr, isExpression);
		}

		/// <inheritdoc cref="IExpressionEvaluator.Matches"/>
		public ICollection<IMatchData> Matches(string text, string expr, bool isExpression = false)
		{
			return this.CurrentInstance.Matches(text, expr, isExpression);
		}

		/// <summary>
		/// Vrací aktuální instanci, se kterou se pracuje.
		/// </summary>
		public IExpressionEvaluator Instance => this.CurrentInstance;

		#endregion
	}
}
