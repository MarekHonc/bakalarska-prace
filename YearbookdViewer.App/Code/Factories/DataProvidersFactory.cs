﻿using System.Collections.Generic;
using YearbookViewer.App.Code.Factories.EmptyImplementation;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.DL.Implementations;
using YearbookViewer.Elastic.Implementations;

namespace YearbookViewer.App.Code.Factories
{
	/// <summary>
	/// Tovární třída, poskytující přístup k datům.
	/// </summary>
	public class DataProviderFactory : FactoryBase<IContentDataProvider>, IContentDataProvider
	{
		#region Singleton

		/// <summary>
		/// Provideři jsou vždy singleton v rámci aplikace.
		/// </summary>

		private readonly EmptyDataProvider emptyDataProvider = new EmptyDataProvider();
		private readonly YearBookContentDataFromFilesProvider yearBookContentDataFromFilesProvider = new YearBookContentDataFromFilesProvider();
		private readonly ElasticContentDataProvider elasticContentDataProvider = new ElasticContentDataProvider();

		#endregion

		/// <summary>
		/// Vytvoří novou instanci tovární třídy, obsahující všechny datové providery.
		/// </summary>
		/// <param name="settingsProvider">Nastavení aplikace.</param>
		public DataProviderFactory(IUserSettingsProvider settingsProvider) : base(settingsProvider) { }

		/// <summary>
		/// Vrací slovník, ve kterém se nachází všechny instance datových providerů.
		/// </summary>
		protected override Dictionary<DataSourceType, IContentDataProvider> InstancesDictionary =>
			new Dictionary<DataSourceType, IContentDataProvider>()
			{
				{ DataSourceType.DefaultEmpty, emptyDataProvider },
				{ DataSourceType.FileBrowser, yearBookContentDataFromFilesProvider },
				{ DataSourceType.ElasticSearch, elasticContentDataProvider }
			};

		#region Implementace

		/// <inheritdoc cref="IContentDataProvider.GetFirst"/>.
		public IContentData GetFirst(IFilterData filters)
		{
			return this.CurrentInstance.GetFirst(filters);
		}

		/// <inheritdoc cref="IContentDataProvider.GetCount"/>.
		public IReadOnlyCollection<IContentData> GetCount(IFilterData filters, int count, int? offset = null)
		{
			return this.CurrentInstance.GetCount(filters, count, offset);
		}

		/// <inheritdoc cref="IContentDataProvider.Save"/>.
		public bool Save(IFilterData filters, string newText)
		{
			return this.CurrentInstance.Save(filters, newText);
		}

		/// <summary>
		/// Vrací aktuální instanci, se kterou se pracuje.
		/// </summary>
		public IContentDataProvider Instance => this.CurrentInstance;

		#endregion

	}
}
