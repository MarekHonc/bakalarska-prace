﻿namespace YearbookViewer.App.Code.Factories
{
	/// <summary>
	/// Enumerátor, obsahující všechny dostupné zdroje dat.
	/// </summary>
	public enum DataSourceType
	{
		/// <summary>
		/// Výchozí zdroj dat, který neposkytuje žádná data, vhodný například pokud chci umožnit spuštění aplikace bez dat.
		/// </summary>
		DefaultEmpty = 0,

		/// <summary>
		/// Zdroj dat, který získává data procházením adresářové struktury.
		/// </summary>
		FileBrowser = 1,

		/// <summary>
		/// Zdroj dat, který získává data z Elastic search.
		/// </summary>
		ElasticSearch = 2
	}
}
