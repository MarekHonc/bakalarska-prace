﻿using System.Collections.Generic;
using YearbookViewer.App.Code.Interfaces;

namespace YearbookViewer.App.Code.Factories
{
	/// <summary>
	/// Základní třída, pro všechny tovární třídy, poskytující přístup pro práci s daty.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public abstract class FactoryBase<T>
	{
		#region private variables

		private readonly IAppSettings settings;

		#endregion

		/// <summary>
		/// Vrací typ aktuálního providera.
		/// </summary>
		protected DataSourceType CurrentDataSource => settings.CurrentDateSource;

		/// <summary>
		/// Vrací slovník se všemi instancemi tříd, se kterými se pracuje.
		/// </summary>
		protected abstract Dictionary<DataSourceType, T> InstancesDictionary { get; }

		/// <summary>
		/// Vrací aktuální instanci, se kterou se právě pracuje.
		/// </summary>
		public T CurrentInstance => this.InstancesDictionary[this.CurrentDataSource];

		/// <summary>
		/// Vytvoří novou instanci tovární třídy.
		/// </summary>
		/// <param name="settingsProvider"></param>
		protected FactoryBase(IUserSettingsProvider settingsProvider)
		{
			this.settings = settingsProvider.Load();
		}
	}
}
