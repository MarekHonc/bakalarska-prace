﻿using System;
using System.Collections.Generic;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.App.Code.Factories.EmptyImplementation
{
	/// <summary>
	/// Prázdný data provider.
	/// </summary>
	public class EmptyDataProvider : IContentDataProvider
	{
		/// <summary>
		/// Prázdná implementace - vrací null.
		/// </summary>
		public IContentData GetFirst(IFilterData filters)
		{
			return null;
		}

		/// <summary>
		/// Prázdná implementace - vrací prázdné pole.
		/// </summary>
		public IReadOnlyCollection<IContentData> GetCount(IFilterData filters, int count, int? offset = null)
		{
			return Array.Empty<IContentData>();
		}

		/// <summary>
		/// Prázdná implementace - neukládá nic!
		/// </summary>
		public bool Save(IFilterData filters, string newText)
		{
			return false;
		}

		/// <summary>
		/// Vrací prázdnou implementaci <see cref="IContentDataProvider"/>.
		/// </summary>
		public IContentDataProvider Instance => this;
	}
}
