﻿using System;
using System.Collections.Generic;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.App.Code.Factories.EmptyImplementation
{
	/// <summary>
	/// Prázdný vyhodnocovač výrazů.
	/// </summary>
	public class EmptyExpressionEvaluator : IExpressionEvaluator
	{
		/// <summary>
		/// Prázdná implementace - vrací true.
		/// </summary>
		public bool IsValid(string expr)
		{
			return true;
		}

		/// <summary>
		/// Prázdná implementace - vrací prázdné pole.
		/// </summary>
		public string[] Split(string text, string expr, bool isExpression = false)
		{
			return Array.Empty<string>();
		}

		/// <summary>
		/// Prázdná implementace - vrací false.
		/// </summary>
		public bool IsMatch(string text, string expr, bool isExpression = false)
		{
			return false;
		}

		/// <summary>
		/// Prázdná implementace - vrací prázdnou kolekci.
		/// </summary>
		public ICollection<IMatchData> Matches(string text, string expr, bool isExpression = false)
		{
			return new List<IMatchData>(0);
		}

		/// <summary>
		/// Vrací prázdnou implementaci <see cref="IExpressionEvaluator"/>.
		/// </summary>
		public IExpressionEvaluator Instance => this;
	}
}
