﻿using System;
using YearbookViewer.Common.Interfaces;

namespace YearbookViewer.App.Code.Factories.EmptyImplementation
{
	/// <summary>
	/// Prázdný provider vyhledávacích filtrů.
	/// </summary>
	public class EmptySearchFilterProvider : ISearchFilterValueProvider
	{
		/// <summary>
		/// Prázdná implementace - vrací prázdné pole.
		/// </summary>
		public string[] GetTopLevelFilter()
		{
			return Array.Empty<string>();
		}

		/// <summary>
		/// Prázdná implementace - vrací prázdné pole.
		/// </summary>
		public string[] GetSecondaryFilter(string topLevelFilter)
		{
			return Array.Empty<string>();
		}

		/// <summary>
		/// Prázdná implementace - vrací prázdné pole.
		/// </summary>
		public string[] YearBookNames(string topLevelFilter, string secondaryFilter)
		{
			return Array.Empty<string>();
		}

		/// <summary>
		/// Vrací prázdnou implementaci <see cref="ISearchFilterValueProvider"/>.
		/// </summary>
		public ISearchFilterValueProvider Instance => this;
	}
}
