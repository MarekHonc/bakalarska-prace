﻿using System;
using System.Windows;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Všemožné užitečné metody.
	/// </summary>
	public static class AppUtils
	{
		/// <summary>
		/// Spustí akci na UI vlákně asynchronně - vhodné pro všechny akce,
		/// které mění objekty na kterých je závislé nějaké view.
		/// </summary>
		public static async Task RunOnUiThreadAsync(Func<Task> funcTask)
		{
			if (Application.Current == null)
				return;

			await Application.Current.Dispatcher.Invoke(funcTask, DispatcherPriority.ApplicationIdle);
		}

		/// <summary>
		/// Spustí akci na UI vlákně - vhodné pro všechny akce,
		/// které mění objekty na kterých je závislé nějaké view.
		/// </summary>
		public static void RunOnUiThread(Action func)
		{
			if (Application.Current == null)
				return;

			Application.Current.Dispatcher.BeginInvoke(func, DispatcherPriority.ApplicationIdle);
		}
	}
}
