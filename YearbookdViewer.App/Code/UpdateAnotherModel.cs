﻿using System;

namespace YearbookViewer.App.Code
{
	/// <summary>
	/// Atribut, umožňující upozornit na změnu dané property i jiný model.
	/// </summary>
	public class UpdateAnotherModelAttribute : Attribute
	{
		private string modelToUpdate;

		public UpdateAnotherModelAttribute(string modelToUpdate)
		{
			this.modelToUpdate = modelToUpdate;
		}

		/// <summary>
		/// Vrací nebo nastavuje model, na kterém proběhne update, pokud se tado property změní.
		/// </summary>
		public string ModelToUpdate
		{
			get => this.modelToUpdate;
		}
	}
}
