﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.App.ViewModels.ImportWizard;
using YearbookViewer.Common;
using YearbookViewer.Common.Enums;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Elastic;
using YearbookViewer.Readers;
using YearbookViewer.Readers.ImportGrouping;

namespace YearbookViewer.App.Code.ImportWizard
{
	/// <summary>
	/// Třída, která se stará o import dat podle předaných údajů.
	/// </summary>
	public class DataImporter
	{
		private readonly ImportWizardViewModel importViewModel;
		private readonly string dataFolder = Path.Combine(Constants.FileStoreLocation, Constants.DataFolderName);
		private static readonly string[] compatibleExtensions = Constants.CompatibleTextExtensions.Concat(Constants.CompatibleImageExtensions).ToArray();

		/// <summary>
		/// Vytvoří novou instanci importeru dat.
		/// </summary>
		/// <param name="viewModel">Viewmodel s nastavením importu.</param>
		public DataImporter(ImportWizardViewModel viewModel)
		{
			this.importViewModel = viewModel;
		}

		/// <summary>
		/// Vrací, zda-li se má pro vyhledávání používat elastic (import se musí promítnou i do elastiku).
		/// </summary>
		public bool IsUsingElastic => this.importViewModel.ContentProviderStep.SelectedDataSource == DataSourceType.ElasticSearch;

		/// <summary>
		/// Provede import dat do aplikace.
		/// </summary>
		public async Task<FilterMode> DoImport(Action<int> progress)
		{
			// Vyčistím aplikaci od dat.
			await ClearData();

			// Pokud se mají používat custom filtry.
			if (this.importViewModel.DataSelectStep.UseAdvancedFilters)
			{
				return await ImportAdvanced(progress);
			}

			return await ImportDefault(progress);
		}

		#region Private helpers

		/// <summary>
		/// Provede import s výchozím nastavením (standardní postup importu).
		/// </summary>
		/// <returns></returns>
		private async Task<FilterMode> ImportDefault(Action<int> progress)
		{
			var directoryDepth = importViewModel.DataSelectStep.DirectoriesDepth;

			// Pokud mám soubory přímo ve složce.
			if (directoryDepth == 0)
			{
				return await ImportZeroDepth(progress);
			}
			
			// Jedna vnořená úroveň.
			if (directoryDepth == 1)
			{
				return await ImportOneDepth(progress);
			}
			
			// Více než dvě úrovně.
			if(directoryDepth >= 2)
			{
				return await ImportTwoAndMoreDepth(progress);
			}

			// Nepodporovaná úroveň vnoření.
			throw new NotImplementedException($"Directories depth of {directoryDepth} is not supported");
		}

		/// <summary>
		/// Provede import na složce s nulovou hloubkou.
		/// </summary>
		private async Task<FilterMode> ImportZeroDepth(Action<int> progress)
		{
			// Vytvořím základní složku.
			var topLevel = Path.Combine(dataFolder, Constants.DefaultTopLevelFilter);

			Directory.CreateDirectory(topLevel);
			var topObj = await EnsureElasticTopFilterCreated(Constants.DefaultTopLevelFilter);

			// Jediná platná možnost filtru je groupování.
			if (this.importViewModel.DataSelectStep.SetupGroupFilter)
			{
				// Zjistím skupiny.
				var groups =
					ImportGroupingProvider.GetGroups(this.importViewModel.FiltersStep.SelectedGroupFilter.FilterValue);

				await ProcessGroup(progress, groups, Path.GetFileName(this.importViewModel.DataSelectStep.FilePath),
					topLevel, topObj, this.importViewModel.DataSelectStep.FilePath);

				return FilterMode.SecondaryFilter | FilterMode.PageNameFilter;
			}
			else
			{
				// Vytvořím i základní druhý filter.
				var secondaryLevelFilter = Path.Combine(topLevel, Constants.SecondaryLevelFilter);

				Directory.CreateDirectory(secondaryLevelFilter);
				var secondaryObj = await EnsureElasticSecondaryFilterCreated(topObj, Constants.SecondaryLevelFilter);

				// Překopíruji.
				await FileBrowser.CopyFiles(progress, this.importViewModel.DataSelectStep.FilePath,
					secondaryLevelFilter, compatibleExtensions,
					onFileCopied: async (result) => await EnsureElasticContentCreated(topObj, secondaryObj, result)
				);

				return FilterMode.PageNameFilter;
			}
		}

		/// <summary>
		/// Provede import na složce s hloubkou 1.
		/// </summary>
		private async Task<FilterMode> ImportOneDepth(Action<int> progress)
		{
			// Model s prvním filtrem.
			var firstFilterModel = this.importViewModel.FiltersStep.SelectedFirstFilter;

			// Model s druhým filtrem.
			var directories = FileBrowser.GetAllChildrenDirectoriesNames(
				this.importViewModel.DataSelectStep.FilePath,
				ConvertToExpression(firstFilterModel?.Value, firstFilterModel?.FilterValue)
			);

			// Počet dokončených přesunutí.
			var processed = 0;

			if (this.importViewModel.DataSelectStep.SetupGroupFilter)
			{
				// Zjistím skupiny.
				var groups =
					ImportGroupingProvider.GetGroups(this.importViewModel.FiltersStep.SelectedGroupFilter.FilterValue);

				// Složky první úrovně.
				foreach (var topDirectory in directories)
				{
					// Vytvořím složku kam se data budou kopírovat.
					var topLevelPathSource = Path.Combine(this.importViewModel.DataSelectStep.FilePath, topDirectory);
					var topLevelPathDestination = Path.Combine(dataFolder, topDirectory);

					// Vytvořím složku, kam data nakopíruji.
					Directory.CreateDirectory(topLevelPathDestination);
					var topObj = await EnsureElasticTopFilterCreated(topDirectory);

					// Zpracuji.
					processed = await ProcessGroup(progress, groups, topDirectory, topLevelPathDestination, topObj, topLevelPathSource, processed);
				}

				return FilterMode.TopLevelFilter | FilterMode.SecondaryFilter | FilterMode.PageNameFilter;
			}
			else
			{
				// Vytvořím základní složku.
				var topLevel = Path.Combine(dataFolder, Constants.DefaultTopLevelFilter);

				Directory.CreateDirectory(topLevel);
				var topObj = await EnsureElasticTopFilterCreated(Constants.DefaultTopLevelFilter);

				// Projdu všechny složky.
				foreach (var directory in directories)
				{
					// Vytvořím složku kam se data budou kopírovat.
					var secondaryLevelPathSource = Path.Combine(this.importViewModel.DataSelectStep.FilePath, directory);
					var secondaryLevelPathDestination = Path.Combine(topLevel, directory);

					// Vytvořím složku, kam data nakopíruji.
					Directory.CreateDirectory(secondaryLevelPathDestination);
					var secondaryObj = await EnsureElasticSecondaryFilterCreated(topObj, directory);

					processed = await FileBrowser.CopyFiles(progress, secondaryLevelPathSource, 
						secondaryLevelPathDestination, compatibleExtensions, alreadyProcessed: processed,
						onFileCopied: async (result) => await EnsureElasticContentCreated(topObj, secondaryObj, result)
					);
				}

				return FilterMode.SecondaryFilter | FilterMode.PageNameFilter;
			}
		}

		private async Task<int> ProcessGroup(Action<int> progress, Dictionary<string, List<GroupingModel>> groups,
			string topDirectory, string topLevelPathDestination, ElasticTopLevelFilter topObj, string topLevelPathSource, int processed = 0)
		{
			// Vytáhnu si obsah.
			var groupsForCurrent = groups.FirstOrDefault(g => topDirectory.Contains(g.Key));

			// A musím zpracovat všechny obsahy.
			foreach (var secondaryDirectory in groupsForCurrent.Value)
			{
				var destinationRoot = Path.Combine(topLevelPathDestination, secondaryDirectory.ToString().EnsureValidDirectoryName());

				// Vytvořím složku, kam data nakopíruji.
				Directory.CreateDirectory(destinationRoot);
				var secondaryObj = await EnsureElasticSecondaryFilterCreated(topObj, secondaryDirectory.ToString().EnsureValidDirectoryName());

				// A překopíruji - musím si řešit sám.
				foreach (var fileName in Directory.EnumerateFiles(topLevelPathSource))
				{
					var match = Regex.Match(Path.GetFileName(fileName), @"s\d+");

					// Zjistím aktuální stránku.
					if (match.Success)
					{
						var pageNumber = int.Parse(match.Value.Replace("s", string.Empty));

						if ((pageNumber >= secondaryDirectory.StartPage ||
						     (pageNumber == 0 && secondaryDirectory.StartPage == 1)) &&
						    (!secondaryDirectory.EndPage.HasValue || pageNumber <= secondaryDirectory.EndPage))
						{
							// Překopíruji soubor (musím po jednom, nemám zaručeno, že jsou ve stejné složce).
							var newFile = await FileBrowser.CopyFile(fileName, destinationRoot);
							if(newFile.IsNullOrEmpty())
								continue;

							await EnsureElasticContentCreated(topObj, secondaryObj, newFile);

							// Soubor překopírován.
							processed++;

							// A dám vědět do progressu.
							progress(processed);
						}
					}
				}
			}

			return processed;
		}

		/// <summary>
		/// Provede import na složce s hloubkou 2 a více.
		/// </summary>
		private async Task<FilterMode> ImportTwoAndMoreDepth(Action<int> progress)
		{
			// Model s prvním filtrem.
			var firstFilterModel = this.importViewModel.FiltersStep.SelectedFirstFilter;

			// Model s druhým filtrem.
			var directories = FileBrowser.GetAllChildrenDirectoriesNames(
				this.importViewModel.DataSelectStep.FilePath,
				ConvertToExpression(firstFilterModel?.Value, firstFilterModel?.FilterValue)
			);

			// Přesunuté soubory.
			var processed = 0;

			// Projdu všechny složky.
			foreach (var topDirectory in directories)
			{
				// Vytvořím složku kam se data budou kopírovat.
				var topLevelPathSource = Path.Combine(this.importViewModel.DataSelectStep.FilePath, topDirectory);
				var topLevelPathDestination = Path.Combine(dataFolder, topDirectory);

				// Vytvořím složku, kam data nakopíruji.
				Directory.CreateDirectory(topLevelPathDestination);
				var topObj = await EnsureElasticTopFilterCreated(topDirectory);

				// Model se sekundárními filtry.
				var secondaryFilterModel = this.importViewModel.FiltersStep.SelectedSecondFilter;

				// A zpracovávám další vnořené složky.
				var secondaryDirectories = FileBrowser.GetAllChildrenDirectoriesNames(
					topLevelPathSource,
					ConvertToExpression(secondaryFilterModel?.Value, secondaryFilterModel?.FilterValue)
				);

				// A projdu složky.
				foreach (var secondaryDirectory in secondaryDirectories)
				{
					// Zjistím si cesty.
					var currentRoot = Path.Combine(topLevelPathSource, secondaryDirectory);
					var destinationRoot = Path.Combine(topLevelPathDestination, secondaryDirectory);

					// Vytvořím složku, kam data nakopíruji.
					Directory.CreateDirectory(destinationRoot);
					var secondaryObj = await EnsureElasticSecondaryFilterCreated(topObj, secondaryDirectory);

					// A soubory zkopíruji.
					processed = await FileBrowser.CopyFiles(progress, currentRoot, 
						destinationRoot, compatibleExtensions, alreadyProcessed: processed,
						onFileCopied: async (result) => await EnsureElasticContentCreated(topObj, secondaryObj, result)
					);
				}
			}

			// Aplikace má všechny tři filtry.
			return FilterMode.TopLevelFilter | FilterMode.SecondaryFilter | FilterMode.PageNameFilter;
		}

		/// <summary>
		/// Provede import dat podle speciálního nastavení uživatele.
		/// </summary>
		private async Task<FilterMode> ImportAdvanced(Action<int> progress)
		{
			// Počet překopírovaných souborů.
			var filesCopied = 0;

			// Zatím mám celé filtry, pokud bude třeba, postupně zredukuji.
			var result = FilterMode.TopLevelFilter | FilterMode.SecondaryFilter | FilterMode.PageNameFilter;

			// Mám jeden hlavní filtr.
			if (importViewModel.AdvancedFiltersStep.AllFilesToImport.Count == 1)
			{
				// Zobrazovat první položku je zbytečné.
				result = FilterMode.SecondaryFilter | FilterMode.PageNameFilter;

				// Mám i jednu položku sekundárního filtru -> zobrazovat je ho zbytečné.
				if (importViewModel.AdvancedFiltersStep.AllFilesToImport.First().Value.Count == 1)
				{
					result = FilterMode.PageNameFilter;
				}
			}

			// Projdu celý slovník s importem a vytvořím korespondující složky a překopíruji soubory.
			foreach (var topLevelFolder in this.importViewModel.AdvancedFiltersStep.AllFilesToImport)
			{
				string topFolder;
				string topFolderName;

				// Count 1 -> nemá cenu filtr zobrazovat.
				if (result < FilterMode.TopLevelFilter)
				{
					topFolder = Path.Combine(dataFolder, Constants.DefaultTopLevelFilter);
					topFolderName = Constants.DefaultTopLevelFilter;
				}
				else
				{
					topFolder = Path.Combine(dataFolder, topLevelFolder.Key);
					topFolderName = topLevelFolder.Key;
				}

				// Vytvořím složku.
				Directory.CreateDirectory(topFolder);
				var topObj = await EnsureElasticTopFilterCreated(topFolderName);

				// Druhá úroveň.
				foreach (var secondaryLevelFolder in topLevelFolder.Value)
				{
					string secondaryFolder;
					string secondaryFolderName;

					// Count 1 -> nemá cenu filtr zobrazovat.
					if (result < FilterMode.SecondaryFilter)
					{
						secondaryFolder = Path.Combine(topFolder, Constants.SecondaryLevelFilter);
						secondaryFolderName = Constants.SecondaryLevelFilter;
					}
					else
					{
						secondaryFolder = Path.Combine(topFolder, secondaryLevelFolder.Key);
						secondaryFolderName = secondaryLevelFolder.Key;
					}

					// Vytvořím složku.
					Directory.CreateDirectory(secondaryFolder);
					var secondaryObj = await EnsureElasticSecondaryFilterCreated(topObj, secondaryFolderName);

					// Překopíruji soubory.
					foreach (var file in secondaryLevelFolder.Value)
					{
						// Překopíruji soubor (musím po jednom, nemám zaručeno, že jsou ve stejné složce).
						var newFile = await FileBrowser.CopyFile(file.Item1, secondaryFolder);
						if (newFile.IsNullOrEmpty())
							continue;

						await EnsureElasticContentCreated(topObj, secondaryObj, newFile);

						// Soubor překopírován.
						filesCopied++;

						// A dám vědět do progressu.
						progress(filesCopied);
					}
				}
			}

			return result;
		}

		/// <summary>
		/// Vrací převedený typ filtru jako výraz.
		/// </summary>
		private static string ConvertToExpression(AvailableFilters? type, string value)
		{
			// Pokud je typ null.
			if (type == null)
				return null;

			// Jinak konvertiji na regex.
			switch (type.Value)
			{
				case AvailableFilters.BeginsWith:
					return value + "*";
				case AvailableFilters.EndsWith:
					return "*" + value;
				case AvailableFilters.Contains:
					return "*" + value + "*";
				default:
					throw new NotImplementedException($"Filter type of {type} is not convertible to expression.");
			}
		}

		/// <summary>
		/// Vymaže všechna data, která aplikace používá.
		/// </summary>
		private async Task ClearData()
		{
			// Smažu původní data.
			Directory.Delete(dataFolder, true);

			// Pokud používám elastic, tak ho vyčistím.
			if (this.IsUsingElastic)
			{
				await ElasticContext.RemoveAllExisting();
			}
		}

		#region Elastic search

		/// <summary>
		/// Zajistí, že filtr hlavní úrovně je vytvořen i v Elastic search.
		/// </summary>
		private async Task<ElasticTopLevelFilter> EnsureElasticTopFilterCreated(string topLevel)
		{
			if (!this.IsUsingElastic)
				return null;

			var model = await ElasticContext.CreateTopLevel(topLevel);
			return model;
		}

		/// <summary>
		/// Zajistí, že filtr druhý úrovně je vytvořen i v Elastic search.
		/// </summary>
		private async Task<ElasticSecondaryLevel> EnsureElasticSecondaryFilterCreated(ElasticTopLevelFilter topLevel, string secondLevel)
		{
			if (!this.IsUsingElastic)
				return null;

			var model = await ElasticContext.CreateSecondaryLevel(topLevel, secondLevel);
			return model;
		}

		/// <summary>
		/// Zajistí, že data obrázku a textu jou vytvořeny i v Elastic search.
		/// </summary>
		private async Task<ElasticContent> EnsureElasticContentCreated(ElasticTopLevelFilter topLevel,
			ElasticSecondaryLevel secondaryLevel, string pathToFile)
		{
			if (!this.IsUsingElastic)
				return null;

			// Zjistím koncovku.
			var extension = Path.GetExtension(pathToFile).Replace(".", string.Empty);

			// Pustím pouze textové soubory.
			if (!Constants.CompatibleTextExtensions.Contains(extension))
				return null;

			// zjistím korespondující čtečku.
			var reader = CustomTextReader.GetReader(extension);

			// Musím přečíst všechny hodnoty.
			var model = await ElasticContext.CreateContent(
				topLevel,
				secondaryLevel,
				Path.GetFileName(pathToFile),
				pathToFile,
				string.Join(Environment.NewLine, reader.Read(pathToFile)),
				CustomTextReader.GetImageForText(pathToFile)
			);

			return model;
		}

		#endregion

		#endregion
	}
}
