﻿namespace YearbookViewer.App.Code.ImportWizard
{
	/// <summary>
	/// Výčet všech dostupných nastavení filtrů pro import.
	/// </summary>
	public enum AvailableFilterConfigurations
	{
		/// <summary>
		/// Filtr, respektující adresářovou strukturu.
		/// </summary>
		FileHierarchy,

		/// <summary>
		/// Filtr, umožňující volit rozsahy (např. při číselném pojmenování souborů).
		/// </summary>
		ContentRanges,

		/// <summary>
		/// Filtr, umožňující výběr pomocí regexu (např. všechny složky začínající s "r").
		/// </summary>
		Regex,
	}
}
