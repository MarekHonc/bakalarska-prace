﻿namespace YearbookViewer.App.Code.ImportWizard
{
	/// <summary>
	/// Rozhraní označující stránku wizardu, po které následuje další.
	/// </summary>
	public interface IWithNextPage
	{
		/// <summary>
		/// Vrací stránku, která následuje po této.
		/// </summary>
		WizardPage NextPage { get; }
	}
}
