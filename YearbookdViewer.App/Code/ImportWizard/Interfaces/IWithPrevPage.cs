﻿namespace YearbookViewer.App.Code.ImportWizard
{
	/// <summary>
	/// Rozhraní označující stránku wizardu, kterému předcházela stránka.
	/// </summary>
	public interface IWithPrevPage
	{
		/// <summary>
		/// Vrací stránku, která následuje po této.
		/// </summary>
		WizardPage PrevPage { get; }
	}
}
