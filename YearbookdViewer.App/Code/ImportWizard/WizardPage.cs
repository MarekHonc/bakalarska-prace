﻿namespace YearbookViewer.App.Code.ImportWizard
{
	/// <summary>
	/// Výčet všech stránek import wizardu.
	/// </summary>
	public enum WizardPage
	{
		/// <summary>
		/// První stánka importu, informace o tom, co se bude dít.
		/// </summary>
		Welcome = 0,

		/// <summary>
		/// Stránka, kde se vybírá jak se bude mezi daty vyhledávat.
		/// </summary>
		ContentProvider = 1,

		/// <summary>
		/// Stránka, kde si uživatel vybírá data.
		/// </summary>
		DataSelect = 2,

		/// <summary>
		/// Stránka s konfigurací prvního filtru.
		/// </summary>
		Filters = 3,

		/// <summary>
		/// Stránka s definicí naprosto custom importu.
		/// </summary>
		AdvancedFilters = 4,

		/// <summary>
		/// Stránka s potvrzením před importem.
		/// </summary>
		Confirm = 5,

		/// <summary>
		/// Stránka se závěrečným shrnutím.
		/// </summary>
		Finish = 6
	}
}
