﻿using YearbookViewer.Readers.TesseractOCR;

namespace YearbookViewer.App.Models.Comboboxes
{
	/// <summary>
	/// Třída, reprezentující model pro výběr jazyku v komboboxu.
	/// </summary>
	public class OCRLanguageComboboxValueModel : BaseComboboxValueModel<OCRLanguage>
	{
		public OCRLanguageComboboxValueModel(OCRLanguage value, int index)
			: base(value, index)
		{
		}

		public override string DisplayText => this.Value.ToString();
	}
}
