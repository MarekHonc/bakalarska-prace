﻿using System.Globalization;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.Models.Comboboxes
{
	/// <summary>
	/// Model pro kombobox s výběrem kultury.
	/// </summary>
	public class CultureComboboxValueModel : BaseComboboxValueModel<CultureInfo>
	{
		public CultureComboboxValueModel(CultureInfo value, int index)
			: base(value, index)
		{
		}

		/// <summary>
		/// Vrací text pro zobrazení (už hezký text).
		/// </summary>
		public override string DisplayText => this.Value.NativeName.FirstCharToUpper();
	}
}
