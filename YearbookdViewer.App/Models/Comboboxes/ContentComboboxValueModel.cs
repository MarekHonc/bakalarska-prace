﻿using System.Text.RegularExpressions;

namespace YearbookViewer.App.Models.Comboboxes
{
	/// <summary>
	/// Třída pro kombobox s výběrem obsahů.
	/// </summary>
	public class ContentComboboxValueModel : BaseComboboxValueModel<string>
	{
		public ContentComboboxValueModel(string value, int index, string year)
			: base(value, index)
		{
			this.Year = year;
		}

		/// <summary>
		/// Vrací nebo nastvuje ke kterému patří roku.
		/// </summary>
		public string Year
		{
			get;
		}

		/// <summary>
		/// Vrací text pro zobrazení (už hezký text).
		/// </summary>
		public override string DisplayText => Regex.Replace(this.Value, @"\[\d+\]", string.Empty).Trim();
	}
}
