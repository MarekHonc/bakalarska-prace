﻿namespace YearbookViewer.App.Models.Comboboxes
{
	/// <summary>
	/// Model pro zobrazení stránek do comboboxu.
	/// </summary>
	public class PageNameComboboxValueModel : BaseComboboxValueModel<string>
	{
		public PageNameComboboxValueModel(string value, int index, string year)
			: base(value, index) 
		{
			this.Year = year;
		}

		/// <summary>
		/// Vrací nebo nastvuje ke kterému patří roku.
		/// </summary>
		public string Year
		{
			get;
		}

		/// <summary>
		/// Vrací text pro zobrazení (už hezký text).
		/// </summary>
		public override string DisplayText
		{
			get
			{
				// Zjistím poslední index tečky.
				var index = this.Value.LastIndexOf('.');

				// Pokud není, použiji délku stringu.
				if (index < 0)
					index = this.Value.Length;

				// A oříznu koncovky.
				return this.Value.Substring(0, index);
			}
		}
	}
}
