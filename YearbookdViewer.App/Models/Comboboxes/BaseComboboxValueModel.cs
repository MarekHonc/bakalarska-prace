﻿namespace YearbookViewer.App.Models.Comboboxes
{
	/// <summary>
	/// Model, pro zobrazování Comboboxů.
	/// </summary>
	public abstract class BaseComboboxValueModel<T>
	{
		/// <summary>
		/// Vytvoří novou instanci základního mode pro combobox.
		/// </summary>
		/// <param name="value">Hodnota položky.</param>
		/// <param name="index">Index položky.</param>
		public BaseComboboxValueModel(T value, int index)
		{
			this.Value = value;
			this.Index = index;
		}

		/// <summary>
		/// Vrací nebo nastavuje index, na kterém se položka nachází.
		/// </summary>
		public int Index
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje hodnotu aktuální položky.
		/// </summary>
		public T Value
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací zformátovaný text, pro zobrazení do comboboxu.
		/// </summary>
		public abstract string DisplayText
		{
			get;
		}

		/// <summary>
		/// Oveeride Equals aby .NET bral v potaz hodnoty objektu.
		/// </summary>
		/// <remarks>Je zde aby se správně choval combobox, pri změně vybrané položky z kódu.</remarks>
		public override bool Equals(object obj)
		{
			if (obj is BaseComboboxValueModel<T> model)
			{
				return model.Index == this.Index && model.Value.Equals(this.Value);
			}

			return base.Equals(obj);
		}

		/// <summary>
		/// Oveeride GetHashCode aby .NET bral bral v potaz hodnoty objektu.
		/// </summary>
		/// <remarks>Je zde aby se správně choval combobox, pri změně vybrané položky z kódu.</remarks>
		public override int GetHashCode()
		{
			return this.Index + this.Value.GetHashCode();
		}
	}
}
