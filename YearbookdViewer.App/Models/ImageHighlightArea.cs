﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.Models
{
	/// <summary>
	/// Vrací nebo nastavuje model, podle kterého se zvýrazňují oblasti v obrázku.
	/// </summary>
	public class ImageHighlightArea
	{
		/// <summary>
		/// Vytvoří novou instanci model, který informuje o zvýraznění oblasti.
		/// </summary>
		private ImageHighlightArea(int x, int y, int height, int width, string text)
		{
			X = x;
			Y = y;
			Height = height;
			Width = width;
			Text = text;
		}

		/// <summary>
		/// Vrací X souřadnici výskytu.
		/// </summary>
		public int X
		{
			get;
		}

		/// <summary>
		/// Vrací Y souřadnici výskytu.
		/// </summary>
		public int Y
		{
			get;
		}

		/// <summary>
		/// Vrací výšku zvýrazněné oblasti.
		/// </summary>
		public int Height
		{
			get;
		}

		/// <summary>
		/// Vrací šířku zvýrazněné oblasti.
		/// </summary>
		public int Width
		{
			get;
		}

		/// <summary>
		/// Vrací text zvýrazněné oblasti.
		/// </summary>
		public string Text
		{
			get;
		}

		/// <summary>
		/// Vytvoří všechny zvýrazněné oblasti, na základě vyhledávaných klíčových slov.
		/// </summary>
		public static ImageHighlightArea[] Create(string path, string searchText, bool highlightWholeWord)
		{
			if (searchText.IsNullOrEmpty())
				return Array.Empty<ImageHighlightArea>();

			IEnumerable<string> lines;

			try
			{
				// První řádek je header.
				lines = File.ReadAllLines(path).Skip(1);
			}
			catch (Exception)
			{
				return Array.Empty<ImageHighlightArea>();
			}

			// Ukládám do concurent bagu - zpracování pomocí paralelního foreach.
			var result = new ConcurrentBag<ImageHighlightArea>();

			// Pomocí parallel -> Pracuji v rámci paměti je to ok.
			_ = Parallel.ForEach(lines, line =>
			{
				var splitted = line.Split('\t');

				if (splitted[11].IsNullOrEmpty())
					return;

				var highLight = new ImageHighlightArea(
					(int)Math.Ceiling(double.Parse(splitted[6])),
					(int)Math.Ceiling(double.Parse(splitted[7])),
					(int)Math.Ceiling(double.Parse(splitted[9])),
					(int)Math.Ceiling(double.Parse(splitted[8])),
					splitted[11]
				);

				if (highLight.Text.Contains(searchText))
				{
					if (highLight.Text.Length == searchText.Length || highlightWholeWord)
					{
						result.Add(highLight);
					}
					else
					{
						// Velikost jednoho znaku.
						var characterWidth = (double)highLight.Width / highLight.Text.Length;

						// Vypočítám šířku nálezu vůči celému slovu.
						var newWidth = (int) (characterWidth * searchText.Length);

						// Vypočítám o kolik musím posunout x
						var index = highLight.Text.IndexOf(searchText, StringComparison.InvariantCulture);
						var newX = highLight.X + (int)(index * characterWidth);

						// Aktualizuji nález.
						highLight = new ImageHighlightArea(
							newX,
							highLight.Y,
							highLight.Height,
							newWidth,
							highLight.Text
						);

						result.Add(highLight);
					}
				}
			});

			return result.ToArray();
		}
	}
}
