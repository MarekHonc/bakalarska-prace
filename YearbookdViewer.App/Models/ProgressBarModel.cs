﻿using YearbookViewer.App.Code;
using YearbookViewer.App.ViewModels.Base;

namespace YearbookViewer.App.Models
{
	/// <summary>
	/// Model pro používání progress baru.
	/// </summary>
	public class ProgressBarModel : ViewModelBase
	{
		private int progressBarValue;
		private int progressBarMaxValue;
		private bool progressBarVisible;

		/// <summary>
		/// Vrací nebo nastavuje, zda-li je viditelný progress bar.
		/// </summary>
		public bool Visible
		{
			get
			{
				return this.progressBarVisible;
			}
			set
			{
				if (this.progressBarVisible == value)
					return;

				this.progressBarVisible = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, zda-li je progress bar schovaný.
		/// </summary>
		[DependsUpon(nameof(Visible))]
		public bool Hidden => !this.Visible;

		/// <summary>
		/// Aktuální hodnota progressbaru - kolik je již hotovo.
		/// </summary>
		public int Value
		{
			get
			{
				return this.progressBarValue;
			}
			set
			{
				this.progressBarValue = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Maximální hodnota progress baru - kolik celkem musí být hotovo.
		/// </summary>
		public int MaxValue
		{
			get
			{
				return this.progressBarMaxValue;
			}
			set
			{
				this.progressBarMaxValue = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Resetuje progress bar.
		/// </summary>
		public void Reset()
		{
			this.Value = 0;
			this.Visible = false;
		}
	}
}
