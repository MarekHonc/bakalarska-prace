﻿using System;
using YearbookViewer.App.Localization;
using YearbookViewer.App.Models.Comboboxes;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.App.Models.Wizard.Comboboxes
{
	/// <summary>
	/// Combobox pro zobrazení způsobů filtrace dat.
	/// </summary>
	public class AvailableFilterComboboxValueModel : BaseComboboxValueModel<AvailableFilters>
	{
		public AvailableFilterComboboxValueModel(AvailableFilters value, int index) : base(value, index)
		{
		}

		/// <summary>
		/// Vrací nebo nastavuje hodnotu filtru.
		/// </summary>
		public string FilterValue
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje text k zobrazení.
		/// </summary>
		public override string DisplayText
		{
			get
			{
				switch (this.Value)
				{
					case AvailableFilters.BeginsWith:
						return ImportWizardResources.AvailableFilters_BeginsWith;
					case AvailableFilters.EndsWith:
						return ImportWizardResources.AvailableFilters_EndsWith;
					case AvailableFilters.Contains:
						return ImportWizardResources.AvailableFilters_Contains;
					case AvailableFilters.Contents:
						return ImportWizardResources.AvailableFilters_Contents;
					default:
						throw new Exception($"Unknown {nameof(AvailableFilters)} value: {this.Value}");
				}
			}
		}
	}
}
