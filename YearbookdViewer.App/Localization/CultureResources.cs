﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Forms;

namespace YearbookViewer.App.Localization
{
	/// <summary>
	/// Třída, obalující XAML přístup k resourcům.
	/// Vlastně upravuje jenom to, že se při použítí Bindingu nyní mohou měnit za běhu aplikace.
	/// </summary>
	public class CultureResources
	{
		#region Private variables

		private static bool bFoundInstalledCultures = false;
		private static ObjectDataProvider resourceProvider;
		private static ObjectDataProvider importResourceProvider;
		private static HashSet<CultureInfo> supportedCultures = new HashSet<CultureInfo>();

		#endregion

		/// <summary>
		/// Vrací všechny dostupné kultury (jazyky).
		/// </summary>
		public static HashSet<CultureInfo> SupportedCultures
		{
			get 
			{ 
				return supportedCultures;
			}
		}

		/// <summary>
		/// Vrací providera, který se stará o lokalizaci.
		/// </summary>
		public static ObjectDataProvider ResourceProvider
		{
			get
			{
				// Pokud ještě data providera nemám získám ho.
				if (resourceProvider == null)
					resourceProvider = (ObjectDataProvider)App.Current.FindResource("Resources");

				return resourceProvider;
			}
		}

		/// <summary>
		/// Vrací providera, který se stará o lokalizaci importu.
		/// </summary>
		public static ObjectDataProvider ImportResourceProvider
		{
			get
			{
				if (importResourceProvider == null)
					importResourceProvider = (ObjectDataProvider)App.Current.FindResource("ImportWizardResources");

				return importResourceProvider;
			}
		}

		/// <summary>
		/// Vrací providera, který se stará o lokalizaci exportu.
		/// </summary>
		public static ObjectDataProvider ExportResourceProvider
		{
			get
			{
				if (importResourceProvider == null)
					importResourceProvider = (ObjectDataProvider)App.Current.FindResource("ExportWizardResources");

				return importResourceProvider;
			}
		}

		/// <summary>
		/// "Konstruktor" statické třídy.
		/// </summary>
		static CultureResources()
		{
			// Nemám žádné kultury.
			if (!bFoundInstalledCultures)
			{
				foreach (string dir in Directory.GetDirectories(Application.StartupPath))
				{
					try
					{
						// Hledám v kompilovaných dll, co mám za dostupné kultury, aby přidání nové bylo co nejjednodušší.
						var dirinfo = new DirectoryInfo(dir);
						var foundCulure = CultureInfo.GetCultureInfo(dirinfo.Name);

						// Ještě kouknu jestli existuje pro danou kulturu .dll sobor, pokud ano -> mohu ho použít.
						if (dirinfo.GetFiles(Path.GetFileNameWithoutExtension(Application.ExecutablePath) + ".resources.dll").Length > 0)
						{
							supportedCultures.Add(foundCulure);
						}
					}
					catch (ArgumentException) // Výjimka prostě ignoruji.
					{

					}
				}

				bFoundInstalledCultures = true;
			}
		}

		/// <summary>
		/// Vrací instanci resourců, ze kterých se berou texty.
		/// </summary>
		public Resources GetResourceInstance()
		{
			return new Resources();
		}

		/// <summary>
		/// Vrací instanci resourců pro kroky import wizardu.
		/// </summary>
		public ImportWizardResources GetWizardResourceInstance()
		{
			return new ImportWizardResources();
		}

		/// <summary>
		/// Vrací instanci resourců pro export dat z aplikace.
		/// </summary>
		public ExportWizardResources GetExportResourceInstance()
		{
			return new ExportWizardResources();
		}

		/// <summary>
		/// Změní aktuální culture aplikace.
		/// </summary>
		/// <param name="culture">Cílená lokalizace.</param>
		public static void ChangeCulture(CultureInfo culture)
		{
			if (supportedCultures.Contains(culture))
			{
				Resources.Culture = culture;
				ImportWizardResources.Culture = culture;
				ExportWizardResources.Culture = culture;

				ResourceProvider.Refresh();
				// ImportResourceProvider.Refresh();
				// ExportResourceProvider.Refresh();
			}
		}
	}
}
