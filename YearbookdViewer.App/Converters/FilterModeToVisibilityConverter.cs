﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Converter, konvertující flags enum na viditelnost.
	/// </summary>
	public class FilterModeToVisibilityConverter : MarkupExtension, IValueConverter
	{
		/// <summary>
		/// Vrací nebo nastavuje hodnotu, při které bude prvek viditelný.
		/// </summary>
		public int VisibleValue
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací hodnotu, při které bude prvek viditelný přetypovanou na mód filtrů.
		/// </summary>
		public FilterMode VisibleValueEnum => (FilterMode)this.VisibleValue;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			// Pokud hodnota není Filter mode enum - skryji.
			if (!(value is FilterMode filterMode)) 
				return Visibility.Collapsed;
		
			// Jinak zobrazuji pokud požadovaný stav je menší roven aktuálnímu (nejnižší filtr má nejnižší hodnotu).
			if (this.VisibleValueEnum <= filterMode)
				return Visibility.Visible;

			return Visibility.Collapsed;

		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}

	}
}
