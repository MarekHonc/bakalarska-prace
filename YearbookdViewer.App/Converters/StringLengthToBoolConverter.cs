﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Koverter pro konverzi délky stringu na bool.
	/// </summary>
	public class StringLengthToBoolConverter : MarkupExtension, IValueConverter
	{
		/// <summary>
		/// Vrací nebo natavuje délku stringu, při které se vrací true.
		/// </summary>
		public int FalseValue
		{
			get;
			set;
		} = 0;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is string s)
			{
				return s.Length != this.FalseValue;
			}

			return false;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
