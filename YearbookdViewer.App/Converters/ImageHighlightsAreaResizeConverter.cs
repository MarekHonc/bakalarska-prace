﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Koverter, který upravuje rozměry vrácené z OCR pro Aplikaci.
	/// </summary>
	public class ImageHighlightsAreaResizeConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			if (values.Length == 3 && values[0] is int position && values[1] is int imageSize && values[2] is double displayedSize)
			{
				return (double)(position * displayedSize / imageSize);
			}

			return 0d;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
