﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Markup;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Konvertor konvertující bool hodnoty (z <c>false</c> udělá <c>true</c> a naopak).
	/// </summary>
	public class BoolValueConverter : MarkupExtension, IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is bool typedValue)
			{
				return !typedValue;
			}

			return false;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is bool typedValue)
			{
				return !typedValue;
			}

			return true;
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
