﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace YearbookViewer.App.Converters
{
	//  new GridLength(1, GridUnitType.Star);

	/// <summary>
	/// Konverter pro konverzi čísla na veliksot gridu.
	/// </summary>
	public class IntToGridLengthConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value is int length)
			{
				return new GridLength(length, GridUnitType.Star);
			}

			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if(value is GridLength length)
			{
				return length.Value;
			}

			return value;
		}
	}
}
