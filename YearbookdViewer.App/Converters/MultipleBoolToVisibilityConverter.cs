﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Konvertor konvertující více bool hodnot na visibilitu.
	/// Funguje tak, že pokud alespoň jeden v boolů je true, vrací <see cref="TrueValue"/>.
	/// </summary>
	public class MultipleBoolToVisibilityConverter : MarkupExtension, IMultiValueConverter
	{
		public MultipleBoolToVisibilityConverter()
		{
			this.TrueValue = Visibility.Visible;
			this.FalseValue = Visibility.Collapsed;
		}

		/// <summary>
		/// Vrací nebo nastavuje hodnotu viditelnosti, která se použije pokud je hodnota true.
		/// </summary>
		public Visibility TrueValue
		{
			get;
			set;
		}


		/// <summary>
		/// Vrací nebo nastavuje hodnotu viditelnosti, která se použije pokud je hodnota false.
		/// </summary>
		public Visibility FalseValue
		{
			get;
			set;
		}

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			// Pokud pracuji s bool hodnotou.
			foreach (var value in values)
			{
				if (value is bool boolValue && boolValue)
				{
					return this.TrueValue;
				}
			}

			// Jestli nemám bool tak prostě vrátím false.
			return this.FalseValue;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
