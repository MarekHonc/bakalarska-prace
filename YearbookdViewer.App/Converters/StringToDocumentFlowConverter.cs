﻿using System;
using System.IO;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Documents;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Coverter pro konverzi plain textu do FlowDocumentu, který vyžaduje např. RichTextBox.
	/// </summary>
	public class StringToDocumentFlowConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			FlowDocument doc = new FlowDocument();

			if (value is string s)
			{
				using (var reader = new StringReader(s))
				{
					string newLine;

					while ((newLine = reader.ReadLine()) != null)
					{
						Paragraph paragraph = new Paragraph();
						paragraph.Inlines.Add(new Run(newLine));
						doc.Blocks.Add(paragraph);
					}
				}
			}

			return doc;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var result = string.Empty;

			if (value is FlowDocument doc)
			{
				result = new TextRange(doc.ContentStart, doc.ContentEnd).Text;
			}

			return result;
		}
	}
}
