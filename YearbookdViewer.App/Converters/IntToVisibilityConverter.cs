﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Konvertor, konvertující číslo na viditelnost.
	/// </summary>
	public class IntToVisibilityConverter : MarkupExtension, IValueConverter
	{
		/// <summary>
		/// Vrací nebo nastavuje hodnotu, při které bude prvek zobrazen.
		/// </summary>
		public int VisibleValue
		{
			get;
			set;
		}

		/// <summary>
		/// Vrací nebo nastavuje jestli se má nastat přímá rovnost (<c>false</c>) nebo stačí když číslo bude větší (<c>true</c>)
		/// </summary>
		public bool OrMore
		{
			get;
			set;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is int integer)
			{
				if(integer == this.VisibleValue)
					return Visibility.Visible;

				if (this.OrMore && integer > this.VisibleValue)
					return Visibility.Visible;
			}

			return Visibility.Collapsed;
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}

		#region NotImplemented

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
