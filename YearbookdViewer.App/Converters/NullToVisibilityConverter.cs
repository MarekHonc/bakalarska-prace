﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Převání null na viditelnost objektu.
	/// </summary>
	public class NullToVisibilityConverter : MarkupExtension, IValueConverter
	{
		/// <summary>
		/// Vrací nebo nastavuje viditelnost v případě že objekt konverze je null.
		/// </summary>
		public Visibility NullValue
		{
			get;
			set;
		} = Visibility.Hidden;

		/// <summary>
		/// Vrací nebo nastavuje viditelnost v případě že objekt konverze není null.
		/// </summary>
		public Visibility NotNullValue
		{
			get;
			set;
		} = Visibility.Visible;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return value == null ? this.NullValue : this.NotNullValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
