﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace YearbookViewer.App.Converters
{
	/// <summary>
	/// Converter pro konverzi boolu na hodnotu viditelnosti.
	/// </summary>
	public class BoolToVisibilityConverter : MarkupExtension, IValueConverter
	{
		public BoolToVisibilityConverter()
		{
			this.TrueValue = Visibility.Visible;
			this.FalseValue = Visibility.Collapsed;
		}

		/// <summary>
		/// Vrací nebo nastavuje hodnotu viditelnosti, která se použije pokud je hodnota true.
		/// </summary>
		public Visibility TrueValue
		{ 
			get;
			set;
		}


		/// <summary>
		/// Vrací nebo nastavuje hodnotu viditelnosti, která se použije pokud je hodnota false.
		/// </summary>
		public Visibility FalseValue
		{ 
			get;
			set;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			// Pokud pracuji s bool hodnotou.
			if(value is bool boolValue)
			{
				return boolValue ? this.TrueValue : this.FalseValue;
			}

			// Jestli nemám bool tak prostě vrátím false.
			return this.FalseValue;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return this.TrueValue.Equals(value);
		}

		public override object ProvideValue(IServiceProvider serviceProvider)
		{
			return this;
		}
	}
}
