﻿using YearbookViewer.App.ViewModels;
using System.Windows;

namespace YearbookViewer.App.Views
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow(MainViewModel mainViewModel)
		{
			InitializeComponent();

			// DataContext okna.
			this.DataContext = mainViewModel;

			// Ještě si zaregistruji, že na změnu ve vyhledávaném termínu musím hodit i upadte na highlighter.
			// Našel jsem různě řešení jak convertery tak custom elemety, ale to mi nefunguje, pokud mě napadne lepší způsob, nahradím jím tento.
			mainViewModel.MainContentViewModel.PropertyChanged += (sender, args) =>
			{
				if (args.PropertyName == nameof(MainContentViewModel.SearchInText))
					MainContent.HighlighInText(mainViewModel.MainContentViewModel.SearchInText);
			};
		}
	}
}
