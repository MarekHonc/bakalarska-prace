﻿using System.Windows.Controls;

namespace YearbookViewer.App.Views.Popups
{
	/// <summary>
	/// Interaction logic for SearchResults.xaml
	/// </summary>
	public partial class SearchResults : UserControl
	{
		public SearchResults()
		{
			InitializeComponent();
		}
	}
}
