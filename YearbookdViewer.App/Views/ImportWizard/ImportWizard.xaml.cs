﻿using System.ComponentModel;
using System.Windows;
using YearbookViewer.App.ViewModels.ImportWizard;

namespace YearbookViewer.App.Views.ImportWizard
{
	/// <summary>
	/// Interaction logic for ImportWizzard.xaml
	/// </summary>
	public partial class ImportWizard : Window
	{
		private readonly ImportWizardViewModel context; 

		public ImportWizard(ImportWizardViewModel viewModel)
		{
			InitializeComponent();

			// Nastavím data context okna.
			this.DataContext = viewModel;
			this.context = viewModel;
		}

		/// <summary>
		/// Zavře okno s import wizardem.
		/// </summary>
		private void CloseWindow(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// Akce volaná před zavřením okna.
		/// </summary>
		protected override void OnClosing(CancelEventArgs e)
		{
			// Nedovolím zavřít pokud probíhá import.
			e.Cancel = this.context.ConfirmStep.ProgressBar.Visible;
			
			base.OnClosing(e);
		}
	}
}
