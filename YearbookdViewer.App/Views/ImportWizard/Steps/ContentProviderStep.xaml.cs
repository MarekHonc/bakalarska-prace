﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace YearbookViewer.App.Views.ImportWizard.Steps
{
	/// <summary>
	/// Interaction logic for ContentProviderStep.xaml
	/// </summary>
	public partial class ContentProviderStep : UserControl
	{
		public ContentProviderStep()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Event, pro focus na input při kliknutí na label.
		/// </summary>
		private void FocusInput(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount == 1) //Note that this is a lie, this does not check for a "real" click
			{
				var label = (Label)sender;
				Keyboard.Focus(label.Target);
			}
		}
	}
}
