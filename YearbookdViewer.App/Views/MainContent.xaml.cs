﻿using System.Windows.Input;
using System.Windows.Controls;
using YearbookViewer.App.Code;
using YearbookViewer.App.ViewModels;
using System.Windows.Controls.Primitives;

namespace YearbookViewer.App.Views
{
	/// <summary>
	/// Interaction logic for MainContent.xaml
	/// </summary>
	public partial class MainContent : UserControl
	{
		public MainContent()
		{
			InitializeComponent();

			// Nastavím parametry commandů, chci je mít hezky typované.
			this.MoveBackWard.CommandParameter = true;
			this.MoveForward.CommandParameter = false;

			this.Increase.CommandParameter = true;
			this.Decrease.CommandParameter = false;
		}

		/// <summary>
		/// Event, pro focus na input při kliknutí na label.
		/// </summary>
		private void FocusInput(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount == 1) //Note that this is a lie, this does not check for a "real" click
			{
				var label = (Label)sender;
				Keyboard.Focus(label.Target);
			}
		}

		/// <summary>
		/// Zvýrozňování v textu dělám jenom na úrovní GUI, s právě zobrazených textem se stejně nepracuje.
		/// </summary>
		private void HighlighInText(object sender, TextChangedEventArgs e)
		{
			var textBox = (TextBox)sender;
			HighlighInText(textBox.Text);
		}

		/// <summary>
		/// Zvýraznění stringu v rich text boxu.
		/// </summary>
		/// <param name="search">Výraz, který chci zvýraznit.</param>
		public void HighlighInText(string search)
		{
			HighLighter.HighlightSelection(ImageTextBox, search ?? string.Empty, ((MainContentViewModel)this.DataContext).Filters);
		}

		/// <summary>
		/// Přiblížení kliknutím na ovládací prvek.
		/// </summary>
		private void Zoom(object sender, System.Windows.RoutedEventArgs e)
		{
			ImageContainer.Zoom(
				ImageContainer.DesiredSize.Width / 2,
				ImageContainer.DesiredSize.Height / 2,
				(bool)((Button)sender).CommandParameter // Celkem cool přetypování
			);
		}

		/// <summary>
		/// Reser veškerého přibližování.
		/// </summary>
		private void ResetZoom(object sender, System.Windows.RoutedEventArgs e)
		{
			ImageContainer.Reset();
		}

		/// <summary>
		/// Event, který se zavolá na ukončení dragu spliteru -> vlastně pak uloží aktuální rozložení.
		/// Je to méně náročné než když se ukládá pokaždé v setteru dané property :/.
		/// </summary>
		private void GridSplitter_DragCompleted(object sender, DragCompletedEventArgs e)
		{
			var model = (MainContentViewModel)DataContext;

			if (model.SaveLayout.CanExecute(null))
				model.SaveLayout.Execute(null);
		}
	}
}
