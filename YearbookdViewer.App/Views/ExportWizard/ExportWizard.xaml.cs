﻿using System.ComponentModel;
using System.Windows;
using YearbookViewer.App.ViewModels.ExportWizard;

namespace YearbookViewer.App.Views.ExportWizard
{
	/// <summary>
	/// Interaction logic for ExportWizard.xaml
	/// </summary>
	public partial class ExportWizard : Window
	{
		private readonly ExportWizardViewModel context;

		public ExportWizard(ExportWizardViewModel dataContext)
		{
			InitializeComponent();

			// Nastavím datové kontexty.
			this.context = dataContext;
			this.DataContext = dataContext;
		}

		/// <summary>
		/// Zavře okno s import wizardem.
		/// </summary>
		private void CloseWindow(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		/// <summary>
		/// Akce volaná před zavřením okna.
		/// </summary>
		protected override void OnClosing(CancelEventArgs e)
		{
			// Nedovolím zavřít pokud probíhá export.
			e.Cancel = this.context.ProgressBar.Visible;

			base.OnClosing(e);
		}
	}
}
