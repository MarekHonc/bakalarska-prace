﻿using System.Windows.Controls;
using System.Windows.Input;

namespace YearbookViewer.App.Views
{
	/// <summary>
	/// Interaction logic for SearchFilters.xaml
	/// </summary>
	public partial class SearchFilters : UserControl
	{
		public SearchFilters()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Event, pro focus na input při kliknutí na label.
		/// </summary>
		private void FocusInput(object sender, MouseButtonEventArgs e)
		{
			if (e.ClickCount == 1) //Note that this is a lie, this does not check for a "real" click
			{
				var label = (Label)sender;
				Keyboard.Focus(label.Target);
			}
		}
	}
}
