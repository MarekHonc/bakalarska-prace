﻿using System.Windows.Input;
using YearbookViewer.App.Code.Commands;

namespace YearbookViewer.App.ViewModels.Base
{
	/// <summary>
	/// Bázový view model pro modely, které se mají zobrazovat jako popupy.
	/// </summary>
	public class PopupViewModelBase : ViewModelBase
	{
		#region Private variables

		private bool isPopupOpened;

		#endregion

		#region Commands

		public ICommand Open { get; private set; }

		public ICommand Close { get; private set; }

		#endregion

		public PopupViewModelBase()
		{
			this.Open = new Command(() => this.IsPopupOpened = true);
			this.Close = new Command(() => this.IsPopupOpened = false);
		}

		/// <summary>
		/// Vrací nebo nastavuje, zda-li je otevřen popup s nastavení.
		/// </summary>
		public bool IsPopupOpened
		{
			get
			{
				return this.isPopupOpened;
			}
			set
			{
				if (value == this.isPopupOpened)
					return;

				this.isPopupOpened = value;
				RaisePropertyChanged();
			}
		}
	}
}
