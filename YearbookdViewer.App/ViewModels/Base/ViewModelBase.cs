﻿using System.Runtime.CompilerServices;
using YearbookViewer.App.Code.Interfaces;
using System.ComponentModel;
using System.Collections.Generic;
using YearbookViewer.App.Code;
using System.Reflection;
using System.Linq;
using System;
using System.Windows.Threading;

namespace YearbookViewer.App.ViewModels.Base
{
	/// <summary>
	/// Bátový view model, společný pro všechny view modely.
	/// </summary>
	public class ViewModelBase : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private Dictionary<string, List<string>> dependencies;
		private Dictionary<string, List<string>> modelDependencies;

		/// <summary>
		/// Vyvolá oznámení o změně (této) property.
		/// </summary>
		/// <param name="propertyName">Název property, která se změnila. Nemusí se předávat, pokud je voláno ze setteru property.</param>
		protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = "")
		{
			// Zjistím si, co vše je na této property závistlé.
			BuildDependenciesIfNecessary();

			// Dám vědět že se hodnota změnila.
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

			// Pro všechny property, které jsou závislé, tak také řeknu že se změnily.
			if (this.dependencies.TryGetValue(propertyName, out List<string> toNotify))
			{
				toNotify.ForEach(prop => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop)));
			}
		}

		/// <summary>
		/// Vytvoří strom s proměnnými, kde:
		/// proměnná => [{proměnné}, {na}, {ní}, {závislé}]
		/// </summary>
		private void BuildDependenciesIfNecessary()
		{
			// Už nejaké mám -> nemusím nic dělat.
			if (this.dependencies != null)
				return;

			this.dependencies = new Dictionary<string, List<string>>();

			// Vytáhnu všechny property.
			var properties = GetType().GetProperties()
				.Where(prop => prop.IsDefined(typeof(DependsUponAttribute), false))
				.Select(prop => new
				{
					Name = prop.Name,
					DependsOn = ((DependsUponAttribute)prop.GetCustomAttribute(typeof(DependsUponAttribute))).Properties
				}).ToArray();

			// Pro každou property, ji přidám do kolekce se závislostmi.
			foreach (var property in properties)
			{
				foreach (var leadingProperty in property.DependsOn)
				{
					if (!this.dependencies.TryGetValue(leadingProperty, out var list))
					{
						list = new List<string>();
						this.dependencies[leadingProperty] = list;
					}

					list.Add(property.Name);
				}
			}
		}

		/// <summary>
		/// Vytvoří strom se vztahy mezi modely modely kde:
		/// model => [{proměnné}, {které}, {když se změní tam se model aktualizuje}]
		/// </summary>
		private void BuildPropertyChangedModelDependanciesIfNecessary()
		{
			if (modelDependencies != null)
				return;

			this.modelDependencies = new Dictionary<string, List<string>>();

			// Vytáhnu všechny property.
			var models = GetType().GetProperties()
				.Where(prop => prop.IsDefined(typeof(UpdateAnotherModelAttribute), false))
				.Select(prop => new
				{
					prop.Name,
					((UpdateAnotherModelAttribute)prop.GetCustomAttribute(typeof(UpdateAnotherModelAttribute))).ModelToUpdate
				}).ToArray();

			// Pro každý model přidám do kolekce všechny property na které mám reagovat.
			foreach (var model in models)
			{
				if (!this.modelDependencies.TryGetValue(model.ModelToUpdate, out var list))
				{
					list = new List<string>();
					this.modelDependencies[model.ModelToUpdate] = list;
				}
				list.Add(model.Name);
			}
		}

		/// <summary>
		/// Vrací jestli by měl být model updatován, preferované je volání uvnirř eventu property changed.
		/// </summary>
		/// <param name="modelName">Název modelu, který se chce updatovat.</param>
		/// <param name="property">Název property, která způsobila volání property changed.</param>
		public bool ShouldBeUpdated(string modelName, string property)
		{
			BuildPropertyChangedModelDependanciesIfNecessary();

			if (this.modelDependencies.TryGetValue(modelName, out var leadingProperties))
			{
				// Mám interface, který bere ohled na možnost zastavení propagace, tak koukám i na hodnotu z něj.
				if (this is IStopableUpdatePropagation model)
				{
					return leadingProperties.Contains(property) && !model.StopUpdatePropagation;
				}

				return leadingProperties.Contains(property);
			}

			return false;
		}

		/// <summary>
		/// Provede danou akci se zpožděním.
		/// "Řekne dané akci - hele proveď se za 5 minut".
		/// </summary>
		/// <param name="action">Akce, která se provede.</param>
		/// <param name="delay">Časový delay, po kterém se akce provede.</param>
		protected void ExecuteWithDelay(Action action, TimeSpan delay)
		{
			// Vyvtořím si časovač.
			var timer = new DispatcherTimer
			{
				Interval = delay,
				Tag = action
			};

			// Na skončení intervalu -> zastavím a provedu akci.
			timer.Tick += new EventHandler((sender, e) => 
			{
				var t = (DispatcherTimer)sender;
				var a = (Action)timer.Tag;

				a.Invoke();
				t.Stop();
			});

			// začnu odpočítávat.
			timer.Start();
		}
	}
}
