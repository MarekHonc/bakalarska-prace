﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.App.ViewModels.ImportWizard.Steps;

namespace YearbookViewer.App.ViewModels.ImportWizard
{
	/// <summary>
	/// View model, starající se o importování dat do aplikace.
	/// </summary>
	public class ImportWizardViewModel : ViewModelBase
	{
		private BaseStep currentPageModel;
		private bool controlsEnabled;
		private readonly SearchFilterViewModel searchFilterViewModel;
		private readonly IUserSettingsProvider settings;

		#region modely pro import

		private static readonly BaseStep welcomeStep = new WelcomeStep();
		private static readonly BaseStep contentProviderStepModel = new ContentProviderStep();
		private static readonly BaseStep dataSelectStep = new DataSelectStep();
		private static readonly BaseStep firstFilterStep = new FiltersStep((DataSelectStep)dataSelectStep);
		private static readonly BaseStep advancedFiltersStep = new AdvancedFiltersStep((DataSelectStep)dataSelectStep);
		private static readonly BaseStep confirmStep = new ConfirmStep((DataSelectStep)dataSelectStep);
		private static readonly BaseStep finishStep = new FinishStep();

		/// <summary>
		/// Slovík se všemi stránkami, které se v importu nacházejí.
		/// </summary>
		private static Dictionary<WizardPage, BaseStep> Steps => new Dictionary<WizardPage, BaseStep>()
		{
			{ WizardPage.Welcome, welcomeStep },
			{ WizardPage.ContentProvider, contentProviderStepModel },
			{ WizardPage.DataSelect, dataSelectStep },
			{ WizardPage.Filters, firstFilterStep },
			{ WizardPage.AdvancedFilters, advancedFiltersStep },
			{ WizardPage.Confirm, confirmStep },
			{ WizardPage.Finish, finishStep },
		};

		#endregion

		#region Commandy

		public ICommand NextPage { get; private set; }

		public ICommand PrevPage { get; private set; }

		public IAsyncCommand DoImport { get; private set; }

		#endregion

		/// <summary>
		/// Vytvoří novou instanci import wizardu.
		/// </summary>
		/// <param name="searchFilterViewModel">Model s filtry aplikace.</param>
		/// <param name="settings">Nastavení aplikace.</param>
		public ImportWizardViewModel(SearchFilterViewModel searchFilterViewModel, IUserSettingsProvider settings)
		{
			// Začínám welcome stránkou.
			this.CurrentPageModel = Steps[WizardPage.Welcome];

			// Orientace mezi stránkami.
			this.NextPage = new Command(() => this.NextPageAction());
			this.PrevPage = new Command(() => this.PrevPageAction());
			this.DoImport = new AsyncCommand(this.DoImportAction);

			// A povolím procházení wizardu.
			this.ControlsEnabled = true;

			// Ještě si uložím hlavní model aplikace, abych ho mohl případně ovlivnit.
			this.searchFilterViewModel = searchFilterViewModel;

			// A potřebuji i nastavení.
			this.settings = settings;
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuální stránku, na které se wizard nachází.
		/// </summary>
		public BaseStep CurrentPageModel
		{
			get
			{
				return this.currentPageModel;
			}
			set
			{
				this.currentPageModel = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, zda-li se nacházím na první stránce importu.
		/// </summary>
		[DependsUpon(nameof(CurrentPageModel))]
		public bool PrevPageVisible => this.CurrentPageModel is IWithPrevPage;

		/// <summary>
		/// Vrací, zda-li se nacházím na poslední stránce importu.
		/// </summary>
		[DependsUpon(nameof(CurrentPageModel))]
		public bool NextPageVisible => this.CurrentPageModel is IWithNextPage && !this.DoImportVisible;

		/// <summary>
		/// Vrací, zda-li má být viditelné tlačítko pro provedení imprtu.
		/// </summary>
		[DependsUpon(nameof(CurrentPageModel))]
		public bool DoImportVisible => this.CurrentPageModel is ConfirmStep;

		/// <summary>
		/// Vrací, zda-li se nacházím na poslední stránce importu.
		/// Poslední stránka je speciální stránka, která nemá vazbu na předchozí ani následující.
		/// </summary>
		[DependsUpon(nameof(CurrentPageModel))]
		public bool IsLastPage => !this.PrevPageVisible && !this.NextPageVisible;

		/// <summary>
		/// Vrací, nebo nastavuje, zda-li jsou tlačítka importu povolená.
		/// </summary>
		public bool ControlsEnabled
		{
			get
			{
				return this.controlsEnabled;
			}
			set
			{
				if (value == this.controlsEnabled)
					return;

				this.controlsEnabled = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Event volaný po úspěšném provedení importu.
		/// </summary>
		public static event EventHandler OnImportCompleted;

		#region Steps

		public WelcomeStep WelcomeStep => (WelcomeStep)Steps[WizardPage.Welcome];

		public ContentProviderStep ContentProviderStep => (ContentProviderStep)Steps[WizardPage.ContentProvider];

		public DataSelectStep DataSelectStep => (DataSelectStep)Steps[WizardPage.DataSelect];

		public FiltersStep FiltersStep => (FiltersStep)Steps[WizardPage.Filters];

		public AdvancedFiltersStep AdvancedFiltersStep => (AdvancedFiltersStep)Steps[WizardPage.AdvancedFilters];

		public ConfirmStep ConfirmStep => (ConfirmStep)Steps[WizardPage.Confirm];

		public FinishStep FinishStep => (FinishStep)Steps[WizardPage.Finish];

		#endregion

		/// <summary>
		/// Akce, která se provede při klepnutí na tlačítko další.
		/// </summary>
		private void NextPageAction()
		{
			if (this.CurrentPageModel.IsValid())
			{
				var model = (IWithNextPage)this.CurrentPageModel;
				this.CurrentPageModel = Steps[model.NextPage];
				this.CurrentPageModel.ValidationMessages.Clear();
			}
		}

		/// <summary>
		/// Akce, která se provede při klepnutí na tlačítko předchozí.
		/// </summary>
		private void PrevPageAction()
		{
			var model = (IWithPrevPage)this.CurrentPageModel;
			this.CurrentPageModel = Steps[model.PrevPage];
			this.CurrentPageModel.ValidationMessages.Clear();
		}

		/// <summary>
		/// Akce, která provede import.
		/// </summary>
		private async Task DoImportAction()
		{
			// Tlačítka vypnu na UI vlákně.
			AppUtils.RunOnUiThread(() =>
			{
				this.ControlsEnabled = false;
				this.ConfirmStep.ProgressBar.Visible = true;
				this.ConfirmStep.ProgressBar.MaxValue = 
					this.DataSelectStep.UseAdvancedFilters
						? this.AdvancedFiltersStep.FileCount
						: this.DataSelectStep.FileCount;
			});
			
			// Provedu import.
			var importer = new DataImporter(this);
			var applicationMode = await importer.DoImport(done =>
			{
				AppUtils.RunOnUiThread(() => this.ConfirmStep.ProgressBar.Value = done);
			});

			// Tlačítka zapnu na UI vlákně a přepnu stránku.
			AppUtils.RunOnUiThread(() =>
			{
				// Povolím ovládací prvky importu.
				this.ControlsEnabled = true;
				NextPageAction();

				// Resetuji progress bar.
				this.ConfirmStep.ProgressBar.Reset();

				// Uložím nastavení z importu do aplikace.
				this.settings.SaveAfterImport(applicationMode, this.ContentProviderStep.SelectedDataSource, this.ContentProviderStep.ServerAddress);

				// Nastavím mód aplikace.
				this.searchFilterViewModel.ApplyModeToFilters(applicationMode);
				this.searchFilterViewModel.ClearFiltersAction(clearTopLevelFilters: true);

				// Pokud mám event který mohu zavolat.
				if (OnImportCompleted != null)
				{
					OnImportCompleted(this, EventArgs.Empty);
				}
			});
		}
	}
}
