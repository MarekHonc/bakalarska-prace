﻿using System;
using System.IO;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.Localization;
using YearbookViewer.Common;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Třetí krok importu - obsahuje výběr dat, které se budou importovat.
	/// </summary>
	public class DataSelectStep : BaseStep, IWithNextPage, IWithPrevPage
	{
		#region private variables

		private Tuple<string, int> pathsCache;
		private Tuple<string, int> fileCountCache;
		private string filePath;
		private bool setupFirstFilter;
		private bool setupSecondFilter;
		private bool setupGroupFilter;
		private bool useAdvancedFilters;

		#endregion

		/// <summary>
		/// Vrací nebo nastavuje cestu, ze které se budou importovat data.
		/// </summary>
		public string FilePath
		{
			get
			{
				return this.filePath;
			}
			set
			{
				this.filePath = value;

				this.SetupGroupFilter = false;
				this.SetupFirstFilter = false;
				this.SetupSecondFilter = false;
				this.UseAdvancedFilters = false;

				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, kolik vnořených podložek se nachází ve složce <see cref="FilePath"/>.
		/// </summary>
		[DependsUpon(nameof(FilePath))]
		public int DirectoriesDepth
		{
			get
			{
				// Pokud mám již uloženo
				if (this.pathsCache != null && this.pathsCache.Item1 == this.FilePath)
					return this.pathsCache.Item2;

				// Cestu uloženou ještě nemám, zjistím hloubku adresáře.
				// Pokud cestu nemám, vracím -1.
				if (this.FilePath.IsNullOrEmpty())
					return -1;

				// Uložím, abych nemusel dokola procházet.
				this.pathsCache = Tuple.Create(this.FilePath,
					Common.FileBrowser.GetNumberOfNestedDirectories(this.FilePath));

				return this.pathsCache.Item2;
			}
		}

		/// <summary>
		/// Vrací kolik je souborů v dané složce <see cref="FilePath"/>.
		/// </summary>
		[DependsUpon(nameof(FilePath))]
		public int FileCount
		{
			get
			{
				// Pokud mám již uloženo
				if (this.fileCountCache != null && this.fileCountCache.Item1 == this.FilePath)
					return this.fileCountCache.Item2;

				// Cestu uloženou ještě nemám, zjistím počet souborů.
				// Pokud cestu nemám, vracím -1.
				if (this.FilePath.IsNullOrEmpty())
					return -1;

				// Uložím do cache.
				this.fileCountCache = Tuple.Create(this.FilePath, FileBrowser.GetNumberOfFiles(this.FilePath));

				return this.fileCountCache.Item2;
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje, zda-li si uživatel přeje nastavit první filtr.
		/// </summary>
		public bool SetupFirstFilter
		{
			get
			{
				return this.setupFirstFilter;
			}
			set
			{
				if (value == this.setupFirstFilter)
					return;

				// Pokud nastavuji na true -> nuluji že mám používat advenced filtr.
				if(value)
					this.UseAdvancedFilters = false;

				this.setupFirstFilter = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje, zda-li si uživatel přeje nastavit druhý filtr.
		/// </summary>
		public bool SetupSecondFilter
		{
			get
			{
				return this.setupSecondFilter;
			}
			set
			{
				if (value == this.setupSecondFilter)
					return;

				// Pokud nastavuji na true -> nuluji že mám používat advenced filtr.
				if (value)
					this.UseAdvancedFilters = false;

				this.setupSecondFilter = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje, zda-li si uživatel přeje nastavit groupovací filter.
		/// </summary>
		public bool SetupGroupFilter
		{
			get
			{
				return this.setupGroupFilter;
			}
			set
			{
				if (value == this.setupGroupFilter)
					return;

				// Pokud nastavuji na true -> nuluji že mám používat advenced filtr.
				if (value)
					this.UseAdvancedFilters = false;

				this.setupGroupFilter = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, zda-li je nastaven jakýkoliv ne-custom filtr.
		/// </summary>
		[DependsUpon(nameof(SetupFirstFilter), nameof(SetupSecondFilter), nameof(SetupGroupFilter))]
		public bool UseAnyStandardFilter => this.SetupFirstFilter || this.SetupSecondFilter || this.SetupGroupFilter;

		/// <summary>
		/// Vrací nebo nastavuje, zda-li se mají používat vlastní filtry.
		/// </summary>
		public bool UseAdvancedFilters
		{
			get
			{
				return this.useAdvancedFilters;
			}
			set
			{
				if (value == this.useAdvancedFilters)
					return;

				// Pokud nastavuji na true -> nuluji že mám používat advenced filtr.
				if (value)
				{
					this.SetupFirstFilter = false;
					this.SetupSecondFilter = false;
					this.SetupGroupFilter = false;
				}

				this.useAdvancedFilters = value;
				RaisePropertyChanged();
			}
		}

		#region Page navigation

		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.DataSelect;

		/// <inheritdoc cref="IWithNextPage.NextPage"/>
		public WizardPage NextPage
		{
			get
			{
				if (this.UseAdvancedFilters)
					return WizardPage.AdvancedFilters;

				if (this.SetupFirstFilter || this.SetupSecondFilter || this.SetupGroupFilter)
					return WizardPage.Filters;

				return WizardPage.Confirm;
			}
		}

		/// <inheritdoc cref="IWithPrevPage.PrevPage"/>
		public WizardPage PrevPage => WizardPage.ContentProvider;

		#endregion

		/// <inheritdoc cref="BaseStep.IsValid"/>
		public override bool IsValid()
		{
			base.IsValid();

			if (this.FilePath.IsNullOrEmpty() || !Directory.Exists(this.FilePath))
			{
				this.ValidationMessages.Add(ImportWizardResources.DataSelectStep_InvalidFilePath);
				return false;
			}

			return true;
		}
	}
}
