﻿using System.Collections.ObjectModel;
using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.ViewModels.Base;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Základní model pro stránku wizardu.
	/// </summary>
	public abstract class BaseStep : ViewModelBase
	{
		private ObservableCollection<string> validationMessages;

		protected BaseStep()
		{
			this.ValidationMessages = new ObservableCollection<string>();

			if (this.UseLiveValidation)
			{
				this.PropertyChanged += ((sender, args) => this.IsValid());
			}
		}

		/// <summary>
		/// Vrací stránku, pro kterou je tento model.
		/// </summary>
		public abstract WizardPage CurrentPage
		{
			get;
		}

		/// <summary>
		/// Vrací, zda-li se má stránka validovat při změně property.
		/// </summary>
		public virtual bool UseLiveValidation => true;

		/// <summary>
		/// Vrací, jestli jsou aktuální data v modelu validní.
		/// </summary>
		public virtual bool IsValid()
		{
			this.ValidationMessages.Clear();

			return true;
		}

		/// <summary>
		/// Vrací kolekci se všemi validačními errory.
		/// </summary>
		public ObservableCollection<string> ValidationMessages
		{
			get
			{
				return this.validationMessages;
			}
			set
			{
				this.validationMessages = value;
				RaisePropertyChanged();
			}
		}
	}
}
