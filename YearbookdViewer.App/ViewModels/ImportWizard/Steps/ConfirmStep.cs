﻿using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.Models;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Šestý krok importu - pouze potvrzení importu.
	/// </summary>
	public class ConfirmStep : BaseStep, IWithNextPage, IWithPrevPage
	{
		private readonly DataSelectStep dataSelect;
		private ProgressBarModel progressBar;

		public ConfirmStep(DataSelectStep dataSelect)
		{
			this.dataSelect = dataSelect;
			this.ProgressBar = new ProgressBarModel();
		}

		#region Page navigation

		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.Confirm;

		/// <inheritdoc cref="IWithNextPage.NextPage"/>
		public WizardPage NextPage => WizardPage.Finish;

		/// <inheritdoc cref="IWithPrevPage.PrevPage"/>
		public WizardPage PrevPage
		{
			get
			{
				if (this.dataSelect.UseAdvancedFilters)
					return WizardPage.AdvancedFilters;

				if (this.dataSelect.SetupFirstFilter || this.dataSelect.SetupSecondFilter || this.dataSelect.SetupGroupFilter)
					return WizardPage.Filters;

				return WizardPage.DataSelect;
			}
		}

		#endregion

		/// <summary>
		/// Vrací nebo nastavuje progress bar importu dat.
		/// </summary>
		public ProgressBarModel ProgressBar
		{
			get
			{
				return this.progressBar;
			}
			set
			{
				this.progressBar = value;
				RaisePropertyChanged();
			}
		}
	}
}