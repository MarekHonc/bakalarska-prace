﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Microsoft.Win32;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.Localization;
using YearbookViewer.App.Models.Wizard.Comboboxes;
using YearbookViewer.Common.Enums;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Čtvrtý krok importu - obsahuje nastavení prvního filtru.
	/// </summary>
	public class FiltersStep : BaseStep, IWithNextPage, IWithPrevPage
	{
		private AvailableFilterComboboxValueModel selectedFirstFilter;
		private AvailableFilterComboboxValueModel selectedSecondFilter;
		private AvailableFilterComboboxValueModel selectedGroupFilter;

		public FiltersStep(DataSelectStep dataSelect)
		{
			this.DataSelectStep = dataSelect;
			this.LoadGroups = new Command(LoadFileAction);
		}

		/// <summary>
		/// Vrací kolekci všech položek, které lze použit v prvním filtru.
		/// </summary>
		private AvailableFilters[] firstFilter =
			((AvailableFilters[])Enum.GetValues(typeof(AvailableFilters)))
				.Where(f => f.GetFilterTypes().Contains(FilterType.Filter))
				.ToArray();

		/// <summary>
		/// Vrací kolekci všech položek, které lze použít ve druhém filtru.
		/// </summary>
		private AvailableFilters[] secondFilter =
			((AvailableFilters[])Enum.GetValues(typeof(AvailableFilters)))
				.Where(f => f.GetFilterTypes().Contains(FilterType.Group))
				.ToArray();

		#region page navigation

		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.Filters;

		/// <inheritdoc cref="IWithNextPage.NextPage"/>
		public WizardPage NextPage => WizardPage.Confirm;

		/// <inheritdoc cref="IWithPrevPage.PrevPage"/>
		public WizardPage PrevPage => WizardPage.DataSelect;

		#endregion

		/// <summary>
		/// Vrací komand pro nahrání souboru.
		/// </summary>
		public ICommand LoadGroups
		{
			get;
			private set;
		}

		/// <summary>
		/// Na této stránce je živá validace vypnutá.
		/// </summary>
		public override bool UseLiveValidation => false;

		/// <summary>
		/// Vrací nebo nastavuje krok, kde se vybírají data.
		/// </summary>
		public DataSelectStep DataSelectStep { get; }

		/// <summary>
		/// Vrací nebo nastavuje vybraný první filtr.
		/// </summary>
		public AvailableFilterComboboxValueModel SelectedFirstFilter
		{
			get
			{
				return this.selectedFirstFilter;
			}
			set
			{
				this.selectedFirstFilter = value;
				RaisePropertyChanged();
				RaisePropertyChanged(nameof(SelectedFirstFilter.FilterValue));
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje vybraný sekundární filtr.
		/// </summary>
		public AvailableFilterComboboxValueModel SelectedSecondFilter
		{
			get
			{
				return this.selectedSecondFilter;
			}
			set
			{
				this.selectedSecondFilter = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací položky comboboxu pro první filter.
		/// </summary>
		public ObservableCollection<AvailableFilterComboboxValueModel> Filters
		{
			get
			{
				var values = firstFilter
					.Select((filter, index) => new AvailableFilterComboboxValueModel(filter, index));

				return values.ToObservableCollection();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje vybraný filter pro groupování.
		/// </summary>
		public AvailableFilterComboboxValueModel SelectedGroupFilter
		{
			get
			{
				return this.selectedGroupFilter;
			}
			set
			{
				this.selectedGroupFilter = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací položky comboboxu pro groupovací filter.
		/// </summary>
		public ObservableCollection<AvailableFilterComboboxValueModel> GroupFilters
		{
			get
			{
				var values = secondFilter
					.Select((filter, index) => new AvailableFilterComboboxValueModel(filter, index));

				return values.ToObservableCollection();
			}
		}

		/// <summary>
		/// Vrací, zda-li jsou aktuální hodnotu platné.
		/// </summary>
		/// <returns></returns>
		public override bool IsValid()
		{
			base.IsValid();

			if (this.DataSelectStep.SetupFirstFilter)
			{
				if (this.SelectedFirstFilter == null || this.SelectedFirstFilter?.FilterValue.IsNullOrEmpty() == true)
				{
					this.ValidationMessages.Add(ImportWizardResources.FiltersStep_FiltersRequiered);
					return false;
				}
			}

			if (this.DataSelectStep.SetupSecondFilter)
			{
				if (this.SelectedSecondFilter == null || this.SelectedSecondFilter?.FilterValue.IsNullOrEmpty() == true)
				{
					this.ValidationMessages.Add(ImportWizardResources.FiltersStep_FiltersRequiered);
					return false;
				}
			}

			if (this.DataSelectStep.SetupGroupFilter)
			{
				if (this.SelectedGroupFilter == null || this.SelectedGroupFilter?.FilterValue.IsNullOrEmpty() == true)
				{
					this.ValidationMessages.Add(ImportWizardResources.FiltersStep_FiltersRequiered);
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Akce pro nahrání souboru.
		/// </summary>
		private void LoadFileAction()
		{
			var dialog = new OpenFileDialog();

			dialog.Filter = string.Format(ImportWizardResources.FiltersStep_GroupFileType, ".xlsx");

			if (dialog.ShowDialog() == true)
				this.SelectedGroupFilter.FilterValue = dialog.FileName;
		}
	}
}