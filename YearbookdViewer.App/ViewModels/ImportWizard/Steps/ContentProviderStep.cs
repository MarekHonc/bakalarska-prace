﻿using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.Localization;
using YearbookViewer.Elastic;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Druhý krok importu - obsahuje výběr, jak si uživatel přede vyhledávat v datech.
	/// </summary>
	public class ContentProviderStep : BaseStep, IWithNextPage, IWithPrevPage
	{
		#region Page navigation

		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.ContentProvider;

		/// <inheritdoc cref="IWithNextPage.NextPage"/>
		public WizardPage NextPage => WizardPage.DataSelect;

		/// <inheritdoc cref="IWithPrevPage.PrevPage"/>
		public WizardPage PrevPage => WizardPage.Welcome;

		#endregion

		private DataSourceType selectedDataSource;
		private string serverAddress;

		public ContentProviderStep()
		{
			this.SelectedDataSource = DataSourceType.FileBrowser;
			this.ServerAddress = ElasticContext.ElasticServerAddress;
		}

		/// <summary>
		/// Vrací nebo nastavuje vybraný provider dat.
		/// </summary>
		public DataSourceType SelectedDataSource
		{
			get { return this.selectedDataSource; }
			set
			{
				this.selectedDataSource = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje adresu serveru, ze kterého se data získávají.
		/// </summary>
		public string ServerAddress
		{
			get { return this.serverAddress; }
			set
			{
				this.serverAddress = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, jestli se má zobrazit input pro zadání adresy serveru.
		/// </summary>
		[DependsUpon(nameof(SelectedDataSource))]
		public bool ShouldInputServerAddress => this.SelectedDataSource == DataSourceType.ElasticSearch;

		/// <inheritdoc cref="BaseStep.IsValid"/>
		public override bool IsValid()
		{
			base.IsValid();

			// Pokud mám mít připojení na server, zkontroluji ho.
			if (this.ShouldInputServerAddress && !ElasticContext.IsConnectionValid(this.ServerAddress))
			{
				this.ValidationMessages.Add(string.Format(ImportWizardResources.ContentProviderStep_ServerInvalid, this.ServerAddress));
				return false;
			}

			return true;
		}
	}
}
