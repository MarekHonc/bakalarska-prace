﻿using YearbookViewer.App.Code.ImportWizard;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// První krok importu - obsahuje pouze text.
	/// </summary>
	public class WelcomeStep : BaseStep, IWithNextPage
	{
		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.Welcome;

		/// <inheritdoc cref="IWithNextPage.NextPage"/>
		public WizardPage NextPage => WizardPage.ContentProvider;
	}
}
