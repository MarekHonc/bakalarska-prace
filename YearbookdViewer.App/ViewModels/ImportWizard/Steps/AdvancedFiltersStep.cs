﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Code.ImportWizard;
using YearbookViewer.App.Localization;
using YearbookViewer.Common;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Krok importu, kde si uživatel přesně definuje co chce za data.
	/// </summary>
	public class AdvancedFiltersStep : BaseStep, IWithNextPage, IWithPrevPage
	{
		#region Cover variables

		private ObservableCollection<string> topLevelDirectories;
		private string newTopLevelDirectory;
		private string selectedTopLevelDirectory;

		private string newSecondLevelDirectory;
		private string selectedSecondLevelDirectory;

		#endregion

		#region Comandy

		public ICommand AddToTopLevelCollection
		{
			get;
			private set;
		}

		public ICommand AddToSecondLevelCollection
		{
			get;
			private set;
		}

		public ICommand RemoveFromTopLevelCollection
		{
			get;
			private set;
		}

		public ICommand RemoveFromSecondLevelCollection
		{
			get;
			private set;
		}

		public ICommand AddToFilesCollection
		{
			get;
			private set;
		}

		public ICommand RemoveFromFilesCollection
		{
			get;
			private set;
		}

		#endregion

		/// <summary>
		/// Slovník, pro uložení všech složek druhého stupně v závislosti na prvním.
		/// </summary>
		private readonly Dictionary<string, ObservableCollection<string>> secondLevelDirectories;

		/// <summary>
		/// Slovník, pro uložení všech souborů pro import.
		/// </summary>
		private readonly Dictionary<string, Dictionary<string, ObservableCollection<Tuple<string, string>>>> filesToImport;

		/// <summary>
		/// Model, obsahující předchozí krok importu.
		/// </summary>
		private readonly DataSelectStep dataSelectStep;

		public AdvancedFiltersStep(DataSelectStep dataSelectStep)
		{
			this.dataSelectStep = dataSelectStep;

			this.TopLevelDirectories = new ObservableCollection<string>();
			this.secondLevelDirectories = new Dictionary<string, ObservableCollection<string>>();
			this.filesToImport = new Dictionary<string, Dictionary<string, ObservableCollection<Tuple<string, string>>>>();

			this.InitCommands();
		}

		/// <summary>
		/// Vrací nebo nastavuje list hlavních složek.
		/// </summary>
		public ObservableCollection<string> TopLevelDirectories
		{
			get
			{
				return this.topLevelDirectories;
			}
			set
			{
				this.topLevelDirectories = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje název nově vytvořeného filtru.
		/// </summary>
		public string NewTopLevelDirectory
		{
			get
			{
				return this.newTopLevelDirectory;
			}
			set
			{
				this.newTopLevelDirectory = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuálně vybraný první filtr.
		/// </summary>
		public string SelectedTopLevelDirectory
		{
			get
			{
				return this.selectedTopLevelDirectory;
			}
			set
			{
				this.selectedTopLevelDirectory = value;

				if (value.IsNotNullOrEmpty() && !this.secondLevelDirectories.ContainsKey(value))
				{
					this.secondLevelDirectories[value] = new ObservableCollection<string>();
				}

				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje list sekundárních filtrů, které se vztahují k hlavnímu.
		/// </summary>
		[DependsUpon(nameof(SelectedTopLevelDirectory))]
		public ObservableCollection<string> SecondLevelDirectories
		{
			get
			{
				// Nic vybráno není -> vracím null.
				if (this.SelectedTopLevelDirectory.IsNullOrEmpty())
					return null;

				// Pokud mám ve slovníku kolekci, do které přidávám.
				if (this.secondLevelDirectories.TryGetValue(this.SelectedTopLevelDirectory, out var value))
					return value;

				return null;
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje nově vytvořený sekundární filtr.
		/// </summary>
		public string NewSecondLevelDirectory
		{
			get
			{
				return this.newSecondLevelDirectory;
			}
			set
			{
				this.newSecondLevelDirectory = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuálně vybraný druhý filtr.
		/// </summary>
		public string SelectedSecondLevelDirectory
		{
			get
			{
				return this.selectedSecondLevelDirectory;
			}
			set
			{
				this.selectedSecondLevelDirectory = value;

				if (value.IsNotNullOrEmpty())
				{
					if (!this.filesToImport.ContainsKey(this.SelectedTopLevelDirectory))
					{
						this.filesToImport[this.SelectedTopLevelDirectory] = new Dictionary<string, ObservableCollection<Tuple<string, string>>>();
					}

					if (!this.filesToImport[this.SelectedTopLevelDirectory].ContainsKey(value))
					{
						this.filesToImport[this.SelectedTopLevelDirectory][value] = new ObservableCollection<Tuple<string, string>>();
					}
				}

				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací seznam všech souborů k importu v rámci rozražení filtru.
		/// </summary>
		[DependsUpon(nameof(SelectedSecondLevelDirectory))]
		public ObservableCollection<Tuple<string, string>> ImportFiles
		{
			get
			{
				// Nic vybráno není -> vracím null.
				if (this.SelectedSecondLevelDirectory.IsNullOrEmpty())
					return null;

				// Pokud mám ve slovníku kolekci, do které přidávám.
				if (this.filesToImport.TryGetValue(this.SelectedTopLevelDirectory, out var first))
				{
					if (first.TryGetValue(this.SelectedSecondLevelDirectory, out var value))
					{
						return value;
					}
				}

				return null;
			}
		}

		/// <summary>
		/// Vrací všechny položky, které se mají importovat.
		/// </summary>
		public Dictionary<string, Dictionary<string, ObservableCollection<Tuple<string, string>>>> AllFilesToImport => this.filesToImport;

		/// <summary>
		/// Vrací počet všech položek, které se budou importovat.
		/// </summary>
		public int FileCount => this.AllFilesToImport.Sum(f => f.Value.Sum(s => s.Value.Count));

		#region Akce pro komandy

		/// <summary>
		/// Inicializuje komandy pro stránku importu.
		/// </summary>
		private void InitCommands()
		{
			this.AddToTopLevelCollection = new Command(() => AddToCollection(
				this.TopLevelDirectories,
				this.NewTopLevelDirectory,
				value => this.SelectedTopLevelDirectory = value,
				value =>
				{
					this.NewTopLevelDirectory = string.Empty;
					this.SelectedTopLevelDirectory = value;
				}));

			this.RemoveFromTopLevelCollection = new ValueCommand<string>((toRemove) => RemoveByValue(
				this.TopLevelDirectories,
				toRemove,
				value =>
				{
					this.secondLevelDirectories.Remove(value);

					if (this.SelectedTopLevelDirectory == value)
						this.SelectedTopLevelDirectory = string.Empty;

					if (this.filesToImport.ContainsKey(value))
						this.filesToImport.Remove(value);
				}
			));

			this.AddToSecondLevelCollection = new Command(() => AddToCollection(
				this.SecondLevelDirectories,
				this.NewSecondLevelDirectory,
				value => this.SelectedSecondLevelDirectory = value,
				value =>
				{
					this.NewSecondLevelDirectory = string.Empty;
					this.SelectedSecondLevelDirectory = value;
				}));

			this.RemoveFromSecondLevelCollection = new ValueCommand<string>((toRemove) => RemoveByValue(
				this.SecondLevelDirectories,
				toRemove,
				value =>
				{
					if (this.SelectedSecondLevelDirectory == value)
						this.SelectedSecondLevelDirectory = string.Empty;

					if (this.filesToImport.ContainsKey(this.SelectedTopLevelDirectory) && this.filesToImport[this.SelectedTopLevelDirectory].ContainsKey(value))
						this.filesToImport[this.SelectedTopLevelDirectory].Remove(value);
				}
			));

			this.AddToFilesCollection = new Command(AddFiles);

			this.RemoveFromFilesCollection = new ValueCommand<Tuple<string, string>>((toRemove) => RemoveByValue(
				this.ImportFiles,
				toRemove,
				value => { }
			));
		}

		/// <summary>
		/// Přidá prvek do kolekce přičemž hlídá, aby nebyl přidán dvakrát.
		/// </summary>
		private static void AddToCollection(IList<string> destinationCollection,
			string newRecord, Action<string> onContainsAction, Action<string> callback)
		{
			if (destinationCollection.Contains(newRecord))
			{
				onContainsAction(newRecord);
			}
			else
			{
				destinationCollection.Add(newRecord);
			}

			callback(newRecord);
		}

		/// <summary>
		/// Odebere prvek z kolekce podle názvu.
		/// </summary>
		private static void RemoveByValue<T>(IList<T> destinationCollection,
			T remove, Action<T> onRemove)
		{
			var index = destinationCollection.IndexOf(remove);

			if (index > -1)
			{
				destinationCollection.RemoveAt(index);
				onRemove(remove);
			}
		}

		/// <summary>
		/// Přidá soubory k importu.
		/// </summary>
		private void AddFiles()
		{
			var dialog = new OpenFileDialog();

			// Povolím multiselect.
			dialog.Multiselect = true;

			// Pouze existující soubory.
			dialog.CheckFileExists = true;
			dialog.CheckPathExists = true;

			// Výchozí cesta (ale, uživatel si může vybrat i od jinud).
			dialog.InitialDirectory = this.dataSelectStep.FilePath;

			// Povolené koncovky.
			var extensions = Constants.CompatibleImageExtensions
				.Concat(Constants.CompatibleTextExtensions)
				.Select(e => $"*.{e.ToUpper()}")
				.ToArray();

			// Nastavím filtr.
			dialog.Filter = string.Format(ImportWizardResources.AdvancedFiltersStep_FilesFormat, string.Join(";", extensions));

			// Otevřu dialog.
			var result = dialog.ShowDialog();

			// Uživatel potvrdil.
			if (result == DialogResult.OK)
			{
				foreach (var fileName in dialog.FileNames)
				{
					this.ImportFiles.Add(Tuple.Create(fileName, Path.GetFileName(fileName)));
				}
			}
		}

		#endregion

		#region Page navigation

		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.AdvancedFilters;

		/// <inheritdoc cref="IWithNextPage.NextPage"/>
		public WizardPage NextPage => WizardPage.Confirm;

		/// <inheritdoc cref="IWithPrevPage.PrevPage"/>
		public WizardPage PrevPage => WizardPage.DataSelect;

		/// <summary>
		/// Live validace je nežádoucí.
		/// </summary>
		public override bool UseLiveValidation => false;

		/// <inheritdoc cref="IsValid" />
		public override bool IsValid()
		{
			base.IsValid();

			if (this.filesToImport == null || this.filesToImport.Count == 0)
			{
				this.ValidationMessages.Add(ImportWizardResources.AdvancedFiltersStep_ChooseForImport);
				return false;
			}
			else
			{
				foreach (var importCollection in this.filesToImport)
				{
					if (importCollection.Value == null || importCollection.Value.Count == 0)
					{
						this.ValidationMessages.Add(string.Format(ImportWizardResources.AdvancedFiltersStep_TopLevelRequired, importCollection.Key));
						return false;
					}
					else
					{
						foreach (var files in importCollection.Value)
						{
							if (files.Value == null || files.Value.Count == 0)
							{
								this.ValidationMessages.Add(string.Format(ImportWizardResources.AdvancedFiltersStep_SecondaryLevelRequired, files.Key));
								return false;
							}
						}
					}
				}
			}

			return true;
		}

		#endregion
	}
}
