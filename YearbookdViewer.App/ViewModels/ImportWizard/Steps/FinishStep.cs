﻿using YearbookViewer.App.Code.ImportWizard;

namespace YearbookViewer.App.ViewModels.ImportWizard.Steps
{
	/// <summary>
	/// Sedmý krok importu - oznámení o tom, že import proběhl.
	/// </summary>
	public class FinishStep : BaseStep
	{
		/// <inheritdoc cref="BaseStep.CurrentPage"/>
		public override WizardPage CurrentPage => WizardPage.Finish;

		/// <inheritdoc cref="BaseStep.IsValid"/>
		public override bool IsValid()
		{
			throw new System.NotImplementedException();
		}
	}
}