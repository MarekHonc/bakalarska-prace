﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.Common.Interfaces.DataInterfaces;

namespace YearbookViewer.App.ViewModels
{
	/// <summary>
	/// View model, sloužící pro zobrazení více výsledků po dokončení vyhledávání.
	/// </summary>
	public class SearchResultsViewModel : PopupViewModelBase
	{
		#region private variables

		private ObservableCollection<IContentData> yearBooks;
		private IContentData selectedYearBook;
		private string termToHighlight;

		#endregion

		#region Commands

		public ICommand Choose
		{
			get;
			internal set;
		}

		#endregion

		/// <summary>
		/// Vytvoří view model výběr z více nalezených výsledků.
		/// </summary>
		public SearchResultsViewModel()
		{
			this.Choose = new Command(() => { ChooseAction(); });
		}

		/// <summary>
		/// Vrací, jestli je vybrána nějaká ročenka.
		/// </summary>
		[DependsUpon(nameof(SelectedYearBook))]
		public bool IsYearBookSelected => this.SelectedYearBook != null;

		/// <summary>
		/// Vrací nebo nastavuje aktuálně zvolenou ročenku.
		/// </summary>
		public IContentData SelectedYearBook
		{
			get
			{
				return this.selectedYearBook;
			}
			set
			{
				this.selectedYearBook = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje kolekci všech ročenek, ze kterých si uživatel vybírá.
		/// </summary>
		public ObservableCollection<IContentData> YearBooks
		{
			get
			{
				return this.yearBooks;
			}
			set
			{
				this.yearBooks = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje termín, který bude zvýrazněn.
		/// </summary>
		public string TermToHighlight
		{
			get
			{
				return this.termToHighlight;
			}
			set
			{
				this.termToHighlight = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje akci, která se provede po vybrání prvku.
		/// </summary>
		public Action<IContentData> OnChooseAction
		{
			get;
			set;
		}

		/// <summary>
		/// Akce, která se provede po vybrání ročenky, pokud není vybraná, nic nedělá.
		/// </summary>
		private void ChooseAction()
		{
			if (this.SelectedYearBook == null)
				return;

			OnChooseAction(this.SelectedYearBook);
			this.IsPopupOpened = false;
		}

	}
}
