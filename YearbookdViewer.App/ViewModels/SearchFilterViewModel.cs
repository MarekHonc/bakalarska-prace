﻿using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.App.Code;
using System.Windows.Input;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.Common.Extensions;
using YearbookViewer.App.Models.Comboboxes;
using System.Linq;
using System.Collections.ObjectModel;
using YearbookViewer.App.Code.Interfaces;
using System;
using YearbookViewer.Common;
using YearbookViewer.Common.Enums;

namespace YearbookViewer.App.ViewModels
{
	/// <summary>
	/// View model pro vyhledávání v ročenkách.
	/// </summary>
	public class SearchFilterViewModel : ViewModelBase, IFilterData, IStopableUpdatePropagation
	{
		/// <summary>
		/// Konstanta označující kolik chci vrátit výsledků, při vyhledávání.
		/// </summary>
		private const int FilterByTextFetchCount = 10;

		private readonly ISearchFilterValueProvider searchValueProvider;
		private readonly IContentDataProvider contentDataProvider;
		private readonly IExpressionEvaluator expressionEvaluator;

		private readonly SearchResultsViewModel searchResultsViewModel;

		#region Private properties

		private string selectedTopLevelFilter;
		private ContentComboboxValueModel selectedSecondaryFilter;
		private PageNameComboboxValueModel selectedPage;

		private string searchTerm;
		private bool isExpression;
		private bool useGlobalSearch;
		private ObservableCollection<string> availableYears;
		private FilterMode filterMode;

		#endregion

		#region Commands

		public ICommand FilterByText
		{
			get;
			private set;
		}

		public ICommand ClearFilters
		{
			get;
			private set;
		}


		#endregion

		/// <summary>
		/// Vytvoří view model pro práci s filtry.
		/// </summary>
		/// <param name="searchValueProvider">Provider pro hodnoty, které se dosazují do vyhledávání <see cref="ISearchFilterValueProvider"/>.</param>
		/// <param name="contentDataProvider">Provider, který umožní stahovat data z ročenek <see cref="IContentDataProvider"/>.</param>
		/// <param name="expressionEvaluator">Vyhodnocovač výrazů pro vyhledávání v ročenkách.</param>
		/// <param name="searchResultsViewModel">View model, používaný pro robrazování výsledků hledání.</param>
		public SearchFilterViewModel(ISearchFilterValueProvider searchValueProvider, IContentDataProvider contentDataProvider,
			IExpressionEvaluator expressionEvaluator, SearchResultsViewModel searchResultsViewModel)
		{
			this.searchValueProvider = searchValueProvider;
			this.contentDataProvider = contentDataProvider;
			this.searchResultsViewModel = searchResultsViewModel;
			this.expressionEvaluator = expressionEvaluator;

			this.searchResultsViewModel.OnChooseAction = (selected) =>
			{
				this.LoadYearBook(selected);
				this.OnSearch();
			};

			// Commandy
			this.FilterByText = new Command(() => this.FilterByTextAction());
			this.ClearFilters = new Command(() => this.ClearFiltersAction());
		}

		/// <summary>
		/// Vrací všechny dostupné roky, ve kterých jsou ročenky.
		/// </summary>
		/// <remarks>Možná bude objekt?</remarks>
		public ObservableCollection<string> AvailableTopLevelFilters
		{
			get
			{
				if(this.availableYears == null)
					this.availableYears = this.searchValueProvider.GetTopLevelFilter().ToObservableCollection();

				return this.availableYears;
			}
			set
			{
				this.availableYears = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuálně zvolený rok ročenky, pro filtrování.
		/// </summary>
		[UpdateAnotherModel(nameof(MainContentViewModel))]
		public string SelectedTopLevelFilter
		{
			get { return this.selectedTopLevelFilter; }
			set
			{
				this.selectedTopLevelFilter = value;

				this.StopUpdatePropagation = true;

				this.SelectedPage = null;
				this.selectedSecondaryFilter = null;

				this.StopUpdatePropagation = false;

				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací všechny kategorie (?) ročenek.
		/// </summary>
		/// <remarks>Možná bude objekt?</remarks>
		[DependsUpon(nameof(SelectedTopLevelFilter))]
		public ObservableCollection<ContentComboboxValueModel> AvailableSecondaryFilters
		{
			get
			{
				// Vytvořím objekty pro combobox.
				var contents = this.searchValueProvider.GetSecondaryFilter(this.SelectedTopLevelFilter);
				var comboboxValues = contents.Select((content, index) => new ContentComboboxValueModel(content, index, this.SelectedTopLevelFilter));

				return comboboxValues.ToObservableCollection();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuálně zvolenou kategorie(?) ročenky, pro filtrování.
		/// </summary>
		/// <remarks>Kategorii, spíš nadpis, okruh -> nemám vhodné slovo.</remarks>
		[UpdateAnotherModel(nameof(MainContentViewModel))]
		public ContentComboboxValueModel SelectedSecondaryFilter
		{
			get { return this.selectedSecondaryFilter; }
			set
			{
				this.selectedSecondaryFilter = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací všechny názvy stánke ročenek.
		/// </summary>
		/// <remarks>Možná bude objekt?</remarks>
		[DependsUpon(nameof(SelectedSecondaryFilter), nameof(SelectedTopLevelFilter))]
		public ObservableCollection<PageNameComboboxValueModel> AvailablePageNames
		{
			get
			{
				// Vytvořím si hezký objekt pro combobox.
				var pages = this.searchValueProvider.YearBookNames(this.SelectedTopLevelFilter, this.SelectedSecondaryFilter?.Value);
				var comboboxValues = pages
					.Take(200) // Pro jistotu, aby seznam nebyl dlouhý jak týden před výplatou.
					.Select((page, index) => new PageNameComboboxValueModel(page, index, this.SelectedTopLevelFilter));

				return comboboxValues.ToObservableCollection();
			}
		}

		/// <summary>
		/// Vrací jestli jsou dostupné prvky pro další filtrování.
		/// </summary>
		[DependsUpon(nameof(SelectedTopLevelFilter))]
		public bool ExtendedFiltersEnabled => this.SelectedTopLevelFilter.IsNotNullOrEmpty();

		/// <summary>
		/// Vrací nebo nastavuje název ročenky, pro filtrování.
		/// </summary>
		[UpdateAnotherModel(nameof(MainContentViewModel))]
		public PageNameComboboxValueModel SelectedPage
		{
			get { return this.selectedPage; }
			set
			{
				this.selectedPage = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje vyhledávaný termín.
		/// </summary>
		public string SearchTerm
		{
			get { return this.searchTerm; }
			set
			{
				this.searchTerm = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje jestli se má termín pro vyhledávání interpretovat jako výraz.
		/// </summary>
		public bool IsExpression
		{
			get 
			{
				// Pokud nedovoluji výrazy tak všude dávám false.
				if (!(this.expressionEvaluator.Instance is IExpressionAcceptable))
					return false;

				return this.isExpression; 
			}
			set
			{
				if (this.isExpression == value)
					return;

				this.isExpression = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, jestli se pro vyhledávání mezi ročenkami může použít i regex.
		/// </summary>
		[DependsUpon(nameof(AvailableTopLevelFilters))]
		public bool IsExpressionSearchEnabled => this.expressionEvaluator.Instance is IExpressionAcceptable;

		/// <summary>
		/// Vrací jestli je zadaný vyhledávaný termín platný a lze podle něj vyhledávat.
		/// </summary>
		[DependsUpon(nameof(IsExpression), nameof(SearchTerm))]
		public bool IsSearchValid
		{
			get
			{
				if (!this.IsExpression)
					return this.SearchTerm.IsNotNullOrEmpty();

				return this.expressionEvaluator.IsValid(this.SearchTerm);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje možnost vyhledání napříč všemi soubory.
		/// např. chci vyhledat text napříč celou složkou.
		/// </summary>
		public bool UseGlobalSearch
		{
			get { return this.useGlobalSearch; }
			set
			{
				if (this.useGlobalSearch == value)
					return;

				this.useGlobalSearch = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací aktuální mód filtrů aplikace.
		/// </summary>
		public FilterMode FilterMode
		{
			get { return this.filterMode; }
			set
			{
				this.filterMode = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, jestli je vynuceno zastavení propagece updatu ostatních component na tomto modelu.
		/// </summary>
		public bool StopUpdatePropagation
		{
			get;
			private set;
		}

		/// <summary>
		/// Vrací nebo nastavuje model, pro zobrazení výsledků vyhledávání.
		/// </summary>
		public SearchResultsViewModel SearchResultsViewModel
		{
			get
			{
				return this.searchResultsViewModel;
			}
			set
			{
				this.SearchResultsViewModel = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje akci, které se provede úspěšném vyhledání podle textu.
		/// </summary>
		public Action OnSearch
		{
			get;
			set;
		}

		/// <summary>
		/// Vynuluje filtry, které jsou aktuálně používané.
		/// </summary>
		/// <param name="clearTopLevelFilters">Určuje, zda-li se mají vyčistit i filtry hlavní úrovně.</param>
		public void ClearFiltersAction(bool clearTopLevelFilters = false)
		{
			// Zastavím propagaci ven z modelu. TODO: možná předělat aby to šlo použít jako using(.....), nebo šikovně zabalit pro lepší čitelnost.
			this.StopUpdatePropagation = true;

			// Resetuji comboboxy.
			if (this.FilterMode >= FilterMode.TopLevelFilter)
			{
				this.SelectedTopLevelFilter = null;
			}

			if (this.FilterMode >= FilterMode.SecondaryFilter)
			{
				this.SelectedSecondaryFilter = null;
			}

			this.SelectedPage = null;

			// Opět zapnu propagaci.
			this.StopUpdatePropagation = false;

			// Pokud chci vymazat i vrchní filtry.
			if (clearTopLevelFilters)
			{
				// Vymažu hodnotu cover property.
				this.availableYears = null;

				// A znovu natáhnu vše.
				this.AvailableTopLevelFilters = this.AvailableTopLevelFilters;
			}

			// A oznámím změnu (vlastně je mi jedno na čem).
			RaisePropertyChanged(nameof(this.SelectedTopLevelFilter));

			// Vyhledávání
			this.SearchTerm = null;
			this.IsExpression = false;
			this.UseGlobalSearch = false;
		}

		/// <summary>
		/// Aplikuje mód aplikace na zobrazení filtrů.
		/// </summary>
		public void ApplyModeToFilters(FilterMode mode)
		{
			// Zjistím, jestli je vůbec mód podporován.
			if (!Constants.ValidFilterModes.Contains(mode))
				throw new NotSupportedException($"Mode '{mode}' is not supported by application");

			// Pokud neobsahuji top LevelFilter, tak ho nastavím na výchozí hodnotu.
			if (mode < FilterMode.TopLevelFilter)
				this.SelectedTopLevelFilter = Constants.DefaultTopLevelFilter;

			// Pokud neobsahuji sekundární filtr.
			if(mode < FilterMode.SecondaryFilter)
				this.SelectedSecondaryFilter = new ContentComboboxValueModel(Constants.SecondaryLevelFilter, 0, Constants.DefaultTopLevelFilter);

			// A nastavím mód.
			this.FilterMode = mode;

			// Notifikuji změny - rušně, aby bylo 100%.
			RaisePropertyChanged(nameof(ExtendedFiltersEnabled));
		}

		/// <summary>
		/// Vyhledá podle textu.
		/// </summary>
		private void FilterByTextAction()
		{
			/* Dva případy
				Pokud mám zapnuté globální vyhledávání vyhledávám napříč vším a pak jdu na bod 2.
				
				Jinak:
					1) Mám vybranou stránku -> plynule přejdu do zvýraznění v textu
					2) Vyhledávání vrátí jeden výsledek -> stejný postup jako v bodu 1
					3) Mám více výsledků -> zobrazím popup s možností výběru, po kliknutí se chovám jako když najdu 1 výsledek.
			*/

			if (this.SelectedPage == null || this.UseGlobalSearch)
			{
				// Získám data z fitlrů.
				var data = this.contentDataProvider.GetCount(this, FilterByTextFetchCount);

				// Mám jednu.
				if (data.Count == 1)
				{
					// Načtu první ročenku a o vím se nestarám.
					LoadYearBook(data.First());
				}
				else
				{
					this.SearchResultsViewModel.IsPopupOpened = true;
					this.SearchResultsViewModel.YearBooks = data.ToObservableCollection();

					if (this.IsExpression)
					{
						this.SearchResultsViewModel.TermToHighlight = HighLighter.EXPRESSION_PREFIX + this.SearchTerm;
					}
					else
					{
						this.SearchResultsViewModel.TermToHighlight = this.SearchTerm;
					}

					return;
				}
			}

			// Ještě řeknu dál že mohou reagovat.
			this.OnSearch();
		}

		/// <summary>
		/// Nastaví danou ročenku jako aktuálně zvolenou.
		/// </summary>
		/// <param name="data"></param>
		private void LoadYearBook(IContentData data)
		{
			// Vypnu update ven z modelu.
			this.StopUpdatePropagation = true;

			// Vyberu rok i název stránky.
			this.SelectedTopLevelFilter = data.Year;
			this.SelectedPage = this.AvailablePageNames.First(p => p.Value == data.PageName);

			this.StopUpdatePropagation = false;

			// A upozorním zbytek aplikace že chci přenačíst.
			RaisePropertyChanged(nameof(SelectedPage));
		}

		/// <summary>
		/// Přepne aktuálně zobrazovanou stránku.
		/// </summary>
		/// <param name="moveBackwards">Příznak jestli mám přepínat stránky dozadu.</param>
		public void SwitchPage(bool moveBackwards)
		{
			// Mám nic, nebo jeden prvek -> ani nemůžu nikam posunout.
			if (this.AvailablePageNames.Count <= 1)
				return;

			// Buď mám už něco vybrané, nebo předpokládám předvýběr první položky.
			var startingIndex = this.SelectedPage?.Index ?? 0;

			// Posunu číslo stránky správným směrem.
			startingIndex += moveBackwards ? -1 : 1;
			var newIndex = startingIndex < 0 ? (startingIndex + this.AvailablePageNames.Count) : (startingIndex % this.AvailablePageNames.Count);

			// Jenom posunu zpátky, model si už na změnu zareaguje sám.
			this.SelectedPage = this.AvailablePageNames[newIndex];
		}

		#region Implementace IFilterData

		string IFilterData.TopLevelFilter => this.SelectedTopLevelFilter;

		string IFilterData.SecondaryFilter => this.SelectedSecondaryFilter?.Year == this.SelectedTopLevelFilter ? this.SelectedSecondaryFilter?.Value : null;

		string IFilterData.PageName => this.selectedPage?.Year == this.SelectedTopLevelFilter ? this.SelectedPage?.Value : null;

		string IFilterData.SearchTerm => this.SearchTerm;

		bool IFilterData.IsExpression => this.IsExpression;

		bool IFilterData.UseGlobalSearch => this.UseGlobalSearch;

		#endregion
	}
}
