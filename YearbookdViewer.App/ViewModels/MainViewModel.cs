﻿using System;
using System.Globalization;
using System.IO;
using System.Windows.Input;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.App.Localization;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.Common;
using YearbookViewer.Common.Interfaces;

namespace YearbookViewer.App.ViewModels
{
	/// <summary>
	/// Hlavní view model pro celou aplikaci.
	/// </summary>
	public class MainViewModel : ViewModelBase
	{
		#region Private variables

		private readonly SearchFilterViewModel searchFilterViewModel;
		private readonly MainContentViewModel mainContentViewModel;
		private readonly SettingsViewModel settingsViewModel;
		private readonly ISearchFilterValueProvider searchFilterProvider;
		private readonly IContentDataProvider dataProvider;
		private readonly IUserSettingsProvider userSettingsProvider;

		public ICommand ClosePopups
		{
			get;
			private set;
		}

		#endregion

		/// <summary>
		/// Konstruktor pro hlavní model aplikace, v parametrech příjmá modely z dependency injection.
		/// </summary>
		/// <param name="searchFilterViewModel">View model pro vyhledávání v ročenkách <see cref="ViewModels.SearchFilterViewModel"/>.</param>
		/// <param name="mainContentViewModel">View model pro hlavní zobrazení informací <see cref="ViewModels.MainContentViewModel"/>.</param>
		/// <param name="settingsViewModel">View model pro nastavení aplikace uživatelem <see cref="ViewModels.SettingsViewModel"/>.</param>
		/// <param name="searchFilterProvider">Provider pro hodnoty, které se dosazují do vyhledávání <see cref="ISearchFilterValueProvider"/>.</param>
		/// <param name="dataProvider">Provider, který umožní stahovat data z ročenek <see cref="IContentDataProvider"/>.</param>
		/// <param name="userSettingsProvider">Prover, který aplikaci říká jak má být nastavena <see cref="IUserSettingsProvider"/>.</param>
		public MainViewModel(SearchFilterViewModel searchFilterViewModel,
			MainContentViewModel mainContentViewModel, SettingsViewModel settingsViewModel,
			ISearchFilterValueProvider searchFilterProvider, IContentDataProvider dataProvider,
			IUserSettingsProvider userSettingsProvider)
		{
			this.searchFilterViewModel = searchFilterViewModel;
			this.mainContentViewModel = mainContentViewModel;
			this.settingsViewModel = settingsViewModel;
			this.searchFilterProvider = searchFilterProvider;
			this.dataProvider = dataProvider;
			this.userSettingsProvider = userSettingsProvider;

			var settings = userSettingsProvider.Load();
			UpdateFromSettings(settings);
			LoadFileSource(settings);

			RegisterPropertyChangedBetweenModels();

			this.ClosePopups = new Command(() =>
			{
				this.SettingsViewModel.IsPopupOpened = false;
				this.SearchFilterViewModel.SearchResultsViewModel.IsPopupOpened = false;
			});
		}

		/// <summary>
		/// Vrací view model pro ovládání vyhledávacích prvků.
		/// </summary>
		public SearchFilterViewModel SearchFilterViewModel
		{
			get
			{
				return this.searchFilterViewModel;
			}
		}

		/// <summary>
		/// Vrací view model pro zobrazení hlavního obsahu aplikace.
		/// </summary>
		public MainContentViewModel MainContentViewModel
		{
			get
			{
				return this.mainContentViewModel;
			}
		}

		/// <summary>
		/// Vrací view model pro nastavení aplikace uživatelem.
		/// </summary>
		public SettingsViewModel SettingsViewModel
		{
			get
			{
				return this.settingsViewModel;
			}
		}

		/// <summary>
		/// Zaregistruje reakce na změny properties mezi modely.
		/// </summary>
		private void RegisterPropertyChangedBetweenModels()
		{
			this.SearchFilterViewModel.PropertyChanged += (s, args) =>
			{
				if (this.SearchFilterViewModel.ShouldBeUpdated(nameof(MainContentViewModel), args.PropertyName))
				{
					mainContentViewModel.Update();
				}
			};

			// Na vyhledání -> ještě obnovím i vyhledávání v textu.
			this.SearchFilterViewModel.OnSearch = () => { mainContentViewModel.UpdateSearch(); };

			// Po uložení nastavení musím promítnout změny do celé aplikace.
			this.SettingsViewModel.OnSettingsUpdated = (settings) => { UpdateFromSettings(settings); };

			// Pokud si model přeje uložit layout -> umožním mu to.
			this.MainContentViewModel.SaveLayoutAction = () => { this.SettingsViewModel.SaveLayout(); };
		}

		/// <summary>
		/// Akce, která se stará o provedení updatu po nahrání nového nastavení.
		/// </summary>
		/// <param name="settings">Nové nastavení, které mám použít.</param>
		private void UpdateFromSettings(IAppSettings settings)
		{
			CultureResources.ChangeCulture(new CultureInfo(settings.Culture));
			this.SearchFilterViewModel.ApplyModeToFilters(settings.ApplicationMode);
		}

		/// <summary>
		/// Nahraje do providerů dat složku, odkud si aplikace získává data.
		/// </summary>
		private void LoadFileSource(IAppSettings settings)
		{
			// Získám cestu, kde se nahází soubory.
			var path = Path.Combine(Constants.FileStoreLocation, Constants.DataFolderName);

			// Pokud složka neexistuje -> vytvořím.
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);

			// A nahraji ji do providerů.
			if (dataProvider.Instance is IFilePathRequired dataProviderPath)
				dataProviderPath.SetPaths(path, settings.ServerAddress);

			if (searchFilterProvider.Instance is IFilePathRequired searchFilterPath)
				searchFilterPath.SetPaths(path, settings.ServerAddress);
		}
	}
}
