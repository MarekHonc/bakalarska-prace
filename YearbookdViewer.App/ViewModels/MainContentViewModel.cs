﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Localization;
using YearbookViewer.App.Models;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.Common.Interfaces.DataInterfaces;
using YearbookViewer.Readers;
using YearbookViewer.Readers.TesseractOCR;
using MessageBox = System.Windows.MessageBox;

namespace YearbookViewer.App.ViewModels
{
	/// <summary>
	/// View model pro hlavní obsah (obrázek + text).
	/// </summary>
	public class MainContentViewModel : ViewModelBase, IContentData
	{
		private readonly IContentDataProvider dataProvider;
		private readonly SearchFilterViewModel seachFilter;
		private readonly SettingsViewModel settings;

		#region Private variables

		private string name;
		private string imageUrl;
		private string imageText;
		private bool isDataAvailable;
		private string searchInText;
		private string year;
		private string oldText;
		private bool saveSuccess;
		private string pagePath;

		#endregion

		#region Commands

		public ICommand SwitchCurrentPage
		{
			get;
			private set;
		}

		public ICommand Save
		{
			get;
			private set;
		}

		public ICommand SaveAndExport
		{
			get;
			private set;
		}

		public ICommand SaveLayout
		{
			get;
			private set;
		}

		#endregion

		/// <summary>
		/// Vytvoří novou instaci model pro zobrazení hlavního obsahu aplikace.
		/// </summary>
		/// <param name="dataProvider">Provider, poskytující data, získaný z dependency injection <see cref="IContentDataProvider"/>.</param>
		public MainContentViewModel(IContentDataProvider dataProvider, SearchFilterViewModel seachFilter, SettingsViewModel settings)
		{
			this.dataProvider = dataProvider;
			this.seachFilter = seachFilter;
			this.settings = settings;

			// Inicializuji command.
			this.SwitchCurrentPage = new ValueCommand<bool>((value) => this.SwitchPageAction(value));
			this.Save = new Command(() => this.SaveAction());
			this.SaveAndExport = new Command(() => this.SaveAndExportAction());
			this.SaveLayout = new Command(() => this.SaveLayoutAction());
		}

		/// <summary>
		/// Vrací nebo natavuje název ročenky.
		/// </summary>
		public string PageName
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje cestu k textovému souboru.
		/// </summary>
		public string PagePath
		{
			get
			{
				return this.pagePath;
			}
			set
			{
				this.pagePath = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje rok ročenky.
		/// </summary>
		public string Year
		{
			get
			{
				return this.year;
			}
			set
			{
				this.year = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje cestu k obrázku.
		/// </summary>
		public string ImageUrl
		{
			get
			{
				return this.imageUrl;
			}
			set
			{
				this.imageUrl = value;
				
				// Pokud OCR použít lze -> rovnou konvertuji.
				if (OCRReader.CanBeUsed())
				{
					// Získám data z OCR.
					OCRReader.ReadImage(
						this.ImageUrl,
						this.settings.SelectedOCRLanguage.Value, 
						ocrPath =>
						{
							this.ocrConvertedPath = ocrPath;
							RaisePropertyChanged(nameof(Highlights));
						}
					);
				}

				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací obrázek, který se nachází na <see cref="ImageUrl"/> jako Bitmapu.
		/// </summary>
		[DependsUpon(nameof(ImageUrl))]
		public BitmapImage Image
		{
			get
			{
				if (this.ImageUrl.IsNullOrEmpty())
					return null;

				var uri = new Uri(this.ImageUrl, UriKind.RelativeOrAbsolute);

				return new BitmapImage(uri);
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje text, který je na obrázku.
		/// </summary>
		public string ImageText
		{
			get
			{
				return this.imageText;
			}
			set
			{
				this.imageText = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje text, který je aktuálně vyhledávan v textu.
		/// </summary>
		/// <remarks>Upozorním i text, že byl změněn, aby jsem byl schopen zareagovat na změnu.</remarks>
		[DependsUpon(nameof(ImageText))]
		public string SearchInText
		{
			get
			{
				return this.searchInText;
			}
			set
			{
				this.searchInText = value;
				RaisePropertyChanged();
			}
		}

		#region OCR

		/// <summary>
		/// Proměnná, pro uložení dat z OCR (stačí když se aktualizují při změně obrázku).
		/// </summary>
		private string ocrConvertedPath;

		/// <summary>
		/// Vrací všechny oblasti zájmu podle aktuálně vyhledávaného pojmu.
		/// </summary>
		[DependsUpon(nameof(SearchInText))]
		public ImageHighlightArea[] Highlights
		{
			get
			{
				if (this.ocrConvertedPath.IsNullOrEmpty())
					return Array.Empty<ImageHighlightArea>();

				return ImageHighlightArea.Create(this.ocrConvertedPath, this.SearchInText, this.settings.OCRHighlightWholeWord);
			}
		}

		#endregion

		/// <summary>
		/// Vrací nebo nastavuje jestli mám k dispozici data k zobrazení.
		/// </summary>
		public bool IsDataAvailable
		{
			get
			{
				return this.isDataAvailable;
			}
			set
			{
				if (value == isDataAvailable)
					return;

				this.isDataAvailable = value;
				RaisePropertyChanged();
			}
		}


		/// <summary>
		/// Vrací nebo nastavuje, jestli uložení proběhlo úspěšně.
		/// </summary>
		public bool SaveSuccess
		{
			get
			{
				return this.saveSuccess;
			}
			set
			{
				if (value == this.saveSuccess)
					return;

				this.saveSuccess = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací aktuálně používané filtry.
		/// </summary>
		public SearchFilterViewModel Filters => this.seachFilter;

		/// <summary>
		/// Provede přenačtení ze zdroje dat, aby zobrazovaný obsah seděl s tím, co je aktuálně filtrováno.
		/// </summary>
		public void Update()
		{
			// Pokusím se stáhnout data.
			var result = this.dataProvider.GetFirst(this.seachFilter);

			// Informuji model o tom, jestli mám něco staženého.
			this.IsDataAvailable = result != null;

			// Pokud mám, rovnou model naplním.
			if (this.IsDataAvailable)
			{
				UpdateFromData(result);
			}
		}

		/// <summary>
		/// Provede update tohoto model z dat, která stáhnu.
		/// </summary>
		private void UpdateFromData(IContentData data)
		{
			this.PageName = data.PageName;
			this.ImageUrl = data.ImageUrl;
			this.ImageText = data.ImageText;
			this.PagePath = data.PagePath;
			this.oldText = data.ImageText;
		}

		/// <summary>
		/// Přepne aktuálně zobrazovanou stránku.
		/// </summary>
		/// <param name="moveBackwards">Příznak jestli mám přepínat stránky dozadu.</param>
		private void SwitchPageAction(bool moveBackwards)
		{
			this.seachFilter.SwitchPage(moveBackwards);
		}

		/// <summary>
		/// Provede update na vyhledávácím textboxu.
		/// </summary>
		public void UpdateSearch()
		{
			if (this.seachFilter.IsExpression)
			{
				this.SearchInText = HighLighter.EXPRESSION_PREFIX + this.seachFilter.SearchTerm;
			}
			else
			{
				this.SearchInText = this.seachFilter.SearchTerm;
			}
		}

		/// <summary>
		/// Uloží změněný text v aktuálním souboru.
		/// </summary>
		private void SaveAction()
		{
			var saved = this.SaveCore();

			if (saved)
			{
				this.SaveSuccess = true;
				this.ExecuteWithDelay(() => this.SaveSuccess = false, TimeSpan.FromSeconds(2));
			}
		}

		/// <summary>
		/// Uloží a exportuje text v aktuálním souboru.
		/// </summary>
		private void SaveAndExportAction()
		{
			// Nejdříve se ptám na uložení.
			if (this.settings.ConfirmSave)
			{
				// Zobrazím potvrzení.
				var confirmed = MessageBox.Show(Resources.MainContentViewModel_SaveAndExportYearbookQuestion,
					Resources.MainContentViewModel_SaveYearbook, MessageBoxButton.YesNo);

				if (confirmed != MessageBoxResult.Yes)
					return;
			}

			// Vytvořím dialog na export.
			var dialog = new SaveFileDialog();

			// Nastavím výchozí lokaci na dokumenty uživatele.
			dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			
			// Nastavím rovnou i název a správný typ souboru.
			dialog.FileName = Path.GetFileName(this.PagePath);
			var extension = Path.GetExtension(this.PagePath);
			dialog.Filter = string.Format(Resources.MainContentViewModel_ExportFileType, extension);

			// Ukážu dialog.
			var result = dialog.ShowDialog();

			if (result == DialogResult.OK)
			{
				var saved = this.SaveCore(skipConfirmation: true);

				// Nepodařilo se uložit - nedělám nic.
				if (!saved && this.oldText != this.ImageText)
					return;

				if (!CustomTextReader.GetReader(extension).Write(dialog.FileName, this.ImageText))
				{
					// Export se nepovedl -> tak dám vědět.
					MessageBox.Show(Resources.MainContentViewModel_SaveYearbookFailed,
						Resources.MainContentViewModel_SaveYearbook, MessageBoxButton.OK);

					return;
				}

				this.SaveSuccess = true;
				this.ExecuteWithDelay(() => this.SaveSuccess = false, TimeSpan.FromSeconds(2));
			}
		}

		/// <summary>
		/// Uloží změněný text v aktuálním souboru.
		/// </summary>
		private bool SaveCore(bool skipConfirmation = false)
		{
			// Při změně.
			if (this.oldText != this.ImageText)
			{
				if (this.settings.ConfirmSave && !skipConfirmation)
				{
					// Zobrazím potvrzení.
					var result = MessageBox.Show(Resources.MainContentViewModel_SaveYearbookQuestion,
						Resources.MainContentViewModel_SaveYearbook, MessageBoxButton.YesNo);

					if (result != MessageBoxResult.Yes)
						return false;
				}

				// Chci uložit -> uložím.
				if (!this.dataProvider.Save(this.seachFilter, this.ImageText))
				{
					// Uložení se nepovedlo -> tak dám vědět.
					MessageBox.Show(Resources.MainContentViewModel_SaveYearbookFailed,
						Resources.MainContentViewModel_SaveYearbook, MessageBoxButton.OK);

					return false;
				}

				this.oldText = this.ImageText;

				return true;
			}

			return false;
		}

		/// <summary>
		/// Vrací nebo nastvuje akci, která je volána při uložení nastavení.
		/// </summary>
		public Action SaveLayoutAction
		{
			get;
			set;
		} = () => { };

		#region NotUsed

		public string ImageTextForHighlight => throw new NotImplementedException();

		#endregion
	}
}
