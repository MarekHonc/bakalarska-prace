﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.App.Localization;
using YearbookViewer.App.Models.Comboboxes;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.App.ViewModels.ExportWizard;
using YearbookViewer.App.ViewModels.ImportWizard;
using YearbookViewer.Common;
using YearbookViewer.Common.Enums;
using YearbookViewer.Common.Extensions;
using YearbookViewer.Readers.TesseractOCR;

namespace YearbookViewer.App.ViewModels
{
	/// <summary>
	/// View model pro nastavení aplikace uživatelem.
	/// </summary>
	public class SettingsViewModel : PopupViewModelBase, IAppSettings
	{
		private readonly IUserSettingsProvider settingsProvider;
		private readonly ImportWizardViewModel importModel;
		private readonly ExportWizardViewModel exportModel;
		private readonly SearchSpeedTest searchSpeedTest;
		private bool saveSuccess;
		private CultureComboboxValueModel selectedCulture;
		private int imageWidth;
		private int textWidth;
		private bool confirmSave;
		private DataSourceType currentDataSource;
		private string serverAddress;
		private FilterMode applicationMode;
		private OCRLanguageComboboxValueModel selectedOcrLanguage;
		private bool ocrHighlightWholeWord;

		#region Commands

		public ICommand Save { get; private set; }

		public ICommand OpenImport { get; private set; }

		public ICommand ExportAll { get; private set; }

		#endregion

		/// <summary>
		/// Konstruktor pro view model s nastavení aplikace.
		/// </summary>
		/// <param name="settingsProvider">Prover, který aplikaci říká jak má být nastavena <see cref="IUserSettingsProvider"/>.</param>
		/// <param name="importModel">Model pro import.</param>
		/// <param name="exportModel">Model pro export dat.</param>
		/// <param name="searchSpeedTest">Model pro provedení rychlostních testů.</param>
		public SettingsViewModel(IUserSettingsProvider settingsProvider,
			ImportWizardViewModel importModel, ExportWizardViewModel exportModel,
			SearchSpeedTest searchSpeedTest)
		{
			this.settingsProvider = settingsProvider;
			this.importModel = importModel;
			this.exportModel = exportModel;
			this.searchSpeedTest = searchSpeedTest;

			LoadFromSettings();

			this.Save = new Command(() => this.SaveSettings());
			this.OpenImport = new Command(() => this.OpenImportWindow());
			this.ExportAll = new Command(() => this.OpenExportAllWindow());
			this.DoTestCommand = new Command(() => this.DoTest());

			// Po skončení importu notifikuji i tento model, že se import dokončil.
			ImportWizardViewModel.OnImportCompleted += (sender, args) =>
			{
				RaisePropertyChanged(nameof(DataImported));
			};
		}

		/// <summary>
		/// Vrací název aktuálně zvolené kultury.
		/// </summary>
		public string Culture => this.SelectedCulture.Value.Name;

		/// <summary>
		/// Vrací nebo nastavuje aktuálně zvolenou kulturu.
		/// </summary>
		public CultureComboboxValueModel SelectedCulture
		{
			get
			{
				return this.selectedCulture;
			}
			set
			{
				this.selectedCulture = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací všechny dostupné kultury.
		/// </summary>
		public ObservableCollection<CultureComboboxValueModel> AvailableCultures
		{
			get
			{
				return CultureResources.SupportedCultures
					.Select((culture, index) => new CultureComboboxValueModel(culture, index)).ToObservableCollection();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje šířku na kterou se zobrazí obrázek.
		/// </summary>
		public int ImageWidth
		{
			get
			{
				return this.imageWidth;
			}
			set
			{
				this.imageWidth = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje šířku na kterou se zobrazí text.
		/// </summary>
		public int TextWidth
		{
			get
			{
				return this.textWidth;
			}
			set
			{
				this.textWidth = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje, jestli uložení proběhlo úspěšně.
		/// </summary>
		public bool SaveSuccess
		{
			get
			{
				return this.saveSuccess;
			}
			set
			{
				if (value == this.saveSuccess)
					return;

				this.saveSuccess = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje akci, které se zavolá po změně nastavení.
		/// </summary>
		public Action<IAppSettings> OnSettingsUpdated 
		{
			get; 
			set; 
		}

		/// <summary>
		/// Vrací nebo nastavuje, jestli se má aplikace dotazovat o uložení.
		/// </summary>
		public bool ConfirmSave 
		{
			get
			{
				return this.confirmSave;
			}
			set
			{
				if (value == this.confirmSave)
					return;

				this.confirmSave = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje, aktuální zdroj dat.
		/// </summary>
		public DataSourceType CurrentDateSource
		{
			get
			{
				return this.currentDataSource;
			}
			set
			{
				this.currentDataSource = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuální mód filtrů aplikace.
		/// </summary>
		public FilterMode ApplicationMode
		{
			get
			{
				return this.applicationMode;
			}
			set
			{
				this.applicationMode = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje adresu serveru, kde jsou data.
		/// </summary>
		public string ServerAddress
		{
			get
			{
				return this.serverAddress;
			}
			set
			{
				this.serverAddress = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, zda-li aplikace může používat OCR čtečku.
		/// </summary>
		public bool OCREnabled
		{
			get
			{
				return OCRReader.CanBeUsed();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje aktuální jazyk OCR.
		/// </summary>
		public OCRLanguageComboboxValueModel SelectedOCRLanguage
		{
			get
			{
				return this.selectedOcrLanguage;
			}
			set
			{
				this.selectedOcrLanguage = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje, zda-li se při nálezu pomocí OCR má zobrazovat celé slovo, nebo jenom část.
		/// </summary>
		public bool OCRHighlightWholeWord
		{
			get
			{
				return this.ocrHighlightWholeWord;
			}
			set
			{
				if (value == this.ocrHighlightWholeWord)
					return;

				this.ocrHighlightWholeWord = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nastavení jazyku ocr.
		/// </summary>
		OCRLanguage IAppSettings.OCRLanguage => this.SelectedOCRLanguage.Value;

		/// <summary>
		/// Vrací všechny dostupný jazyky pro OCR.
		/// </summary>
		public ObservableCollection<OCRLanguageComboboxValueModel> AvailableOCRLanguages
		{
			get
			{
				return ((OCRLanguage[])Enum.GetValues(typeof(OCRLanguage)))
					.Select((culture, index) => new OCRLanguageComboboxValueModel(culture, index)).ToObservableCollection();
			}
		}

		/// <summary>
		/// Vrací, zda-li má aplikace nějaká data, se kterými se dá pracovat.
		/// </summary>
		public bool DataImported =>
			FileBrowser.GetNumberOfFiles(Path.Combine(Constants.FileStoreLocation, Constants.DataFolderName)) > 0;

		/// <summary>
		/// načte nastavení aplike pro možnou změnu.
		/// </summary>
		private void LoadFromSettings()
		{
			var settings = settingsProvider.Load();

			this.SelectedCulture = this.AvailableCultures.First(c => c.Value.Name == settings.Culture);
			this.SelectedOCRLanguage = this.AvailableOCRLanguages.First(l => l.Value == settings.OCRLanguage);
			this.ConfirmSave = settings.ConfirmSave;
			this.OCRHighlightWholeWord = settings.OCRHighlightWholeWord;
			
			this.ImageWidth = settings.ImageWidth;
			this.TextWidth = settings.TextWidth;

			this.ApplicationMode = settings.ApplicationMode;
		}

		/// <summary>
		/// Uloží změny v layoutu.
		/// </summary>
		public void SaveLayout()
		{
			this.settingsProvider.SaveLayout(this);
		}

		/// <summary>
		/// Uloží nastavení podle aktuálního modelu.
		/// </summary>
		private void SaveSettings()
		{
			this.settingsProvider.Save(this);
			this.IsPopupOpened = false;

			// Pokud mám provést nějakou kaci po uložení.
			if(this.OnSettingsUpdated != null)
				this.OnSettingsUpdated(this);

			this.SaveSuccess = true;
			this.ExecuteWithDelay(() => this.SaveSuccess = false, TimeSpan.FromSeconds(2));
		}

		/// <summary>
		/// Otevře průvodce importem.
		/// </summary>
		private void OpenImportWindow()
		{
			var wnd = new Views.ImportWizard.ImportWizard(importModel);
			wnd.ShowDialog();
		}

		/// <summary>
		/// Otevře možnost exportu dat z aplikace.
		/// </summary>
		private void OpenExportAllWindow()
		{
			// Připravím model pro export.
			exportModel.PrepareExport();

			// A zobrazím okno.
			var wnd = new Views.ExportWizard.ExportWizard(exportModel);
			wnd.ShowDialog();
		}

		#region SpeedTest

		public ICommand DoTestCommand
		{
			get;
			private set;
		}

		#if DEBUG

		public bool ShowTest => true;

		#else
		
		public bool ShowTest => false;

		#endif

		public void DoTest()
		{
			var result = this.searchSpeedTest.GetSpeedResults();
			var formatted = result.Select(r => $"{r.Item2}: {r.Item1}");

			MessageBox.Show(string.Join(Environment.NewLine, formatted), "Výsledky testu", MessageBoxButton.OK);
		}


#endregion

	}
}
