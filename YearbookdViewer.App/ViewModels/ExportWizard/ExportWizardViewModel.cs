﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using YearbookViewer.App.Code;
using YearbookViewer.App.Code.Commands;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.App.Models;
using YearbookViewer.App.ViewModels.Base;
using YearbookViewer.Common;
using YearbookViewer.Common.Extensions;

namespace YearbookViewer.App.ViewModels.ExportWizard
{
	/// <summary>
	/// Model pro možnost exportování dat z aplikace.
	/// </summary>
	public class ExportWizardViewModel : ViewModelBase
	{
		private string destination;
		private ProgressBarModel progressBar;
		private bool exportDone;
		private bool exportButtonEnabled;

		#region comandy

		public IAsyncCommand Export
		{
			get;
			private set;
		}

		#endregion

		/// <summary>
		/// Vytvoří novou instanci modelu pro export.
		/// </summary>
		public ExportWizardViewModel()
		{
			this.ProgressBar = new ProgressBarModel();
			this.Export = new AsyncCommand(DoExport);
		}

		/// <summary>
		/// Vrací nebo nastavuje složku, do které se data exportují.
		/// </summary>
		public string Destination
		{
			get
			{
				return this.destination;
			}
			set
			{
				this.destination = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací, zda-li je možné zmáčknout tlačítko "Export".
		/// </summary>
		[DependsUpon(nameof(Destination))]
		public bool ExportButtonEnabled
		{
			get
			{
				if (this.Destination.IsNullOrEmpty())
					return false;

				return this.exportButtonEnabled;
			}
			set
			{
				if (this.exportButtonEnabled == value)
					return;

				this.exportButtonEnabled = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje zda-li je export hotový.
		/// </summary>
		public bool ExportDone
		{
			get
			{
				return this.exportDone;
			}
			set
			{
				if (this.exportDone == value)
					return;

				this.exportDone = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Vrací nebo nastavuje progress bar importu dat.
		/// </summary>
		public ProgressBarModel ProgressBar
		{
			get
			{
				return this.progressBar;
			}
			set
			{
				this.progressBar = value;
				RaisePropertyChanged();
			}
		}

		/// <summary>
		/// Připraví formulář pro export.
		/// </summary>
		public void PrepareExport()
		{
			this.ExportDone = false;
			this.ExportButtonEnabled = true;
			this.Destination = null;
		}

		/// <summary>
		/// Provede export dat z aplikace.
		/// </summary>
		private async Task DoExport()
		{
			// Zjistím si cestu k datům.
			var path = Path.Combine(Constants.FileStoreLocation, Constants.DataFolderName);

			// Ukážu progress bar.
			AppUtils.RunOnUiThread(() =>
			{
				this.ProgressBar.Visible = true;
				this.ExportButtonEnabled = false;
				this.ProgressBar.MaxValue = FileBrowser.GetNumberOfFiles(path);
			});

			// Počet již zpracovaných souborů.
			var processed = 0;

			// A projíždím složky - úroveň 1.
			foreach (var topLevel in Directory.EnumerateDirectories(path))
			{
				// Cesta na kterou importuji data.
				var exportTo = this.Destination;

				// Název složky.
				var fileName = Path.GetFileName(topLevel);

				// Pokud složka není výchozí -> tak ji také exportuji.
				if (fileName != Constants.DefaultTopLevelFilter)
				{
					exportTo = Path.Combine(exportTo, fileName);
					Directory.CreateDirectory(exportTo);
				}

				// A složky projíždím dál - úroveň 2.
				foreach (var secondLevel in Directory.EnumerateDirectories(topLevel))
				{
					// Název složky.
					fileName = Path.GetFileName(secondLevel);

					// Exportovat do.
					var exportLocation = exportTo;

					// Pokud složka není výchozí -> tak ji také exportuji.
					if (fileName != Constants.SecondaryLevelFilter)
					{
						exportLocation = Path.Combine(exportLocation, fileName);
						Directory.CreateDirectory(exportLocation);
					}

					// A Exportuji soubory.
					processed += await FileBrowser.CopyFiles(
						done =>
						{
							AppUtils.RunOnUiThread(() => this.ProgressBar.Value = done);
						},
						Path.Combine(secondLevel),
						exportLocation,
						recurse: false,
						alreadyProcessed: processed
					);
				}
			}

			// Export je hotov.
			AppUtils.RunOnUiThread(() =>
			{
				this.ProgressBar.Reset();
				this.ExportDone = true;
			});
		}
	}
}
