﻿using Microsoft.Extensions.DependencyInjection;
using YearbookViewer.App.Code.CustomElements;
using YearbookViewer.App.Code.Interfaces;
using YearbookViewer.App.Code.Factories;
using YearbookViewer.Common.Interfaces;
using YearbookViewer.App.ViewModels;
using YearbookViewer.App.Code;
using System.Windows;
using System;
using System.Diagnostics;
using System.Windows.Threading;
using NLog;
using YearbookViewer.App.ViewModels.ExportWizard;
using YearbookViewer.App.ViewModels.ImportWizard;

namespace YearbookViewer.App
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static readonly Logger logger = LogManager.GetLogger("App");

		protected override void OnStartup(StartupEventArgs e)
		{
			var provider = CreateServiceProvider();
			var mainWindow = provider.GetRequiredService<Views.MainWindow>();

			// Ukážu okno.
			mainWindow.Show();

			// Handlery pro výjimky.
			this.DispatcherUnhandledException += App_DispatcherUnhandledException;
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

			// Nakonfiguruji statické služby
			HighlightTermBehavior.Configure(provider.GetRequiredService<IExpressionEvaluator>());
			HighLighter.Configure(provider.GetRequiredService<IExpressionEvaluator>());

			base.OnStartup(e);
		}

		/// <summary>
		/// Odchycení většiny aplikačních výjimek.
		/// </summary>
		private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
		{
			e.Handled = true;
			var shutDown = true;
			
			// Mám debugger -> zeptám se.
			if (Debugger.IsAttached)
			{
				shutDown = MessageBox.Show("Chceš ukončit aplikaci?", "Výjimka", MessageBoxButton.YesNo) == MessageBoxResult.Yes;
				LogFatalException(e.Exception);
			}
			else
			{
				// Jinak loguji a aplikaci vypínám.
				LogFatalException(e.Exception);
			}

			// Vypnu.
			if (shutDown)
			{
				logger.Info("Application shuting down due to unhandled exception.");
				this.Shutdown();
			}
		}

		/// <summary>
		/// Odchycení výjimky z AppDomain v jiném threadu.
		/// </summary>
		/// <remarks>
		/// I přes to, že <see cref="App_DispatcherUnhandledException"/> obslouží většinu výjimek z aktuálního AppDomain, ve vzácných případech může být spuštěn thread na druhém AppDomain.
		/// </remarks>
		private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			// Vytáhnu výjimku.
			var exception = e.ExceptionObject as Exception;

			if (e.IsTerminating)
			{
				// Brzy to žuchne!
				// Alo o to se postará druhý error handler.
				LogFatalException(exception);
			}
			else
			{
				// Todle ještě aplikace zvládne -> pouze warning.
				logger.Warn(exception, exception?.Message);
			}
		}


		#region Private helpers

		private IServiceProvider CreateServiceProvider()
		{
			var services = new ServiceCollection();

			// Přidám providera uživatelského nastavení.
			services.AddSingleton<IUserSettingsProvider, XmlUserSettingsProvider>();

			// Přidám providery dat.
			services.AddSingleton<ISearchFilterValueProvider, SearchFiltersProviderFactory>();
			services.AddSingleton<IContentDataProvider, DataProviderFactory>();
			services.AddSingleton<IExpressionEvaluator, ExpressionEvaluatorsFactory>();

			// Přidám view modely.
			services.AddScoped<MainViewModel>();
			services.AddScoped<SearchFilterViewModel>();
			services.AddScoped<MainContentViewModel>();
			services.AddScoped<SettingsViewModel>();
			services.AddScoped<SearchResultsViewModel>();
			services.AddScoped<ImportWizardViewModel>();
			services.AddScoped<ExportWizardViewModel>();
			services.AddScoped<SearchSpeedTest>();

			// Přidám okna.
			services.AddScoped(s => new Views.MainWindow(s.GetRequiredService<MainViewModel>()));

			// Sestavím celou dependency injection.
			return services.BuildServiceProvider();
		}

		private static void LogFatalException(Exception exc)
		{
			if (exc != null)
			{
				logger.Fatal($"Unhandled exception {exc.Message} at {exc.StackTrace}", exc, DateTime.Now);
			}
		}

		#endregion
	}
}
