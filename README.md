# Bakalářská práce

## Desktopový systém pro editaci a vyhledávání dat z obrazově-textových databází

Tato bakalářská práce se zabývá vytvořením offline desktopové aplikace umožňující vyhledávání klíčových slov v obrazově-textových databázích. Cílem práce je vytvořit jednoduchou, intuitivní a moderní aplikaci za použití WPF, která by umožnila zobrazení téměř jakýchkoliv dat, skládajících se z textu a obrázku. Součástí aplikace je i průvodce importem dat, který uživateli umožňuje importovat téměř jakékoli textově obrázková data. V rámci importu si uživatel může definovat i vyhledávací engine, který slouží pro full-textové vyhledávání. Následně si uživatel může definovat svou vlastní strukturu dat v aplikaci nebo využít jakoukoliv z předdefinovaných možností, které aplikace nabízí. Práce je rozdělena tematicky do čtyř částí. V první části jsou uvedeny všechny technologie, které aplikace používá. Druhá část vysvětluje postupy, na jejichž základě byla aplikace vyvíjena. Třetí část nabízí reálné použití aplikace a poslední část předkládá sadu testovacích dat i postup, kterým byla ověřena platnost dat.

Při tvorbě aplikace byly využity následující technologie:
1) .NET
2) WPF
3) Dependency injection
4) Elastic Search

Dokumentaci i verze Aplikace lze nalézt ve složce CurrentVersion